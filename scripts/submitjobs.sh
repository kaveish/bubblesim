#!/bin/bash

set -e
#set -x

# get run directory
if [ ! $# -eq 1 ]; then
  echo "Must supply run directory as argument"
  exit 1
fi

rundir=$1

if [[ ! -d "$rundir" ]]; then
  echo "Run directory does not exist"
  exit 1
fi

# expand run directory
rundir=$(cd $rundir; pwd)/
echo $rundir

# get job directory
jobdir="$rundir"/jobs/todo/

# loop through job files and extract names
for f in $(find "$jobdir" -name '*.sub'); do
   qsub $f
done
