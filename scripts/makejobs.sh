#!/bin/bash

#set -x
set -e

# get run directory
if [ ! $# -eq 1 ]; then
  echo "Must supply run directory as argument"
  exit 1
fi

rundir=$1

if [[ ! -d "$rundir" ]]; then
  echo "Run directory does not exist"
  exit 1
fi

# expand run directory
rundir=$(cd $rundir; pwd)/
echo $rundir

# get job directory
jobdir="$rundir"/jobs/todo/
mkdir -p "$jobdir"

# change to script directory
dir=`dirname $0`
cd $dir

# lookup MCA diameters, store in array
while IFS=$'\t' read -r -a tmpArray
do
  echo ${tmpArray[0]}
  rightArray[${tmpArray[0]}]=${tmpArray[1]}
  leftArray[${tmpArray[0]}]=${tmpArray[2]}
done < "$rundir/in/mca.txt"

# loop through input files and extract names
for f in $(find "$rundir/in/todo" -name '*.txt' | sort); do
  name=$(basename $f .txt)
  sed s/BICI/$name/g <job-base.sub >"$jobdir"/"$name".sub
  # get number and left/right from name
  side=$(echo $name | cut -d'_' -f 2)
  number=$(echo $name | cut -d'_' -f 1)
  number=$(echo $number | cut -c 5,6,7)
  # store current diameter
  if [ $side = "R" ]; then
    diameter=${rightArray[$number]}
  elif [ $side = "L" ]; then
    diameter=${leftArray[$number]}
  else
    echo "ERROR: Filename must be in wrong format"
  fi
  radius=`echo "scale=3; $diameter / 2" | bc -l`
  # put radius in job file
  sed -i s:RADIUS:"$radius": "$jobdir"/"$name".sub
  # put run directory in job file
  sed -i s:RUNDIR:"$rundir": "$jobdir"/"$name".sub
  # get time of last embolus in each input file
  totaltime=$(tac "$f" |egrep -m 1 . | cut -f1)
  # cast float to integer
  totaltime=${totaltime%.*}
  # put time in job file
  sed -i s:RUNTIME:$totaltime: "$jobdir"/"$name".sub
  echo $name $number $side $diameter $totaltime
done

# make output folder (required for stdout stderr in job.sub)
mkdir -p "$rundir"/out

#qsub "$dir"/job.sub -N emboli-"$name" -o "$dir"/output.txt -e "$dir"/error.txt
