sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI19_R/g' -e 's/EMBFILE/BICI19_R/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.1/' -e 's/STARTHOUR/9/' -e 's/STARTMIN/52/' -e 's/STARTSEC/30/' tmp.gnu > plotallgraphs_pat19R.gnu
gnuplot plotallgraphs_pat19R.gnu
echo "19R"
cat perc/perc_patBICI19_R.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI19_R.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'

sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI19_L/g' -e 's/EMBFILE/BICI19_L/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.1/' -e 's/STARTHOUR/9/' -e 's/STARTMIN/56/' -e 's/STARTSEC/11/' tmp.gnu > plotallgraphs_pat19L.gnu
gnuplot plotallgraphs_pat19L.gnu
echo "19L"
cat perc/perc_patBICI19_L.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI19_L.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'


sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI39_R/g' -e 's/EMBFILE/BICI39_R/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.1/' -e 's/STARTHOUR/10/' -e 's/STARTMIN/48/' -e 's/STARTSEC/02/' tmp.gnu > plotallgraphs_pat39R.gnu
gnuplot plotallgraphs_pat39R.gnu
echo "39R"
cat perc/perc_patBICI39_R.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI39_R.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'

sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI39_L/g' -e 's/EMBFILE/BICI39_L/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.1/' -e 's/STARTHOUR/10/' -e 's/STARTMIN/47/' -e 's/STARTSEC/53/' tmp.gnu > plotallgraphs_pat39L.gnu
gnuplot plotallgraphs_pat39L.gnu
echo "39L"
cat perc/perc_patBICI39_L.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI39_L.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'

sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI40_R/g' -e 's/EMBFILE/BICI40_R/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.02/' -e 's/STARTHOUR/13/' -e 's/STARTMIN/14/' -e 's/STARTSEC/37/' tmp.gnu > plotallgraphs_pat40R.gnu
gnuplot plotallgraphs_pat40R.gnu
echo "40R"
cat perc/perc_patBICI40_R.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI40_R.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'



sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI40_L/g' -e 's/EMBFILE/BICI40_L/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.02/' -e 's/STARTHOUR/13/' -e 's/STARTMIN/14/' -e 's/STARTSEC/40/' tmp.gnu > plotallgraphs_pat40L.gnu
gnuplot plotallgraphs_pat40L.gnu
echo "40L"
cat perc/perc_patBICI40_L.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI40_L.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'

sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI44_R/g' -e 's/EMBFILE/BICI44_R/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.002/' -e 's/STARTHOUR/10/' -e 's/STARTMIN/8/' -e 's/STARTSEC/49/' tmp.gnu > plotallgraphs_pat44R.gnu
gnuplot plotallgraphs_pat44R.gnu
echo "44R"
cat perc/perc_patBICI44_R.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI44_R.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'

sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI44_L/g' -e 's/EMBFILE/BICI44_L/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.002/' -e 's/STARTHOUR/10/' -e 's/STARTMIN/8/' -e 's/STARTSEC/49/' tmp.gnu > plotallgraphs_pat44L.gnu
gnuplot plotallgraphs_pat44L.gnu
echo "44L"
cat perc/perc_patBICI44_L.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI44_L.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'



sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI47_R/g' -e 's/EMBFILE/BICI47_R/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/1/' -e 's/STARTHOUR/13/' -e 's/STARTMIN/23/' -e 's/STARTSEC/40/' tmp.gnu > plotallgraphs_pat47R.gnu
gnuplot plotallgraphs_pat47R.gnu
echo "47R"
cat perc/perc_patBICI47_R.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI47_R.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'

sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI47_L/g' -e 's/EMBFILE/BICI47_L/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/1/' -e 's/STARTHOUR/13/' -e 's/STARTMIN/23/' -e 's/STARTSEC/46/' tmp.gnu > plotallgraphs_pat47L.gnu
gnuplot plotallgraphs_pat47L.gnu
echo "47L"
cat perc/perc_patBICI47_L.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI47_L.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'



sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI54_R/g' -e 's/EMBFILE/BICI54_R/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.01/' -e 's/STARTHOUR/10/' -e 's/STARTMIN/11/' -e 's/STARTSEC/43/' tmp.gnu > plotallgraphs_pat54R.gnu
gnuplot plotallgraphs_pat54R.gnu
echo "54R"
cat perc/perc_patBICI54_R.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI54_R.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'


sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI54_L/g' -e 's/EMBFILE/BICI54_L/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.01/' -e 's/STARTHOUR/10/' -e 's/STARTMIN/20/' -e 's/STARTSEC/30/' tmp.gnu > plotallgraphs_pat54L.gnu
gnuplot plotallgraphs_pat54L.gnu
echo "54L"
cat perc/perc_patBICI54_L.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI54_L.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'


sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI55_R/g' -e 's/EMBFILE/BICI55_R/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.2/' -e 's/STARTHOUR/12/' -e 's/STARTMIN/26/' -e 's/STARTSEC/02/' tmp.gnu > plotallgraphs_pat55R.gnu
gnuplot plotallgraphs_pat55R.gnu
echo "55R"
cat perc/perc_patBICI55_R.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI55_R.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'


sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI55_L/g' -e 's/EMBFILE/BICI55_L/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.1/' -e 's/STARTHOUR/10/' -e 's/STARTMIN/26/' -e 's/STARTSEC/02/' tmp.gnu > plotallgraphs_pat55L.gnu
gnuplot plotallgraphs_pat55L.gnu
echo "55L"
cat perc/perc_patBICI55_L.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI55_L.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'

sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI60_R/g' -e 's/EMBFILE/BICI60_R/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.002/' -e 's/STARTHOUR/11/' -e 's/STARTMIN/02/' -e 's/STARTSEC/34/' tmp.gnu > plotallgraphs_pat60R.gnu
gnuplot plotallgraphs_pat60R.gnu
echo "60R"
cat perc/perc_patBICI60_R.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI60_R.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'



sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI60_L/g' -e 's/EMBFILE/BICI60_L/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.001/' -e 's/STARTHOUR/11/' -e 's/STARTMIN/02/' -e 's/STARTSEC/34/' tmp.gnu > plotallgraphs_pat60L.gnu
gnuplot plotallgraphs_pat60L.gnu
echo "60L"
cat perc/perc_patBICI60_L.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI60_L.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'

sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI61_R/g' -e 's/EMBFILE/BICI61_R/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.1/' -e 's/STARTHOUR/15/' -e 's/STARTMIN/28/' -e 's/STARTSEC/31/' tmp.gnu > plotallgraphs_pat61R.gnu
gnuplot plotallgraphs_pat61R.gnu
echo "61R"
cat perc/perc_patBICI61_R.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI61_R.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'

sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI61_L/g' -e 's/EMBFILE/BICI61_L/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.1/' -e 's/STARTHOUR/15/' -e 's/STARTMIN/28/' -e 's/STARTSEC/32/' tmp.gnu > plotallgraphs_pat61L.gnu
gnuplot plotallgraphs_pat61L.gnu
echo "61L"
cat perc/perc_patBICI61_L.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI61_L.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'

sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI64_R/g' -e 's/EMBFILE/BICI64_R/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.02/' -e 's/STARTHOUR/10/' -e 's/STARTMIN/32/' -e 's/STARTSEC/37/' tmp.gnu > plotallgraphs_pat64R.gnu
gnuplot plotallgraphs_pat64R.gnu
echo "64R"
cat perc/perc_patBICI64_R.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI64_R.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'

sed 's/DIRECTORY/Air_07Aug2013/g' plotallgraphs.gnu > tmp.gnu
sed -e 's/PERCFILE/perc_patBICI64_L/g' -e 's/EMBFILE/BICI64_L/g' -e 's/ZZZZ/at graph 0.6,1.5/' -e 's/YYYY/(0.0)/' -e 's/TICFREQ/0.02/' -e 's/STARTHOUR/10/' -e 's/STARTMIN/32/' -e 's/STARTSEC/38/' tmp.gnu > plotallgraphs_pat64L.gnu
gnuplot plotallgraphs_pat64L.gnu
echo "64L"
cat perc/perc_patBICI64_L.txt | grep -v ',' | awk '{if ($2 > max) max=$2}END{print max}'
cat perc/perc_patBICI64_L.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'

