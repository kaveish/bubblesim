#!/usr/bin/gnuplot -persist

set output "OUTDIR/OUTNAME.svg"
# want width 15cm=5.906in, gnuplot ouputs at 72dpi so want width 425px.
# Desired width is 7cm=2.756in=198.432px
# comes out 1.6x too big so multiply by 1.6
set term svg enhanced size 680,317 background "White" font "Arial,10"
# 827,413
# 590,413

sthour = STARTHOUR
stmin = STARTMIN
stsec = STARTSEC
sttime = sthour*60*60 + stmin*60 + stsec
#set format x "%12.0f:00"
ticfreq2 = BLOCKTICFREQ

# setup colors (Maureen Stone optimal colors for graphs)
set style line 1 pt -1 lc rgb "#396AB1" # blue line
set style line 2 pt -1 lc rgb "#DA7C30" # orange line
set style line 3 lc rgb "#3E9651" # green line
set style line 4 lc rgb "#CC2529" # red line
set style line 5 lc rgb "#535154" # grey line
set style line 6 lc rgb "#6B4C9A" # purple line
set style line 7 lc rgb "#922428" # dark red line
set style line 8 lc rgb "#948B3D" # murky green
set style line 9 lc rgb "#7293CB" # light blue line
set style line 10 lc rgb "#E1974C" # light orange line
set style line 11 lc rgb "#84BA5B" # light green line
set style line 12 lc rgb "#D35E60" # light red line
set style line 13 lc rgb "#808585" # light grey line
set style line 14 lc rgb "#9067A7" # light purple line
set style line 15 lc rgb "#AB6857" # light dark red line
set style line 16 lc rgb "#CCC210" # light murky green
#set style function lines

# get ranges from simulation data
stats "DIRECTORY/PERCFILE.txt" u (sthour+$1/3600.0):2 nooutput
x_max = STATS_max_x
y_max_l = STATS_max_y
stats "DIRECTORY/RIGHTPERC.txt" u (sthour+$1/3600.0):2 nooutput
y_max_r = STATS_max_y
y_max=0
if (y_max_l>y_max_r) {
  y_max=y_max_l
} else {
  y_max=y_max_r
}
set ytics 0,y_max/5,y_max offset 0.5,0 nomirror
y_max = y_max + y_max/5
set yrange[0:y_max]

yyyy = YYYY

# get max bubble size from file and set y axis accordingly
set autoscale y
stats "DIRECTORY/EMBFILE.txt" u 1:2 nooutput
y_max_l = int(STATS_max_y*2*10/2)/10.0*2 + 0.2
stats "DIRECTORY/RIGHTEMB.txt" u 1:2 nooutput
y_max_r = int(STATS_max_y*2*10/2)/10.0*2 + 0.2
if (y_max_l>y_max_r) {
  y_max=y_max_l
} else {
  y_max=y_max_r
}
set ytics 0,0.2,y_max
y_max = y_max + y_max/4
y_max = 10
y_min = 0.005
set ytics auto
set yrange[y_min:y_max]
set logscale y

set xdata time
set timefmt "%s"
set xtics format "%H:%M" offset 0,0.4 nomirror
set mxtics 3

set key bottom right at graph 1.03,0.1

# Time starts at 1940 for some reason, so 0 time = -1893369600
# -1893369600+14*60*60
hour = 60*60
min = 60
offset_gnuplot = -1893369600
offset_self = -60
offset = offset_gnuplot + offset_self
ypos = 0.006
set label 1 "Blood sampling / anaes. interventions" at first 13*hour+12.5*min+offset,ypos rotate by 90
set label 2 "On bypass" at first 13*hour+24.5*min+offset,ypos rotate by 90
set label 3 "Preparing graft" at first 14*hour+28*min+offset,ypos rotate by 90
set label 4 "Stitching graft" at first 14*hour+33*min+offset,ypos rotate by 90
set label 5 "Filling heart" at first 14*hour+39*min+offset,ypos rotate by 90
set label 6 "De-airing" at first 14*hour+53.5*min+offset,ypos rotate by 90
set label 7 "heart" at first 14*hour+56*min+offset,ypos rotate by 90
#set label 7 "Ejection of heart" at first 14*hour+56*min+offset,ypos rotate by 90
set label 8 "Removing aortic cannula / anaes. interventions" at first 15*hour+20*min+offset,ypos rotate by 90

set label 10 "Aortic cross-clamp on" at first 13*hour+33*min+offset,ypos rotate by 90
set label 11 "Aortic cross-clamp off" at first 14*hour+49*min+offset,ypos rotate by 90
set label 12 "Off bypass" at first 15*hour+12*min+offset,ypos rotate by 90

unset label

set timefmt "%s"

#set obj 1 rectangle behind from graph 0,0 to graph 1,1
#set obj 1 fillstyle solid 1.0 fillcolor rgbcolor "gray"

set lmargin 6
#set rmargin 1
#set tmargin 1
#set bmargin 1

set ylabel "bubble diameter (mm)" offset 4,0
set xlabel "time (hh:mm)" offset 0,1
#set style fill transparent solid 0.5 border lc rgb "#396AB1"
set style fill solid border lc rgb "#000000"
set border 3
bubblescale = 200
logscale = 1

# comment out to switch between time since surgery start and actual time
sttime = 0

#plot "DIRECTORY/EMBFILE.txt" using (timecolumn(1)+sttime):($2*2):(($2*logscale)*bubblescale) with circles lc rgbcolor "#black" title "left", "DIRECTORY/RIGHTEMB.txt" using (timecolumn(1)+sttime):($2*2):(($2*logscale)*bubblescale) with circles fs transparent solid 0 border lc rgbcolor "white" lc rgbcolor "white" title "right"
plot "DIRECTORY/EMBFILE.txt" using (timecolumn(1)+sttime):($2*2):($2*bubblescale) with circles lc rgb "#555555" title "left", "DIRECTORY/RIGHTEMB.txt" using (timecolumn(1)+sttime):($2*2):($2*bubblescale) with circles fs transparent solid 0.2 border lc rgb "#000000" lc rgb "#b3b3b3" title "right"
#plot "DIRECTORY/EMBFILE.txt" using (sthour+($1-yyyy)/3600.0):($2*2):($2/20.0) with circles lc rgb "#396AB1" title "", "DIRECTORY/RIGHTEMB.txt" using (sthour+($1-yyyy)/3600.0):($2*2):($2/20.0) with circles fs transparent solid 0.5 border lc rgb "#DA7C30" lc rgb "#000000" title ""
#unset key; unset tics; unset border
#set style fill transparent solid 0.5 border lc rgb "#DA7C30"
#plot "DIRECTORY/RIGHTEMB.txt" using (sthour+($1-yyyy)/3600.0):($2*2):($2/20.0) with circles lc rgb "#DA7C30" title ""
#unset multiplot

reset
set lmargin at screen 0.05
set rmargin at screen 0.95
set bmargin at screen 0.2
set tmargin at screen 0.70
set boxwidth 0.8
set style fill solid
set border 1
set style fill pattern border
set xtics nomirror offset 0,0.5 scale 0
unset ytics
unset key
set offset graph 0.10, 0.10

set yrange [0:0]
set autoscale ymax
set title "volume of air (mL)" offset 0,-0.5

set output "OUTDIR/OUTNAME_volume.svg"
set term svg enhanced size 177,89 background "White"
plot "DIRECTORY/volume-of-air.txt" every ::0::0 u 1:3:xtic(2) with boxes fs pattern 1 lt -1, \
     "DIRECTORY/volume-of-air.txt" every ::1::1 u 1:3:xtic(2) with boxes fs pattern 2 lt -1, \
     '' u 1:($3 + 0.01):($3) with labels

set yrange [0:0]
set autoscale ymax
set title "number of emboli" offset 0,-0.5

set output "OUTDIR/OUTNAME_numemboli.svg"
set term svg enhanced size 177,89 background "White"
plot "DIRECTORY/total-emboli.txt" every ::0::0 u 1:3:xtic(2) with boxes fs pattern 1 lt -1, \
     "DIRECTORY/total-emboli.txt" every ::1::1 u 1:3:xtic(2) with boxes fs pattern 2 lt -1, \
     '' u 1:($3 + 300):($3) with labels