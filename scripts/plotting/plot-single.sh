#!/bin/bash

#set -x

# change to script directory
dir=`dirname $0`
cd $dir

outdir=~/tmp/bubblesim/single/
plotdir=$outdir/plotfiles/
svgdir=$outdir/svg/
pngdir=$outdir/png/
datadir=data
embolidir="$datadir"/single
modeldir="$datadir"/model
mkdir -p "$plotdir" "$outdir" "$svgdir" "$pngdir"

# lookup start times, store in array
while IFS=$'\t' read -r -a tmp_array
do
  # convert time to seconds
  starttime_array[${tmp_array[0]}]=\
$(echo ${tmp_array[1]} | awk -F: '{ print ($1 * 3600) + ($2 * 60) + $3 }')
done < "$datadir"/start-times.txt

# output header
printf "# patient max%%blockedL max%%blockedR %%blockedover1hrL \
%%blockedover1hrR\n"
# create list of available left/right side data for each patient
containsElement () {
  local e
  true=0
  for e in "${@:2}"; do [[ $e == $1 ]] && true=1; done
  echo $true
}
count=0
for f in `ls -v $embolidir/*.txt`; do
  # get name, number and side from filename
  name=$(basename "$f" .txt)
  number=$(echo $name | cut -d'_' -f 1)
  number=$(echo $number | cut -c 5,6,7)
  side=$(echo $name | cut -d'_' -f 2)
  # fill arrays of available patients and whether they have left/right/both data
  if [ "$count" = 0 ]; then
    patients_array[$count]=$number
  elif [ "$(containsElement $number "${patients_array[@]}")" = 0 ]; then
    patients_array[$count]=$number
  fi
  if [ $side = "L" ]; then
    left_available_array[$number]=1
  else
    right_available_array[$number]=1
  fi
  ((count++))
done

# create a blank data file for non existent sides
touch blank-data.txt

# loop through output files and create plot files
trap "exit" INT
for number in ${patients_array[@]}; do
  printf "$number "
  file=BICI"$number"
  file_left="$file"_L
  file_right="$file"_R
  # split time
  ((hour=${starttime_array[number]}/3600))
  ((min=(${starttime_array[number]}%3600)/60))
  ((sec=${starttime_array[number]}%60))
  # get max instantaneous blockage
  max_block_l=$(cat "$modeldir"/"$file_left"_perc.txt \
    | grep -v ',' \
    | awk '{if ($2 > max) max=$2}END{print max}')
  max_block_r=$(cat "$modeldir"/"$file_right"_perc.txt \
    | grep -v ',' \
    | awk '{if ($2 > max) max=$2}END{print max}')
  # get max 1 hour blockage
  max_block_l_1hr=$(cat "$modeldir"/"$file_left"_perc.txt \
    | grep -v ',' \
    | awk '{if ($26 > max) max=$26}END{print max}')
  max_block_r_1hr=$(cat "$modeldir"/"$file_right"_perc.txt \
    | grep -v ',' \
    | awk '{if ($26 > max) max=$26}END{print max}')
  # convert max to decimal notation
  max_block_l=$(printf "%.5f\n" "$max_block_l")
  max_block_r=$(printf "%.5f\n" "$max_block_r")
  max_block_l_1hr=$(printf "%.5f\n" "$max_block_l_1hr")
  max_block_r_1hr=$(printf "%.5f\n" "$max_block_r_1hr")
  printf "%.5f %.5f %.5f %.5f\n" \
    "$max_block_l" "$max_block_r" "$max_block_l_1hr" "$max_block_r_1hr"
  # modify plot file
  sed 's/DIRECTORY/data/g' plot-base-single.gnuplot > "$plotdir"/"$file".gnuplot
  # left/right side data
  if [ "${left_available_array[$number]}" = 1 ]; then
    sed -i \
      -e "s:EMBFILE:emboli/$file_left:g" \
      -e "s:PERCFILE:model/""$file_left""_perc:g" \
      "$plotdir"/"$file".gnuplot
  else
    sed -i \
      -e "s:EMBFILE:../blank-data:g" \
      -e "s:PERCFILE:../blank-data:g" \
      "$plotdir"/"$file".gnuplot
  fi
  sed -i \
    -e "s:RIGHTEMB:emboli/$file_right:g" \
    -e "s:OUTDIR:$svgdir:g" \
    -e "s:RIGHTPERC:model/""$file_right""_perc:g" \
    -e "s:OUTNAME:$file:g" \
    -e "s/ZZZZ/at graph 0.6,1.5/" \
    -e "s/YYYY/(0.0)/" \
    -e "s/BLOCKTICFREQ/1/" \
    -e "s/BUBBLETICFREQ/1/" \
    -e "s/STARTHOUR/$hour/" \
    -e "s/STARTMIN/$min/" \
    -e "s/STARTSEC/$sec/" "$plotdir"/"$file".gnuplot
  # plot
  gnuplot "$plotdir"/"$file".gnuplot
  #convert -density 300 -resize 1180x826 "$svgdir"/"$file".svg "$pngdir"/"$file".png
  #inkscape -z -e "$pngdir"/"$file".png -w 1654 -h 826 -b "#ffffff" "$svgdir"/"$file".svg
  # change svg ends to rounded to stop small circles deforming
  # (this is a gnuplot output problem)
  sed -i 's/stroke-linejoin:miter/stroke-linejoin:round/g' "$svgdir"/"$file".svg
  # convert to png (inkscape works better than convert) want
  # 15x7cm=5.906x2.756in image at 300dpi, inkscape uses 150dpi during
  # conversion so we want pixels = 2*15*0.3937*300=3543 by 2*7*0.3937*300=1654
  inkscape -z -e "$pngdir"/"$file".png -w 3543 -h 1654 -b "#ffffff" "$svgdir"/"$file".svg
  inkscape -z -e "$pngdir"/"$file"_numemboli.png -w 709 -h 354 -b "#ffffff" "$svgdir"/"$file"_numemboli.svg
  inkscape -z -e "$pngdir"/"$file"_volume.png -w 709 -h 354 -b "#ffffff" "$svgdir"/"$file"_volume.svg
  #cat "$modeldir"/"$name"_perc.txt | grep -v ',' | awk '{if ($26 > max) max=$26}END{print max}'
done

# clean up
rm blank-data.txt
