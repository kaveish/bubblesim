#!/bin/bash

# change to script directory
dir=`dirname $0`
cd $dir

mkdir -p out

gnuplot << EOF
set output "out/max-block-instant.svg"
set term svg enhanced size 1200,800 background "White"
set style line 1 pt -1 lc rgb "#396AB1" # blue line
set style line 2 pt -1 lc rgb "#DA7C30" # orange line
set style data histograms
set style histogram gap 1
set boxwidth 1
set style fill solid 1.00 border -1
set xtic scale 0
set xlabel "Patient"
set ylabel "max. % of arterioles blocked"
plot "out/block-percentages.txt"  using 2:xtic(1) title "Left" ls 1, '' using 3 title "Right" ls 2
set output "out/max-block-1hr.svg"
set ylabel "% of arterioles blocked for over 1 hour"
plot "out/block-percentages.txt"  using 4:xtic(1) title "Left" ls 1, '' using 5 title "Right" ls 2
EOF

convert "out/max-block-instant.svg" "out/max-block-instant.png"
convert "out/max-block-1hr.svg" "out/max-block-1hr.png"
rm "out/max-block-instant.svg" "out/max-block-1hr.svg"
