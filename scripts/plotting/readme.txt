# Folder structure

## data

Input data for plotting. Should contain subfolders data/emboli and
data/model. Also contains start-times.txt which lists the start times
of surgery for each patient.

## data/start-times.txt

Format:
[Patient ID] TAB [Start time (HH:MM:SS)]

e.g.:
01	09:05:31
02	10:04:25
03	09:42:15

## data/emboli

Input files containing gas emboli timings, sizes, size error and EBR
from Caroline's sizing algorithm. Single file for each side.

## data/emboli/[REF][NUM]_[SIDE].txt

e.g. BICI21_L.txt

Format:
[Time] TAB [Bubble radius] TAB [Radius error] TAB [EBR]

e.g.:
28.144000	 0.007837	 0.003135	 26.880000
32.723200	 0.011123	 0.004449	 29.880000
41.363200	 0.008034	 0.003214	 27.080000

Time is in seconds from the recording start time listed in
data/start-times.txt. Radius and error are in mm. EBR is for reference
only and has been used in conjunction with other information, such as
haematocrit and vessel diameter, to estimate bubble size.

## data/model

'perc' files output by Jim's simulation. Contain the percentage of end
arterioles blocked for each second.

## data/model/[REF][NUM]_[SIDE]_perc.txt

e.g. BICI21_L_perc.txt

Format:
Not completely known, data is divided into 43 columns.

Known columns:
01 - Time in seconds from TCD recording start time.
02 - Instantaneous percentage of end arterioles blocked.
03 - Error in instantaneous percentage, calculated by running the
simulation multiple times with bubble sizes randomised according to
the error in estimated bubble sizes.
26 - Percentage of end arterioles which have been blocked for over an
hour in total during the course of the simulation. Will increase
monotonically.

## plotfiles

Contains generated gnuplot files.

## out

svg output from gnuplot

## out-png

png output created using convert from svg files.
