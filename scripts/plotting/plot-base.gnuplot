#!/usr/bin/gnuplot -persist
#
#
#    	G N U P L O T
#    	Version 4.4 patchlevel 4
#    	last modified November 2011
#    	System: CYGWIN_NT-5.1 1.7.10(0.259/5/3)
#
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2011
#    	Thomas Williams, Colin Kelley and many others
#
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help seeking-assistance"
#    	immediate help:   type "help"
#    	plot window:      hit 'h'

#set output "OUTDIR/OUTNAME.pdf"
#set term pdf enhanced font "Helvetica-Bold,14" dashed size 6,4
#set output "OUTDIR/OUTNAME.png"
#set term pngcairo enhanced size 1200,800
set output "OUTDIR/OUTNAME.svg"
set term svg enhanced size 1200,800 background "White"

unset clip points
set clip one
unset clip two
set bar 1.000000 front
set border 31 front linetype -1 linewidth 1.000
# set xdata time
set ydata
set zdata
set x2data
set y2data
set timefmt x "%H:%M"
set timefmt y "%d/%m/%y,%H:%M"
set timefmt z "%d/%m/%y,%H:%M"
set timefmt x2 "%d/%m/%y,%H:%M"
set timefmt y2 "%d/%m/%y,%H:%M"
set timefmt cb "%d/%m/%y,%H:%M"
set boxwidth
#set style fill  empty border
#set style circle radius graph 0.02, first 0, 0
set dummy x,y
# set format y "%12.0tx10^{%T}"
set format y "%12.3f"
set format x2 "% g"
set format y2 "% g"
set format z "% g"
set format cb "% g"
set angles radians
unset grid
#set key title ""
#set key inside left top vertical Right noreverse enhanced autotitles nobox
#set key noinvert samplen 2 spacing 1 width 0 height 0
#set key maxcolumns 0 maxrows 0
unset label
unset arrow
set style increment default
unset style line
unset style arrow
set style histogram clustered gap 2 title  offset character 0, 0, 0
unset logscale
set offsets 0, 0, 0, 0
set pointsize 1
set pointintervalbox 1
set encoding default
unset polar
unset parametric
unset decimalsign
set view 60, 30, 1, 1
set samples 100, 100
set isosamples 10, 10
set surface
unset contour
set clabel '%8.3g'
set mapping cartesian
set datafile separator whitespace
unset hidden3d
set cntrparam order 4
set cntrparam linear
set cntrparam levels auto 5
set cntrparam points 5
set size ratio 0 1,1
set origin 0,0
set style data points
set style function lines
set xzeroaxis linetype -2 linewidth 1.000
set yzeroaxis linetype -2 linewidth 1.000
set zzeroaxis linetype -2 linewidth 1.000
set x2zeroaxis linetype -2 linewidth 1.000
set y2zeroaxis linetype -2 linewidth 1.000
set ticslevel 0.5
set mxtics default
set mytics default
set mztics default
set mx2tics default
set my2tics default
set mcbtics default
set xtics border out scale 1,0.5 nomirror norotate  offset character -2.5, 0.2, 0
set xtics 1  norangelimit font "Helvetica,12"
set ytics border out scale 1,0.5 nomirror norotate  offset character 0, 0, 0
set ytics autofreq  norangelimit font "Helvetica,12"
set ztics border in scale 1,0.5 nomirror norotate  offset character 0, 0, 0
set ztics autofreq  norangelimit
set nox2tics
set noy2tics
set cbtics border in scale 1,0.5 mirror norotate  offset character 0, 0, 0
set cbtics autofreq  norangelimit
set title ""
set title  offset character 0, 0, 0 font "" norotate
set timestamp bottom
set timestamp ""
set timestamp  offset character 0, 0, 0 font "" norotate
set rrange [ * : * ] noreverse nowriteback  # (currently [8.98847e+307:-8.98847e+307] )
set trange [ * : * ] noreverse nowriteback  # (currently [-5.00000:5.00000] )
set urange [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set vrange [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set xlabel "Time (hh:mm)"
set xlabel offset character 0, 0.5, 0 font "" textcolor lt -1 norotate
set x2label ""
set x2label  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set x2range [ * : * ] noreverse nowriteback  # (currently [0.00000:10490.2] )
set ylabel  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set y2label ""
set y2label  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set yrange [ * : * ] noreverse nowriteback  # (currently [30.0000:70.0000] )
set y2range [ * : * ] noreverse nowriteback  # (currently [30.4900:67.5600] )
set zlabel ""
set zlabel  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set zrange [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set cblabel ""
set cblabel  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set cbrange [ * : * ] noreverse nowriteback  # (currently [8.98847e+307:-8.98847e+307] )
set zero 1e-08
set lmargin  -1
set bmargin  -1
set rmargin  -1
set tmargin  -1
set locale "en_US.UTF-8"
set pm3d explicit at s
set pm3d scansautomatic
set pm3d interpolate 1,1 flush begin noftriangles nohidden3d corners2color mean
set palette positive nops_allcF maxcolors 0 gamma 1.5 color model RGB
set palette rgbformulae 7, 5, 15
#set colorbox default
#set colorbox vertical origin screen 0.9, 0.2, 0 size screen 0.05, 0.6, 0 front bdefault
set loadpath
set fontpath
set fit noerrorvariables
sthour = STARTHOUR
stmin = STARTMIN
stsec = STARTSEC
set format x "%12.0f:STARTMIN"
set multiplot
set origin 0.17,0
set size 0.8,0.40
set lmargin 0.05
set rmargin 0.9
ticfreq2 = BLOCKTICFREQ
set key ZZZZ font "Helvetica,11" spacing 0.65
set key inside left top
# set xtics ("0" 0, "60" 60, "120" 120, "180" 180, "240" 240) font "Helvetica,12"
# david marshall start
#set style rectangle fillstyle noborder
#set object 1 rectangle from screen 0,0 to screen 1,1

# setup colors (Maureen Stone optimal colors for graphs)
set style line 1 pt -1 lc rgb "#396AB1" # blue line
set style line 2 pt -1 lc rgb "#DA7C30" # orange line
set style line 3 lc rgb "#3E9651" # green line
set style line 4 lc rgb "#CC2529" # red line
set style line 5 lc rgb "#535154" # grey line
set style line 6 lc rgb "#6B4C9A" # purple line
set style line 7 lc rgb "#922428" # dark red line
set style line 8 lc rgb "#948B3D" # murky green
set style line 9 lc rgb "#7293CB" # light blue line
set style line 10 lc rgb "#E1974C" # light orange line
set style line 11 lc rgb "#84BA5B" # light green line
set style line 12 lc rgb "#D35E60" # light red line
set style line 13 lc rgb "#808585" # light grey line
set style line 14 lc rgb "#9067A7" # light purple line
set style line 15 lc rgb "#AB6857" # light dark red line
set style line 16 lc rgb "#CCC210" # light murky green
#set style function lines

# get ranges from simulation data
stats "DIRECTORY/PERCFILE.txt" u (sthour+$1/3600.0):2 nooutput
x_max = STATS_max_x
duration = DURATION/3600.0
set xrange[sthour-duration/20:sthour+1.05*duration]
y_max_l = STATS_max_y
stats "DIRECTORY/RIGHTPERC.txt" u (sthour+$1/3600.0):2 nooutput
y_max_r = STATS_max_y
y_max=0
if (y_max_l>y_max_r) {
  y_max=y_max_l
} else {
  y_max=y_max_r
}
set ytics 0,y_max/5,y_max
y_max = y_max + y_max/5
set yrange[0:y_max]

set ylabel "% blocked" offset 8,0

set key spacing 1.5

# plot "DIRECTORY/PERCFILE.txt" using ($1/3600.0):2 with lines title "instantaneous" lt 1 lw 4, "DIRECTORY/PERCFILE.txt" using ($1/3600.0):4 with lines title "% blocked > 5 mins." lt 2 lw 4, "DIRECTORY/PERCFILE.txt" using ($1/3600.0):14 with lines title "% blocked > 30 mins." lt 3 lw 4, "DIRECTORY/PERCFILE.txt" using ($1/3600.0):26 with lines title "% blocked > 1 hour" lt 4 lw 4
#plot "DIRECTORY/PERCFILE.txt" using (sthour+$1/3600.0):2 with lines title "L instantaneous" ls 1 lw 4,  "DIRECTORY/PERCFILE.txt" using (sthour+$1/3600.0):($2-$3) with lines title "" ls 1 lw 1, "DIRECTORY/PERCFILE.txt" using (sthour+$1/3600.0):($2+$3) with lines title "" ls 1 lw 1, "DIRECTORY/RIGHTPERC.txt" using (sthour+$1/3600.0):2 with lines title "R instantaneous" ls 2 lw 4,  "DIRECTORY/RIGHTPERC.txt" using (sthour+$1/3600.0):($2-$3) with lines title "" ls 2 lw 1,  "DIRECTORY/RIGHTPERC.txt" using (sthour+$1/3600.0):($2+$3) with lines title "" ls 2 lw 1
plot "DIRECTORY/PERCFILE.txt" u (sthour+$1/3600.0):2 w lines title "L instantaneous" ls 1 lw 2, \
"DIRECTORY/PERCFILE.txt" u (sthour+$1/3600.0):($2-$3):($2+$3) notitle ls 9 w filledcu, \
'' u (sthour+$1/3600.0):($2-$3) notitle ls 1, \
'' u (sthour+$1/3600.0):($2+$3) notitle ls 1, \
"DIRECTORY/RIGHTPERC.txt" u (sthour+$1/3600.0):2 w lines title "R instantaneous" ls 2 lw 2, \
"DIRECTORY/RIGHTPERC.txt" u (sthour+$1/3600.0):($2-$3):($2+$3) notitle ls 10 w filledcu, \
'' u (sthour+$1/3600.0):($2-$3) notitle ls 2, \
'' u (sthour+$1/3600.0):($2+$3) notitle ls 2
unset xlabel
set origin 0.17,0.4
set size 0.8,0.60
set key left
#set yrange[0:2000]
set format y "%12.2f"
#bubbleticfreq=BUBBLETICFREQ*2000

# get max bubble size from file and set y axis accordingly
set autoscale y
stats "DIRECTORY/EMBFILE.txt" u (sthour+($1-YYYY)/3600.0):2 nooutput
y_max_l = int(STATS_max_y*2*10/2)/10.0*2 + 0.2
stats "DIRECTORY/RIGHTEMB.txt" u (sthour+($1-YYYY)/3600.0):2 nooutput
y_max_r = int(STATS_max_y*2*10/2)/10.0*2 + 0.2
if (y_max_l>y_max_r) {
  y_max=y_max_l
} else {
  y_max=y_max_r
}
set ytics 0,0.2,y_max
y_max = y_max + y_max/5
y_max = 5
y_min = 0.005
set ytics auto
set yrange[y_min:y_max]
set logscale y

#set ytics 0,bubbleticfreq,10*bubbleticfreq
set xtics ("" 0, "" 1, "" 2, "" 3, "" 4)
unset xtics
set ylabel "bubble diameter (mm)" offset 8,0
# set arrow from 0,30 to 250,30 nohead lt 2
set style fill transparent solid 0.5 border lc rgb "#396AB1"
unset key
set border
plot "DIRECTORY/EMBFILE.txt" using (sthour+($1-YYYY)/3600.0):($2*2):($2/8.0) with circles lc rgb "#396AB1" title ""
#unset key; unset tics; unset border
set key left
set style fill transparent solid 0.5 border lc rgb "#DA7C30"
plot "DIRECTORY/RIGHTEMB.txt" using (sthour+($1-YYYY)/3600.0):($2*2):($2/8.0) with circles lc rgb "#DA7C30" title ""
unset multiplot
set output
#    EOF
