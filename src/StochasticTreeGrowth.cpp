//*******************************************
// Stochastic arterial tree growth simulation
//-------------------------------------------
// (C) J.P. Hague, 2010
//-------------------------------------------
// Current modification 27th November 2010
//*******************************************

#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>

#include "node.h"
#include "Accumulator.h"
#include "RandomGenerator.h"

// Parameters used by the random number class

enum UpdateTypes {
  updateChangePosition,
  updateChangeDiameter,
  updateMoveBranch,
  updateSwapEndPositions,
  updateChangeNoEndNodes
};

// Parameters used by the accumulator class
// and the random number generator

namespace prm {
  double tolerance;
  double beta;             // This does nothing in this code
  int wallTimeMinutes;
  int nmeas;
  int nseries;
  long    	  *idum;
  clock_t                   parameterSetStartTime;
  clock_t                   currentTime;
  ConvergenceType           convergenceType = ConvergenceError;
}

// Global parameters

namespace gprm {
  int nEndNodes;  // Number of end nodes
  int nNodes;     // Total number of nodes

  int totalLevels;
  
  float startNodeDiam;
  float endNodeDiam;

  float endNodePressure;
  float startNodePressure;

  // Total flow through each arteriole is to be determined by
  // establishing oxygen levels required per second by the tissue
  // supplied by the arteriole. Need a biologist to help here.

  float flowPerArteriole;

  // Position of the root node

  float rootXPos;
  float rootYPos;
  float rootZPos;

  // Keep track of the total power dissipated in the tree
  // so that only have to recompute where a node has been
  // directly changed
  
  double totalPowerCurrent;
  double totalPowerLast;

  double annealTemperature;

  double boxSize = 10e-2; // Set box size to 10cm

  std::string outputFileName;

  double viscosity = 3.7e-3; // Viscocity in Pa s

  double pi = 3.14159; // Value of pi
}

// Select a node to be operated on

int selectChangableNode();
int selectMovableNode();
int selectEndNode();

// Compute the distance between nodes

float intraNodeDistance(int index1, int index2);

// Compute the total power dissipation of the tree

double computeTotalPower(std::vector<Node> &nodeVector);

// Flow through the arterioles is chosen to be constant as a
// boundary condition, making the total flow computation
// straight forward (as it can just be computed by the continuity
// equation). However, some configurations will not have the correct
// pressure drop from 100mgHg above atmosphere to atmosphere pressure
// and these should be penalised.

double pressureDropPenalty();

// Attempt to change a node position

bool attemptChangeNodePosition(double range);

// Attempt to swap end node position

bool attemptSwapEndPositions();

// Attempt to change a node diameter (corresponds to the feeder tube)

bool attemptChangeNodeDiameter();

// Attempt to move a node to a different part of the tree
// (cut / rejoin update)

bool attemptNodeMove();

// Determine the distance between nodes (length of the parent vessel)

// Compute the flows in the tree using a recursive method (or otherwise)

void computeFlowsAndResistances();

void computeFlow(int);

RandomGenerator randomNumber(-1247);

// Vector containing the tree;

std::vector<Node> gNodeVector;

// Vector containing the last config of the tree
// should look for more efficient ways of running
// the algorithm where a vector with the previous
// tree is not required

std::vector<Node> gNodeVectorLast;

std::vector<UpdateTypes> gUpdates;
std::vector<float> gUpdatesWeight;

// Routine to initialize the tree

void initializeTree(std::vector<Node> &nodeVector, 
		    double bifurcationExponentMin, double bifurcationExponentMax,
		    double lastElement, int *startLastRow, int *finishLastRow,
		    int *totalLastRow);

// Routine to write file that can be visualised by gnuplot

void writeTree(std::vector<Node> &nodeVector);

// Routine to write all flow and resistance values to file

void writeTreeFlowsResistances(std::vector<Node> &nodeVector);

// Routine to suggest update type

int suggestUpdate();

// Routine for computing the difference in the metabolism penalty

double computeDeltaPenalty(std::vector<int> &affectedNodes,
			   std::vector<Node> &affectedNodesVector);

// Routine to compute the effective power consumption of a node

void computeNodePower(Node &theNode);

// Compute the resistance of the node (and recursively)

double computeResistance(int);

// Recursively compute lengths of nodes

void computeLength(int);

// Copy one node vector to another

void copyNodeVector(std::vector<Node> &nodeVector1,
		    std::vector<Node> &nodeVector2);


int main(int argc,char* argv[]) {

  gprm::pi = acos(-1.0);
  
  int startLastRow = 0, finishLastRow = 0;
  
  double bifurcationExponent = 3.0;

  gprm::startNodeDiam = 1.0e-3;  // Have these fixed for now
  gprm::endNodeDiam = 0.012e-3;

  gprm::rootXPos = 0.0;
  gprm::rootYPos = 0.0;
  gprm::rootZPos = 0.0;
  
  std::cout << "StochasticTreeGrowth.exe <outputFileName> <nEndNodes>" 
	    << std::endl;

  gprm::outputFileName = argv[0+1];
  gprm::nEndNodes = (int)std::atof(argv[1+1]);

  std::cout << "Total arterioles: " << gprm::nEndNodes << std::endl;

  int nNodesLevel = gprm::nEndNodes;

  gprm::nNodes = nNodesLevel;

  // Determine the total number of tree levels in a
  // symmetric tree consistent with the number of
  // arteriole nodes.

  int gprm::totalLevels = 1;

  do {

    nNodesLevel /= 2;

    gprm::nNodes += nNodesLevel;
    
    gprm::totalLevels++;
    
  } while (nNodesLevel >= 1);
  
  std::cout << "Total nodes in tree: " << gprm::nNodes << std::endl;

  gNodeVector.resize(0);
  gNodeVectorLast.resize(0);

  //**************************************************
  // Initialize the tree structure with plausible tree
  //**************************************************
  
  float rootSize = gprm::startNodeDiam;   // Root node has 1mm size

  initializeTree(gNodeVector, bifurcationExponent, 
		 bifurcationExponent, rootSize,
		 &startLastRow, &finishLastRow, &gprm::nEndNodes);

  // Now compute the total power output of the tree.
  // First, flows and resistances should be computed.
  // before this, the lengths need computing

  computeLength(0);
  
  computeFlowsAndResistances();

  gprm::totalPowerCurrent = computeTotalPower(gNodeVector);
  gprm::totalPowerLast = gprm::totalPowerCurrent;

  gUpdates.resize(0);
  gUpdatesWeight.resize(0);

  gUpdates.push_back(updateChangePosition);
  gUpdatesWeight.push_back(0.4);

  gUpdates.push_back(updateSwapEndPositions);
  gUpdatesWeight.push_back(0.3);

  gUpdates.push_back(updateChangeDiameter);
  gUpdatesWeight.push_back(0.3);

  gUpdates.push_back(updateMoveBranch);
  gUpdatesWeight.push_back(0.0);

  
  gprm::annealTemperature = 1e8;
  
  std::cout << "Starting optimization ..." << std::endl;

  int nUpdates = 1;

  double annealParameter = 1e9;

  double nAnnealFactor = 1e6;    // Number of anneal measurements
  
  double deltaAnnealFactor = 1.0 / double(nAnnealFactor);

  bool isAccepted;

  int blockLength = 100;
  
  prm::nmeas = blockLength;
  
  Accumulator acceptanceRatioChangePosition;
  Accumulator acceptanceRatioSwapEndPositions;
  Accumulator acceptanceRatioChangeDiameter;
  Accumulator acceptanceRatioNodeMove;

  acceptanceRatioChangePosition.setSeriesLength(blockLength);
  acceptanceRatioChangePosition.setDataToZero();

  acceptanceRatioSwapEndPositions.setSeriesLength(blockLength);
  acceptanceRatioSwapEndPositions.setDataToZero();

  acceptanceRatioChangeDiameter.setSeriesLength(blockLength);
  acceptanceRatioChangeDiameter.setDataToZero();

  acceptanceRatioNodeMove.setSeriesLength(blockLength);
  acceptanceRatioNodeMove.setDataToZero();

  for (double annealFactor = deltaAnnealFactor; annealFactor<1.0; annealFactor+=deltaAnnealFactor) {

    gprm::annealTemperature *= (1.0-1e-8);
    
    int updateType = gUpdates[suggestUpdate()];
    
    switch (updateType) {
    case (updateChangePosition):

      // std::cout << "Attempt node position change" << std::endl;

      isAccepted = attemptChangeNodePosition(gprm::boxSize);

      acceptanceRatioChangePosition.addNewReading(isAccepted);

      isAccepted = attemptChangeNodePosition(gprm::boxSize/100.0);

      acceptanceRatioChangePosition.addNewReading(isAccepted);

      // std::cout << "cp " << isAccepted << std::endl;

      break;

    case (updateSwapEndPositions):

      isAccepted = attemptSwapEndPositions();

      acceptanceRatioSwapEndPositions.addNewReading(isAccepted);

      break;

    case(updateChangeDiameter):

      // std::cout << "Attempt node diameter change" << std::endl;

      isAccepted = attemptChangeNodeDiameter();

      acceptanceRatioChangeDiameter.addNewReading(isAccepted);

      // std::cout << "cd " << isAccepted << std::endl;

      break;

    case (updateMoveBranch):

      // std::cout << "Attempt branch move" << std::endl;

      isAccepted = attemptNodeMove();
 
      acceptanceRatioNodeMove.addNewReading(isAccepted);

      // std::cout << "nm " << isAccepted << std::endl;
      
      break;
      
    case (updateChangeNoEndNodes):
      
      std::cout << "UpdateChangeNoEndNodes not complete" << std::endl;
      exit(1);

      // isAccepted = attemptChangeNoEndNodes();
      // acceptanceRatioChangeNoEndNodes.addNewReading(isAccepted);

      break;

    }

    nUpdates++;

    //**************************************
    // Every few updates, write out the tree
    // and compute the total power.
    //**************************************

    if (nUpdates%1000==0) {
      writeTree(gNodeVector);
      nUpdates = 0;
      computeFlowsAndResistances();
      gprm::totalPowerCurrent = computeTotalPower(gNodeVector);
      gprm::totalPowerLast = gprm::totalPowerCurrent;
      std::cout << "Anneal factor = " << annealFactor << " ann. temp. " << gprm::annealTemperature << " power " << gprm::totalPowerCurrent
		<< " acceptance: change pos. " << acceptanceRatioChangePosition.getAverageOverSeries() 
		<< " change diam. " << acceptanceRatioChangeDiameter.getAverageOverSeries()
		<< " branch move " << acceptanceRatioNodeMove.getAverageOverSeries() 
		<< " swap end " << acceptanceRatioSwapEndPositions.getAverageOverSeries() << std::endl;
    }

    // Suggest that should change the anneal temperature using the
    // variable parameterization I used in my PhD thesis
    // so that x = 1/T / (a + 1/T) = 1/(Ta+1)
    // I.e. Ta+1 = 1/x, Ta = 1/x-1 = (1-x)/x, T  = (1-x)/ax
    // Then can simply have a for loop, and put
    // more steps in to gain better annealing.
    // The factor a simply controls the crossover point between
    // weak coupling and strong coupling behaviour.

    gprm::annealTemperature = (1.0 - annealFactor) / annealFactor / annealParameter;

  }
  
  writeTree(gNodeVector);
  
  exit(0);
}

int suggestUpdate()
{
  double r = randomNumber.nextDouble();

  double sum = 0.0;

  int i;
  
  for (i=0; i<gUpdatesWeight.size(); i++) {
    sum += gUpdatesWeight[i];
    if (sum > r)
      break;
  }

  return i;
}


float interNodeDistance(int index1, int index2)
{
  float distance = (gNodeVector[index1].xLoc - gNodeVector[index2].xLoc)
    * (gNodeVector[index1].xLoc - gNodeVector[index2].xLoc);
  
  distance += distance = (gNodeVector[index1].yLoc - gNodeVector[index2].yLoc)
    * (gNodeVector[index1].yLoc - gNodeVector[index2].yLoc);

  distance += distance = (gNodeVector[index1].zLoc - gNodeVector[index2].zLoc)
    * (gNodeVector[index1].zLoc - gNodeVector[index2].zLoc);

  return std::sqrt(distance);
}

//********************************************
// Routine for initializing the tree structure
//********************************************
void initializeTree(std::vector<Node> &nodeVector, 
		    double bifurcationExponentMin, 
		    double bifurcationExponentMax,
		    double lastElement, 
		    int *startLastRow, int *finishLastRow,
		    int *totalLastRow)
{
  std::vector<double> nodeSizes(0);

  nodeVector.resize(0);

  double lastElementDouble = lastElement;

  double bifurcationExponent;

  std::cout << "\n";
  std::cout << "Tree information\n";
  std::cout << "----------------\n";
  std::cout << "Tree has " << gprm::totalLevels << " levels" << std::endl;

  for (int i=0; i<gprm::totalLevels; i++) {	
    bifurcationExponent = bifurcationExponentMin
      + (double)i*(bifurcationExponentMax-bifurcationExponentMin)
      /(double)gprm::totalLevels;

    // nodeSizes.add(new Double(lastElementDouble));

    nodeSizes.push_back(lastElementDouble);

    std::cout << "Node " << i << " size : " << lastElementDouble << " mm, bifurcation exp " << bifurcationExponent << std::endl;
    
    // What we computed in our paper
    // lastElementDouble*=std::sqrt(bifurcationExponent)/2.0;
    // Get a bifurcation exponent from the layer ratio
    
    // layerRatio = std::sqrt(bifurcationExponent) / 2.0;
    // bifurcationExponent = -std::log(2.0)/std::log(layerRatio);
    
    std::cout << "True exponent " << bifurcationExponent << std::endl;
    
    // We ought to have used this exponent as defined in Burn's paper
    lastElementDouble /= std::pow(2.0,1.0/bifurcationExponent);
  }
  
  Node theNode;
  
  int levelBase;

  int lastLevelBase, nextLevelBase;

  // Initialise the tree structure
  // The tree structure has been tested and it initialises correctly
  
  std::cout << "Initialise the tree structure" << std::endl;
  
  // double[] betaIJ = new double[gprm::totalLevels * gprm::totalLevels];

  int nLast = (int)std::sqrt(*totalLastRow);

  for (int i=0;i<gprm::totalLevels;i++) {
    levelBase=(int)std::pow(2.0,i)-1;
    lastLevelBase=(int)std::pow(2.0,i-1)-1;
    nextLevelBase=(int)std::pow(2.0,i+1)-1;
    *startLastRow=levelBase;
    *finishLastRow=nextLevelBase;
    for (int j=0;j<(int)std::pow(2.0,i);j++) {
      theNode.size=nodeSizes[i];
      if (i == gprm::totalLevels -1) {
	theNode.daughterA = -2;
	theNode.daughterB = -2;
	theNode.size = gprm::endNodeDiam;
	theNode.pressure = gprm::endNodePressure;
      } else {
	theNode.daughterA=nextLevelBase+2*j;
	theNode.daughterB=nextLevelBase+2*j+1;
      }
      theNode.parent=lastLevelBase+j/2;
      theNode.location=levelBase+j;
      theNode.isBlocked=false;
      theNode.treeLevel=i;

      if (i==gprm::totalLevels-1) {
	theNode.isLastNode=true;
	theNode.xLoc = 10.0e-3 * (randomNumber.nextDouble() - 0.5);
	theNode.yLoc = 10.0e-3 * (randomNumber.nextDouble() - 0.5);
	theNode.zLoc = -10.0e-3;
      }

      if (i==0) {
	theNode.parent=-1;
	theNode.size = gprm::startNodeDiam;
	theNode.pressure = gprm::startNodePressure;
      }

      // std::cout << "tree info. " << theNode.location << " p " << theNode.parent << " dA " << theNode.daughterA << " dB " << theNode.daughterB << std::endl;

      nodeVector.push_back(theNode);
    }
  }
  
  *totalLastRow = *finishLastRow - *startLastRow;
  
  std::cout << "... tree structure initialised with total elements: " << nodeVector.size() << std::endl;
  std::cout << "Capiliary nodes starting at " << *startLastRow << " and ending at " << *finishLastRow << std::endl;
}

void writeTree(std::vector<Node> &nodeVector)
{
  std::ofstream outputFile(gprm::outputFileName.c_str());
  
  outputFile << 0 << " " << 0 << " " << 0 << std::endl;

  for (int i=1; i<nodeVector.size(); i++) {
    outputFile << nodeVector[i].xLoc << " " 
	       << nodeVector[i].yLoc << " "
	       << nodeVector[i].zLoc << std::endl;

    outputFile << nodeVector[nodeVector[i].parent].xLoc << " " 
	       << nodeVector[nodeVector[i].parent].yLoc << " "
	       << nodeVector[nodeVector[i].parent].zLoc << std::endl;

    outputFile << std::endl;
  }
  
  outputFile.close();
}

// Routine to write values of flow and resistance etc from
// the tree to file.
void writeTreeFlowsResistances(std::vector<Node> &nodeVector)
{
  std::ofstream outputFile("flowrestest.d");

  for (int i=1; i<nodeVector.size(); i++) {
    outputFile << i << " " 
	       << nodeVector[i].size << " "
	       << nodeVector[i].length << " "
	       << nodeVector[i].flow << " "
	       << nodeVector[i].resistance << " "
	       << nodeVector[i].xLoc << " " 
	       << nodeVector[i].yLoc << " "
	       << nodeVector[i].zLoc << " ";
    outputFile << nodeVector[nodeVector[i].parent].xLoc << " " 
	       << nodeVector[nodeVector[i].parent].yLoc << " "
	       << nodeVector[nodeVector[i].parent].zLoc << std::endl;

    outputFile << std::endl;
  }
  
  outputFile.close();
}

//*********************************************
// Attempt to change a node position.
// Note that although only a single node
// and its daughters is changed, the whole tree
// is affected as all flows may change..
// Thus earlier attempts to only recompute
// power distribution for the changed nodes
// were incorrect
//*********************************************
bool attemptChangeNodePosition(double range)
{
  int iNode = selectChangableNode();

  double xLoc = range * (randomNumber.nextDouble()-0.5);
  double yLoc = range * (randomNumber.nextDouble()-0.5);
  double zLoc = range * (randomNumber.nextDouble()-0.5);

  if (xLoc > gprm::boxSize / 2.0) xLoc -= gprm::boxSize;
  if (yLoc > gprm::boxSize / 2.0) yLoc -= gprm::boxSize;
  if (zLoc > gprm::boxSize / 2.0) zLoc -= gprm::boxSize;

  if (xLoc < -gprm::boxSize / 2.0) xLoc += gprm::boxSize;
  if (yLoc < -gprm::boxSize / 2.0) yLoc += gprm::boxSize;
  if (zLoc < -gprm::boxSize / 2.0) zLoc += gprm::boxSize;

  gNodeVector[iNode].xLoc = xLoc;
  gNodeVector[iNode].yLoc = yLoc;
  gNodeVector[iNode].zLoc = zLoc;

  // This is the only update that requires
  // a recomputation of lengths. In principle
  // only need to recompute lengths near changes
  
  computeLength(0);

  // Now compute the total power output of the tree

  computeFlowsAndResistances();

  gprm::totalPowerCurrent = computeTotalPower(gNodeVector);
  
  bool isAccepted;

  if (std::exp((gprm::totalPowerLast - gprm::totalPowerCurrent) / gprm::annealTemperature) 
      > randomNumber.nextDouble()) {
    isAccepted = true;
  } else {
    isAccepted = false;
  }

  // std::cout << "HH" << std::endl;

  if (isAccepted) {
    copyNodeVector(gNodeVector,gNodeVectorLast);
    gprm::totalPowerLast = gprm::totalPowerCurrent;
  } else {
    copyNodeVector(gNodeVectorLast,gNodeVector);
    gprm::totalPowerCurrent = gprm::totalPowerLast;
  }


  // std::cout << "II" << std::endl;

  return isAccepted;

}

//*********************************************
// Attempt to change a node position.
// Note that although only a single node
// and its daughters is changed, the whole tree
// is affected as all flows may change..
// Thus earlier attempts to only recompute
// power distribution for the changed nodes
// were incorrect
//*********************************************
bool attemptSwapEndPositions()
{
  int iNode = selectEndNode();
  int jNode = selectEndNode();

  double xLoc_i = gNodeVector[iNode].xLoc;
  double yLoc_i = gNodeVector[iNode].yLoc;
  double zLoc_i = gNodeVector[iNode].zLoc;

  double xLoc_j = gNodeVector[jNode].xLoc;
  double yLoc_j = gNodeVector[jNode].yLoc;
  double zLoc_j = gNodeVector[jNode].zLoc;

  gNodeVector[iNode].xLoc = xLoc_j;
  gNodeVector[iNode].yLoc = yLoc_j;
  gNodeVector[iNode].zLoc = zLoc_j;

  gNodeVector[jNode].xLoc = xLoc_i;
  gNodeVector[jNode].yLoc = yLoc_i;
  gNodeVector[jNode].zLoc = zLoc_i;

  // This is the only update that requires
  // a recomputation of lengths. In principle
  // only need to recompute lengths near changes
  
  computeLength(0);

  // Now compute the total power output of the tree

  computeFlowsAndResistances();

  gprm::totalPowerCurrent = computeTotalPower(gNodeVector);
  
  bool isAccepted;

  if (std::exp((gprm::totalPowerLast - gprm::totalPowerCurrent) / gprm::annealTemperature) 
      > randomNumber.nextDouble()) {
    isAccepted = true;
  } else {
    isAccepted = false;
  }

  if (isAccepted) {
    copyNodeVector(gNodeVector,gNodeVectorLast);
    gprm::totalPowerLast = gprm::totalPowerCurrent;
  } else {
    copyNodeVector(gNodeVectorLast,gNodeVector);
    gprm::totalPowerCurrent = gprm::totalPowerLast;
  }

  return isAccepted;

}


//********************************************
// Make an attempt at changing a node diameter
//********************************************
bool attemptChangeNodeDiameter()
{
  int iNode = selectChangableNode();
  
  // Ensure that the new diameter is allowed. Although in principle
  // there needn't be any restriction on vessel size decrease, which ought
  // to come about according to the power optimisation

  // double newDiameter = daughterDiameter + 
  //   (parentDiameter - daughterDiameter) * randomNumber.nextDouble();

  double newDiameter = gprm::endNodeDiam + (gprm::startNodeDiam - gprm::endNodeDiam) * randomNumber.nextDouble();

  gNodeVector[iNode].size = newDiameter;  

  // Now compute the total power output of the tree

  computeFlowsAndResistances();

  // std::cout << "FF" << std::endl;

  gprm::totalPowerCurrent = computeTotalPower(gNodeVector);
  
  // std::cout << gprm::totalPowerCurrent << std::endl;

  // std::cout << "GG" << std::endl;

  bool isAccepted = false;

  if (std::exp((gprm::totalPowerLast - gprm::totalPowerCurrent) / gprm::annealTemperature) 
      > randomNumber.nextDouble()) {
    isAccepted = true;
  } else {
    isAccepted = false;
  }

  // std::cout << "HH" << std::endl;

  if (isAccepted) {
    copyNodeVector(gNodeVector,gNodeVectorLast);
    gprm::totalPowerLast = gprm::totalPowerCurrent;
  } else {
    copyNodeVector(gNodeVectorLast,gNodeVector);
    gprm::totalPowerCurrent = gprm::totalPowerLast;
  }


  // Use metropolis

  //if (std::exp(deltaPenalty / gprm::annealTemperature) > randomNumber.nextDouble()) {
  //  isAccepted = true;
  //} else {
  //  isAccepted = false;
  //}
  
  //if (isAccepted) {
  //  for (int i=0; i<affectedNodes.size(); i++) {
  //    gNodeVector[affectedNodes[i]] = affectedNodesVector[i];
  //    gNodeVectorLast[affectedNodes[i]] = affectedNodesVector[i];
  //  }
  //}

  return isAccepted;
}

// Attempt to move a node 

bool attemptNodeMove()
{
  
  bool isAccepted;

  // Select two nodes. 
  // Cut will occur between the node and its parent

  int iNode1 = selectMovableNode();
  int iNode2 = selectMovableNode();

  // Check that the resulting tree is actually allowed.
  // Not sure how to do this yet.
  // Think it is simply necessary to check that one
  // of the nodes is not on the same tree as the other.
  // This can be achieved by repeatedly moving up the
  // tree from each of the two nodes
  // through the parent route until the top node
  // is reached (which is denoted by a -1)

  bool disallowed = false;

  int iNodeCheck = iNode1;

  do {
    if (iNodeCheck == iNode2) disallowed = true;
    iNodeCheck = gNodeVector[iNodeCheck].parent;
  } while (iNodeCheck >= 0);

  iNodeCheck = iNode2;

  do {
    if (iNodeCheck == iNode1) disallowed = true;
    iNodeCheck = gNodeVector[iNodeCheck].parent;
  } while (iNodeCheck >= 0);

  if (disallowed) return false;

  // Swap the links by updating the linked list between
  // parent and node. The parent of node A will become
  // the parent of node B and vice versa. The links
  // from the 

  if (gNodeVector[gNodeVector[iNode1].parent].daughterA == iNode1) {
    gNodeVector[gNodeVector[iNode1].parent].daughterA = iNode2;
  } else {
    gNodeVector[gNodeVector[iNode1].parent].daughterB = iNode2;
  }

  if (gNodeVector[gNodeVector[iNode2].parent].daughterA == iNode2) {
    gNodeVector[gNodeVector[iNode2].parent].daughterA = iNode1;
  } else {
    gNodeVector[gNodeVector[iNode2].parent].daughterB = iNode1;
  }

  std::swap(gNodeVector[iNode1].parent,
	    gNodeVector[iNode2].parent);

  // Now compute the total power output of the tree

  computeFlowsAndResistances();

  gprm::totalPowerCurrent = computeTotalPower(gNodeVector);
  
  // std::cout << gprm::totalPowerCurrent << std::endl;

  if (std::exp((gprm::totalPowerLast - gprm::totalPowerCurrent) / gprm::annealTemperature) 
      > randomNumber.nextDouble()) {
    isAccepted = true;
  } else {
    isAccepted = false;
  }

  if (isAccepted) {
    copyNodeVector(gNodeVector,gNodeVectorLast);
    gprm::totalPowerLast = gprm::totalPowerCurrent;
  } else {
    copyNodeVector(gNodeVectorLast,gNodeVector);
    gprm::totalPowerCurrent = gprm::totalPowerLast;
  }


  return isAccepted;

}

void copyNodeVector(std::vector<Node> &nodeVector1,
		    std::vector<Node> &nodeVector2)
{
  nodeVector2.resize(nodeVector1.size());
  for (int i=0; i<nodeVector1.size(); i++) {
    nodeVector2[i] = nodeVector1[i];
  }
}

//********************************************
// Compute the difference in the power penalty
//********************************************
double computeDeltaPenalty(std::vector<int> &affectedNodes,
			   std::vector<Node> &affectedNodesVector)
{
  // Establish the power in the nodes affected by the change in
  // a single diameter or position
  
  double deltaPower = 0.0;
  
  for (int i=0; i<affectedNodesVector.size(); i++) {
    computeNodePower(affectedNodesVector[i]);
    computeNodePower(gNodeVector[affectedNodes[i]]);
    
    deltaPower += affectedNodesVector[i].power;
    deltaPower -= gNodeVector[affectedNodes[i]].power;
  }
  
  return deltaPower;
  
}

// A changable node shouldn't include any of the end nodes, or the root node
int selectChangableNode()
{
  int selectedNode;
  do {
    selectedNode = (int)(std::floor(double(gprm::nNodes) * randomNumber.nextDouble()));
  } while (gNodeVector[selectedNode].parent < 0 || 
	   gNodeVector[selectedNode].daughterA < 0 || 
	   gNodeVector[selectedNode].daughterB < 0);

  return selectedNode;
}

//***********************************************
// A movable node shouldn't include the root node
//***********************************************
int selectMovableNode()
{
  int selectedNode;
  do {
    selectedNode = (int)(std::floor(double(gprm::nNodes) * randomNumber.nextDouble()));
  } while (gNodeVector[selectedNode].parent < 0);

  return selectedNode;
}

//*****************************
// Select an end node at random
//*****************************
int selectEndNode()
{
  int selectedNode;
  do {
    selectedNode = (int)(std::floor(double(gprm::nNodes) * randomNumber.nextDouble()));
  } while (!gNodeVector[selectedNode].isLastNode);
  return selectedNode;
}

// The total power in a single tube is given by equation 3.6.1 in Zamek's book
// which is really from Murray's 1926 paper:
//
// H = q^2 l A/a^4 + B l a^2
// 
// where H is total power. The first term represents total pumping power
// required to overcome viscocity and the second term represents the 
// metabolic energy required to maintain a volume of blood.
// Presumably, B is proportional to l just like A (although this is not
// mentioned in Zamek's book) since the total volume of the cylinder is
// equal to pi * a^2 * l.
// Other than that, the parameter B is difficult to establish - presumably
// it could be estimated, but not sure how.
// The variable A = 8 mu / pi, where mu is the viscocity and q is
// the flow rate.
void computeNodePower(Node &theNode)
{
  double mu = gprm::viscosity;        // The viscocity
  double variableA = 8.0 * mu / gprm::pi;
  double variableB = 2.8 * gprm::pi;             

  // Approx max metabolism for blood generation is 2.8J/s/m^3

  theNode.power = 0.0;

  theNode.power += theNode.length*(variableA *std::pow(theNode.flow,2)
				   / std::pow(theNode.size/2.0,4)
				   + variableB * std::pow(theNode.size/2.0,2));
}


//******************************************************
// Compute flows on the tree using a recursive algorithm
// N.B. This only works for Pouseille flow!
//******************************************************
void computeFlowsAndResistances()
{
  // Computes all resistances below the root node
  // using a recursive algorithm

  computeResistance(0);

  // Each node has an element resistanceBelow containing
  // the resistance below the node. Another element 
  // resistance containing the resistance of the node
  // As with the resistance, a recursive algoritm is
  // appropriate.
  
  // With knowledge of the total input flow, the
  // relative flows in the two daughter branches
  // can be determined as Q_A / Q_B = R_B / R_A.
  // Also, the flow through the pipe above the node
  // can be determined (by flow conservation). It is
  // Q_0 = Q_A (1 + R_A / R_B) or 
  // Q_0 = Q_B (1 + R_B / R_A)
  // Although this only gives a ratio - the initial
  // flow must be known? Not is the pressure is known
  // it can be computed!
  // 
  // V = R_0 Q_A (1 + R_A / R_B) + Q_A R_A
  // 
  // or 
  //
  // V = Q_0 R_0 + Q_0 [1+R_A/R_B]^{-1}
  // 
  // leading to Q_0

  // Presumably it just requires the total resistance
  // of the tree below and including the top node!

  computeFlow(0);

  writeTreeFlowsResistances(gNodeVector);
}

void computeFlow(int index)
{
  double flow;
  double top_pressure = gNodeVector[index].pressure; // This needs to be the pressure
  // at the top of the inlet pipe

  double node_pressure;

  flow = top_pressure / gNodeVector[index].resistanceBelow;

  // Pressure at the bifurcation

  double top_pressure_node = top_pressure - flow * gNodeVector[index].resistance;

  gNodeVector[index].flow = flow;

  int daughterA = gNodeVector[index].daughterA;
  int daughterB = gNodeVector[index].daughterB;

  gNodeVector[daughterA].pressure = top_pressure_node;
  gNodeVector[daughterB].pressure = top_pressure_node;

  if (!gNodeVector[daughterA].isLastNode) {
    computeFlow(daughterA);
  }

  if (!gNodeVector[daughterB].isLastNode) {
    computeFlow(daughterB);
  }
  
}

void computeLength(int index)
{
  double length = 0.0;
  length += std::pow((gNodeVector[index].xLoc -
		      gNodeVector[gNodeVector[index].parent].xLoc),2);
  length += std::pow((gNodeVector[index].yLoc -
		      gNodeVector[gNodeVector[index].parent].yLoc),2);
  length += std::pow((gNodeVector[index].zLoc -
		      gNodeVector[gNodeVector[index].parent].zLoc),2);
  gNodeVector[index].length = std::sqrt(length);
  if (!gNodeVector[index].isLastNode) {
    computeLength(gNodeVector[index].daughterA);
    computeLength(gNodeVector[index].daughterB);
  }
}

//******************************************************
// Compute resistances on the tree using a 
// recursive algorithm
//******************************************************
double computeResistance(int index)
{
  double resistance;
  
  if (gNodeVector[index].isLastNode) {

    resistance = 8.0 * gprm::viscosity * gNodeVector[index].length / gprm::pi / std::pow(gNodeVector[index].size/2.0,4.0);  // Compute the resistance using the Pouiselle expression
    gNodeVector[index].resistance = resistance;

  } else {

    gNodeVector[index].resistance = 8.0 * gprm::viscosity * gNodeVector[index].length / gprm::pi / std::pow(gNodeVector[index].size/2.0,4.0);  // Compute the resistance using the Pouiselle expression

    // This can't be right as haven't computed the resistance at each node
    // yet
    
    resistance = 1.0/computeResistance(gNodeVector[index].daughterA)
      + 1.0/computeResistance(gNodeVector[index].daughterB);
    resistance = 1.0/resistance + gNodeVector[index].resistance;
  }

  gNodeVector[index].resistanceBelow = resistance;

  return resistance;
}

// Compute the total power dissipation of the tree
 double computeTotalPower(std::vector<Node> &nodeVector)
 {
   double totalPower = 0.0;
   for (int i=0; i<nodeVector.size(); i++) {
     computeNodePower(gNodeVector[i]);
     totalPower += nodeVector[i].power;
     // std::cout << nodeVector[i].power << std::endl;
   }
   return totalPower;
 }
 
 // Compute a penalty for incorrect pressures in the 
 // final nodes
 double pressureDropPenalty(std::vector<Node> &nodeVector)
 {
   double penalty = 0.0;
   for (int i=0; i<nodeVector.size(); i++) {
     if (nodeVector[i].daughterA == -2) {
       penalty += std::pow(gprm::endNodePressure - nodeVector[i].pressure,2);
     }
   }

   return penalty;
 }
