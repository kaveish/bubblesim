#ifndef EMBOLUS_H
#define EMBOLUS_H

// There are currently two embolus types, 
// an embolus with a linear dissolve rate
// which corresponds to a volume dissolve rate which is
// proportional to surface area and assuming a spherical, 
// cylindircal or hybrid
// shape. This is type linearRadius
//
// Type linearArea:
//
// There is also an embolus that disolves 
// roughly proportional to surface area
// following the in vivo data from 
// "Theoretical and experimental intravascular gas
// embolism absorption dynamics" by A.B. Branger and D.M.Eckmann in
// J. Appl. Physiol vol 87, pp1287-1295 (1999).
//
// A fit to the in-vivo data in Fig. 2 yields the following function:
// V(t) = c * (a-t) ^ b         (in nl. N.B. 1nl = 10^{-9} m^3, t is in mins)
// assuming that t is the input time for a 2.8nl (nanolitre) embolus.
// Note that this is an effective radius, computed using
// r = (3 V(t) / 4 pi ) ^ (1/3). A more sophisticated model would be needed
// to determine how the sausage shape forms 
// to get a more accurate measure of which
// branch of the tree 
//
// with
// a               = 24.2418          +/- 0.3578       (1.476%) (in minutes)
// b               = 2.00539          +/- 0.04699      (2.343%)
// c               = 0.00463395       +/- 0.0008206    (17.71%)
//
// Note that this isn't really any worse than their parameterisation because
// this comes direct from experimental data, and doesn't have the unphysical
// kink that can be seen in their theory
// N.B. Have changed variable a to embolusLifetime
//      embolusLifetime is measured in mins
//--------------------------------------------------------------
// Adding linearAreaEckmann
// 
// Use the parameterisation of the theoretical model (Eq. 19)
//
// T_X = 2m_X \pi^{1/3}(V_0)^(2/3)(2 + X)(4/3 + X)^{-2/3}
//
// This gives the lifetime. Since the volume / lifetime relation
// is well parameterised, can then use this relation with
// (T_X - t_elapsed) to calculate the volume.
//
// The parameters m_X are given as:
//
// m_0 = 97.5 min/mm^2
// m_2.6 = 130.9 min/mm^2
//
// Start by just considering the dissolution of a
// spherical embolus, but could introduce linear interpolation
// of m_X between the two know values for added realism
//--------------------------------------------------------------

enum EmbolusType {
  linearRadius,
  linearArea,
  linearAreaEckmann
};

enum EmbolusComposition {
  embolusGas,
  embolusSolid,
  embolusCO2
};

class Embolus {

 private:
  
  bool isDeformable;

  double pi;

  double embolusLifetime, b, c;

  double delta_time;

  double timeElapsed;

  double initialRadius;  // Initial radius of the embolus
  double dissolveRate;   // dissolve rate if there is a linear dissolution

  double radius;         // Current radius of the embolus

  double volume;         // Current volume of the embolus

  double aspectRatio;
  double m_aspectRatio;

  // N.B. A linear dissolution corresponds to a volume dissolve rate which is
  // proportional to surface area and assuming a spherical, cylindircal or hybrid
  // shape.

  int embolusType;

  int embolusComposition;

  double comp_fac;

 public:

  Embolus() {

    comp_fac=1.0; // comp_fac is used for faster dissolving emboli such as CO2

    delta_time = 1.0;
    timeElapsed = 0.0;
    embolusType = linearRadius;
    embolusComposition = embolusGas;
    isDeformable = false;

    // Gradient for the Eckmann parameterisation

    aspectRatio = 0.0;    // Following parameter applies to aspect ratio 0
    m_aspectRatio = 97.5; // min/mm^2

    if (embolusComposition == embolusGas) {
      embolusLifetime=24.2418;
      b=2.00539;
      c=0.00463395;
    } else if (embolusComposition == embolusCO2) {
      embolusLifetime=24.2418/20.0;
      b=2.00539;
      c=std::pow(20.0,b)*0.00463395;
      comp_fac=20.0; // CO2 emboli dissolve 20 times faster
    } else {
      // For solid emboli, dissolve rate of 0.1mm/hour
      // Convert to mm/sec.

      dissolveRate = 0.1/3600.0;
    }

    pi = std::acos(-1.0);

  }

  int location;
  //double ratio;

  bool isAdvance(double vesselSize,double vesselPressure) {
    // This returns true if the embolus is allowed to advance
    return !isStuck(vesselSize,vesselPressure);
  }

  bool isDissolved() {
    if (timeElapsed > (embolusLifetime * 60.0) 
	&& (embolusType == linearArea || embolusType == linearAreaEckmann)) 
      return true;
    if (radius < 0 && embolusType == linearRadius) return true;
    return false;
  }

  bool isStuck(double vesselSize,double vesselPressure) {

    // Embolus never stuck if radius is smaller than
    // vessel radius

    if (radius < vesselSize) return false;

    if (!isDeformable) {
      if (radius > vesselSize) return true;
    } else {
      // Compute volume for stiction assuming that the
      // ends of the embolus form hemispheres.
      // Effective volume in m^3

      double effVolume = 1e-9 * 4.0*pi*(std::pow(radius,3.0) 
					- std::pow(vesselSize,3.0))/3.0;

      // The stiction coefficient in N/m^2, K = 10
      // this is 100 dyne/cm^2. 1 dyne = 1x10^-5 N, 1 cm = 10^-2 m

      double stictionCoefficient = 10.0;

      // Stiction length in m

      double stictionLength = 1e6 * effVolume / (pi*vesselSize*vesselSize);

      double stictionForce = stictionCoefficient * 2.0 * pi * 1e-3 * vesselSize * stictionLength;

      double stictionLimitPressure = 1e6 * stictionForce / pi / std::pow(vesselSize,2.0);

      if (stictionLimitPressure > vesselPressure) return true;

    }
    return false;
  }

  void decrement() {
    timeElapsed += delta_time;
    if (embolusType == linearRadius) {
      radius-=dissolveRate;
    } else if (embolusType == linearArea) {

      // N.B. In this expression, require t in mins, not secs

      volume = c * std::pow(embolusLifetime - timeElapsed / 60.0, b);
     
      // currently the volume is in nl. 1000nl = 1mm^3

      volume *= 1e-3;

      // currently the volume is in mm^3

      radius = 1.0e3 * std::pow(3.0e-9 * volume / 4.0 / pi, 1.0/3.0); 

      // radius now in mm

    } else if (embolusType == linearAreaEckmann) {

      volume = std::pow(std::pow(comp_fac*(embolusLifetime-timeElapsed / 60.0)
				 /(4.0+2.0*aspectRatio)/m_aspectRatio,3.0)
			/ pi,0.5)
	*(4.0/3.0+aspectRatio);

      // According to dimensional analysis of Eckmann eq (19), 
      // volume is in mm^3

      radius = std::pow(3.0 * volume / 4.0 / pi, 1.0/3.0); 

      // radius is now in mm

    }

    return;
  }

  void setDissolveRate(double one) {
    dissolveRate = one;
  }

  void setInitialRadius(double one) {
    initialRadius = one;
    radius = one;

    // std::cout << "SETTING INITIAL RADIUS " << embolusType << std::endl;

    volume = 1e-9 * 4.0 * pi * std::pow(initialRadius,3.0) / 3.0;
    
    // currently, the volume is in m^3
    
    // Convert to mm^3
    
    volume = 1e9 * volume;
    
    if (embolusType == linearArea) {
      // the value of a needs modifying to reflect the dissolve time of the
      // embolus, which may be bigger or smaller than the original one

      // Require the volume in nl, 1mm^3 = 1000nl
      
      volume *= 1e3;
      
      // currently the volume is in nl

      // N.B. for fit, embolusLifetime is parameter a

      embolusLifetime = volume / c;
      
      embolusLifetime = std::pow(embolusLifetime,1.0/b);

      // std::cout << "checking a " << a << std::endl;

      // exit(1);
      
    } else if (embolusType == linearAreaEckmann) {

      // Volume here is required in mm^3

      // T_X = 2m_X \pi^{1/3}(V_0)^(2/3)(2 + X)(4/3 + X)^{-2/3}

      embolusLifetime = 2.0*m_aspectRatio*std::pow(pi,1.0/3.0)
	*std::pow(volume,2.0/3.0)
	*(2.0+aspectRatio)*std::pow(4.0/3.0+aspectRatio,-2.0/3.0);

      // CO2 emboli dissolve 20 times faster

      embolusLifetime/=comp_fac;

    }
  }

  double getRadius() {
    return radius;
  }

  void setLinearRadius() {
    embolusType = linearRadius;
  }

  void setLinearArea() {
    // std::cout << "SETTING EMBOLUS DISSOLVE LINEAR IN AREA" << std::endl;
    embolusType = linearArea;
  }

  void setEmbolusSolid() {
    embolusComposition = embolusSolid;
  }

  void setEckmannParameterisation() {
    embolusType = linearAreaEckmann;
  }

  void setDeformable(bool one) {
    isDeformable = one;
  }

  double readEmbolusLifetime() {
    return embolusLifetime;
  }

  double readVolume() {
    return volume;
  }

};

#endif
