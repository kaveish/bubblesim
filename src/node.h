#ifndef NODE_H
#define NODE_H

#include <vector>

class Node {

 public:

  Node() {
    isEmbolus = false;
    isBlocked = false;
    isLastNode = false;

    nCompromised = 20;

    isCompromised.resize(nCompromised);

    for (int i=0; i<nCompromised; i++) {
      isCompromised[i] = false;
    }

    flow = 0.0;          // Flow in ml / s

    resistance = 0.0;       // Resistance of segment
    resistanceBelow = 0.0;  // Total resistance of whole tree below segment

    orientation = 0.0;   // Orientation of node to vertical

    pressure = 0.0;  // Pressure of the top end of the tube

    power = 0.0;   // Power loss in segment

    xLoc = 0.0;          // Position in mm
    yLoc = 0.0;          // Position in mm
    zLoc = 0.0;          // Position in mm
  }

  Node operator = (const Node& other) {
    this->isEmbolus = other.isEmbolus;
    this->isBlocked = other.isBlocked;
    this->isCompromised = other.isCompromised;
    this->pressure = other.pressure;
    this->resistanceBelow = other.resistanceBelow;
    this->resistance = other.resistance;
    this->power = other.power;
    this->size = other.size;
    this->length = other.length;
    this->treeLevel = other.treeLevel;
    this->daughterA = other.daughterA;
    this->daughterB = other.daughterB;
    this->location = other.location;
    this->parent = other.parent;
    this->isLastNode = other.isLastNode;
    this->xLoc = other.xLoc;
    this->yLoc = other.yLoc;
    this->zLoc = other.zLoc;
    this->flow = other.flow;
    return *this;
  }

  bool isEmbolus;     // True if an embolus is located at the node
  bool isBlocked;     // True if the node is blocked

  // True if the node has been blocked for too long - permanent
  // neurocognitive decline.

  int nCompromised;

  std::vector<bool> isCompromised; 

  float pressure;        // Pressure at the bifurcation
  float resistanceBelow; // Resistance of the pipe above the bifurcation and all vessels below (computed with Pouiselle flow)

  float resistance;     // Resistance of the pipe above the bifurcation

  float power;          // Power dissipated in the node

  float size;    // This is the diameter of the parent tube feeding the node

  float length;  // This is the length of the parent tube feeding the node

  int treeLevel; // Index relating to how far down the tree the node is found
  int daughterA; // Index to find the next node in the A direction
  int daughterB; // Index to find the next node in the B direction
  int location;  // Index of this node in the array
  int parent;
  bool isLastNode;

  float xLoc;    // x, y and z locations of the node
  float yLoc;
  float zLoc;

  float orientation; // Orientation of node to vertical

  float flow;   // Total flow thought the parent vessel
  
  //*****************************************
  // Set the blockage state,
  // checking if the blockage state is
  // changed. Useful to establish if the
  // tree state changed such that all tree
  // flows and pressures need recalculating.
  // N.B. In c++, ^ operator is exclusive or.
  //*****************************************

  bool setBlocked(bool newBlocked) {
    bool stateChange = newBlocked ^ isBlocked;
    isBlocked = newBlocked;
    return stateChange;
  }

  void setCompromised(int i, bool one) {
    isCompromised[i] = one;
  }

};

#endif
