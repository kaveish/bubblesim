//
//  The header file "nr.h" which contains prototypes of all
//  routines from "Numerical Recipies in C"
//
#ifndef _NR_H_
#define _NR_H_

float bessi(int n, float x);
float bessi0(float x);
float bessi1(float x);
float bessj(int n, float x);
float bessj0(float x);
float bessj1(float x);
float bessk(int n, float x);
float bessk0(float x);
float bessk1(float x);
float bessy(int n, float x);
float bessy0(float x);
float bessy1(float x);
double expdev(long *idum);
void psdes(unsigned long *lword, unsigned long *irword);
double ran1(long *idum);
double ran2(long *idum);
float ran3(long *idum);
float ran4(long *idum);

#endif /* _NR_H_ */
