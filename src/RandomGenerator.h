#ifndef RANDOMGENERATOR_H
#define RANDOMGENERATOR_H

#define NTAB 32

class RandomGenerator {

 public:
  // note #undef's at end of file 
  long IM1;
  long IM2;
  double AM;
  long IMM1;
  long IA1;
  long IA2;
  long IQ1;
  long IQ2;
  long IR1;
  long IR2;
  long NDIV;
  double EPS;
  double RNMX;
  
  long idum;
  long idum2;
  long iy;
  
  long iv[NTAB];
  
  RandomGenerator(long seed)
  {

    IM1=2147483563;
    IM2=2147483399;
    IA1=40014;
    IA2=40692;
    IQ1=53668;
    IQ2=52774;
    IR1=12211;
    IR2=3791;
    EPS=1.2e-9;

    RNMX=(1.0-EPS);
    IMM1=(IM1-1);
    NDIV=(1+IMM1/NTAB);
    AM=(1.0/(double)IM1);
    
    idum=seed;
    idum2=123456789;
    
    iy=0;
 
  }
  
  double nextDouble()
  {
    int j;
    long k;
    double temp;
    
    if (idum <= 0) {
      if (-idum < 1) {
	idum=1;
      } else {
	idum = -idum;
      }
      idum2=idum;
      for (j=NTAB+7;j>=0;j--) {
	k=(idum)/IQ1;
	idum=IA1*(idum-k*IQ1)-k*IR1;
	if (idum < 0) idum += IM1;
	if (j < NTAB) iv[j] = idum;
      }
      iy=iv[0];
    }
    k=(idum)/IQ1;
    idum=IA1*(idum-k*IQ1)-k*IR1;
    if (idum < 0) idum += IM1;
    k=idum2/IQ2;
    idum2=IA2*(idum2-k*IQ2)-k*IR2;
    if (idum2 < 0) idum2 += IM2;
    j=(int)(iy/NDIV);
    iy=iv[j]-idum2;
    iv[j] = idum;
    if (iy < 1) iy += IMM1;
    if ((temp=AM*iy) > RNMX) return RNMX;
    else return temp;
  }
};

#endif
