#ifndef PARAMETERS_H
#define PARAMETERS_H

// #include "Lattice.h"
// #include "Action.h"

#include "ConvergenceType.h"

// #include "Action.h"

// Parameters of the system. Technically, exchange and dwave signs
// do not belong here, since they are determined in the appropriate
// classes (Bipolaron, Multipolaron etc.)

namespace prm {

  extern bool isExchange;
  extern bool isMinimalUpdate;
  extern bool isCorrelatedKinkInsertions;

  extern bool flagUpdateI, flagUpdateII, flagUpdateIII, flagUpdateIV;  
  extern bool flagUpdateV, flagUpdateVI, flagUpdateVII, flagUpdateVIII;  

  extern double           commonExchangeProbability;
  extern double           exchangeProbability;
  extern double           globalShiftProbability;
  extern double           polaronUpdateRatio;
  
  extern  int             globalShiftMaxDistance;

  extern int              period;           // Period for periodic BCs
  extern int 		  ndim;             // No. dim. in Bravais lattice
  extern int 		  ndimspace;        // No. of spacial dimensions
  // extern Lattice*         plattice;         // Pointer to instance of lattice
  extern int              wallTimeMinutes;
  
  extern int              nParticles;       // Total number of particles
  extern int              nSpin;            // Total number of spin magnitudes

  extern double           tolerance;        // Required accuracy
  extern double 	  beta;             // Inverse temperature
  extern double		  omega;            // Phonon frequency
  extern double	 	  lambda;           // Effective electron-phonon coupling
  extern double           onsitecoulomb;    // Hubbard U
  extern double           offsitecoulomb;   // Extended Hubbard V
  extern double           instAuxPotMag;    // Auxiliary Coulomb potential
  extern double           extpotmag;        // Magnitude of external potential
  extern double           extpotmagPrime;   // Magnitude of external potential
  extern double           auxpotmag;        // Magnitude of auxiliary potential
  extern double           intRange;         // Range of polaron screening
  extern double           tPrime;           // In case of anisotropy or nn hopping
  extern double           gammaFactor;      // Measure of interaction range
  extern double           normalFactor;     // Measure of interaction range
  
  extern double           auxiliaryWeightingParameter;

  extern double           correlatedKinksParameter;

  extern bool             isInfiniteHubbardU;

  extern int             flagMeasureTotalEnergy;
  extern int             flagMeasurePotentialEnergy;
  extern int             flagMeasureKineticEnergy;
  extern int             flagMeasureHubbardEnergy;
  extern int             flagMeasureEffectiveMass;
  extern int 		 flagMeasureNumberOfPhonons;
  extern int 		 flagMeasureSize;
  extern int             flagMeasureProbabilityDensity;
  extern int 		 flagMeasureIsotopeExponent;
  extern int		 flagMeasureDensityOfStates;
  extern int             flagMeasureSpectrum;
  extern int             flagMeasureSpectrumReal;
  extern int             flagMeasureTriplet;
  extern int             flagMeasureDWave;
  extern int             flagMeasureFermions;

  extern bool            flagCalculatePhi;

  extern bool            flagOutputTerminal;
  
  extern clock_t                   parameterSetStartTime;
  extern clock_t                   currentTime;

  // extern EnergyScale               energyScale;

  // extern LatticeType               latticeType;
  
  // extern BoundaryConditions        boundaryConditions;
  // extern ExternalPotential         externalPotentialType;
  // extern AuxExternalPotential      auxExternalPotentialType;
  // extern InstantaneousPotential    instantaneousPotentialType;
  // extern AuxInstantaneousPotential auxInstantaneousPotentialType;
  // extern TimeBoundaryConditions    timeBoundaryConditions;
  extern ConvergenceType           convergenceType;
  // extern InteractionType           interaction;

  // extern SimulationType            simulationType;

  // extern AuxiliaryWeighting        auxiliaryWeighting;
  
  // Parameters of the run
  
  extern int		  nheat;
  extern int		  nseries;
  extern int	          nmeas;
  
  // Service variables
  
  extern std::ofstream 	  outMain;
  extern long*    	  idum;
  extern int              runIdentifier;
  extern int              subRunIdentifier;
  
  // Service variables for MPI
  
  extern int              ierr;
  extern int              processorNumber;
  extern int              processorTotal;
  
  // Constants for use throughout code

  extern int              zero,one,two,three;

}

#endif         //  PARAMETERS_H
