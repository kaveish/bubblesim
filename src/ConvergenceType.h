#ifndef CONVERGENCETYPE_H
#define CONVERGENCETYPE_H

enum ConvergenceType {
  ConvergenceError,
  ConvergenceSeries,
  ConvergenceTime,
  ConvergenceBootstrap,
  ConvergenceBootstrapSeries
};

#endif
