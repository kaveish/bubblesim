//***************************************************************************
// Accumulator.h                                                             
// Header of 'Accumulator' class                                             
// Copyright (C) 2002-2006 Pavel Kornilovich.  All right reserved            
// Mountain View, CA                                                         
// Copyright (C) 2005-2006 James P. Hague.  
// Loughborough University
//***************************************************************************

#ifndef ACCUMULATOR_H
#define ACCUMULATOR_H

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <string>
#include <sstream>

#include "parameters.h"

class Accumulator {
  
 protected :
    
  // Data associated with the weighting factor
    
  double internalWeighting;

  bool          flagWeightingFactor;
  
  // Data associated with the global structure of the accumulator
  
  bool isDumpFlag;
  
  int	        numberOfSeries;          // Total number of series
  int		nseriesCompleted;        // Number of complete single length
  int           ndoubleseriesCompleted;  // Number of complete double length

  // Data associated with the current series of measurements
  
  int	        seriesLength; // The series length
  int 		nreadings;    // Total number of readings


  double        currentExternalSum;
  double 	currentSeriesSum;
  double        currentWeightingSum;

  double        lastSeriesSum;
  double        lastWeightingSum;
  
  double        localSum;
  double        localSquaredSum;
  double        localDoubleSum;
  double        localDoubleSquaredSum;

  double        weightingSum;
  double        weightingSquaredSum;
  double        weightingDoubleSum;
  double        weightingDoubleSquaredSum;

  double        weightingCovarienceSum;

  double        externalSum;
  double        externalSumSquared; // Reserved for calculating covarience

  double        localTolerance;

  double        lastSeriesAverage;
  double        lastWeightingAverage;

  double        histogramMin;
  double        histogramMax;
  int           histogramSize;
   
  std::vector<double> measurementRecord;  
  std::vector<double> weightingRecord;

  std::vector<int> histogram;

  std::ofstream outFile;

 public :  
  
  // Construtors and destructors

  Accumulator& operator= (const Accumulator& right);

  // const Accumulator& operator= (const Accumulator& right);
  
  Accumulator();
  Accumulator(int);
  Accumulator(int, int);
  Accumulator(const Accumulator&);
  ~Accumulator();

  void setSeriesLength(int);
  void setLocalTolerance(double);
  void setWeightingFactor(double);

  // Set up a histogram with minimum, maximum and number of bins

  void setHistogram(double,double,int);

  // Useful functions
  
  void          addNewCovarienceReading(double&,double&);
  void		addNewReading(double&);
  void		addNewReading(int&);
  void		addNewReading(bool&);
  // double 	getCurrentSeriesAverage();
  double 	getAverageOverSeries();
  double 	getAverageOverSeriesNoWeighting();
  double 	getAverageWeighting();
  double 	getStandardDeviation();
  double 	getStandardDeviationSeries();
  double 	getDeviationWeighting();
  double 	getCovarienceWeighting();

  double      bootstrap(std::vector<double> &dataValues);
  double      bootstrapCovariance(std::vector<double> &dataValuesOne,
				  std::vector<double> &dataValuesTwo);
  double      bootstrapCovarianceErrorEst(std::vector<double> &dataValuesOne,
					  std::vector<double> &dataValuesTwo);
  int           selectRandomValue(int);

  double 	getStandardDeviationDouble();
  double        getStandardDeviationCovarient();
  double        getStandardDeviationCovarientSq();
  double        getLastSeriesAverage();
  void		setDataToZero();
  bool          isConverged();
  void          setDumpFlag();
  int           estimatedIterations();

  // In the event that there is a sign associated with the measurement

  double        getStandardDeviationWeighted(Accumulator &);
  double        getAverageOverSeriesWeighted(Accumulator &);  
  bool          isConvergedWeighted(Accumulator &);

  // In the event that the log of the measurement should be taken

  double        getStandardDeviationLogBeta();
  double        getAverageOverSeriesLogBeta();  
  bool          isConvergedLogBeta();

  // Functions for writing to screen
  
  std::string printScr();
  std::string printScrLogBeta();  
  std::string printScrWeighted(Accumulator &sign);

  // Functions for writing to file

  void printFile(std::ofstream &dataStream);
  void printFileLogBeta(std::ofstream &dataStream);
  void printFileWeighted(Accumulator &sign, std::ofstream &dataStream);
};


#endif   // ACCUMULATOR_H
