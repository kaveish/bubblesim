//***************************************************
// Embolic stroke simulation
// (C) J.P. Hague, 2010-2012, The Open University
//------------------------------------
// First modification, Feb 2nd 2010.
// The code is now modified to accept an input file 
// with embolization times and sizes for comparison
// with operations.
//
// Current modification, Sep 21st 2011
// Code modifications for gaseous emboli:
// Allow deformable gaseous emboli with
// stiction coefficient from experiment see:
// Anesthesiology 2003; 99:400–8
// Embolism Bubble Adhesion Force 
// in Excised Perfused
// Microvessels
// Akira Suzuki, David M. Eckmann,
//
// Include ischemia index: Thresholds 
// of 3 min (reversable ischemia),
// 30 mins (partially reversable)
// up to 3 hours 
// (irreversable reversable)
//
// Strategy for model tests
//-------------------------
// looking for threshold
// -------------------------------------

#include <cmath>
#include <iostream>
#include <fstream>
#include <ctime>

#include "embolus.h"
#include "node.h"
#include "Accumulator.h"
#include "RandomGenerator.h"
#include "ConvergenceType.h"

// Parameters used by the accumulator class
// and the random number generator

namespace prm {
  double tolerance;
  double beta;             // This does nothing in this code
  int wallTimeMinutes;
  int nmeas;
  int nseries;
  long*    	  idum;
  clock_t                   parameterSetStartTime;
  clock_t                   currentTime;
  ConvergenceType           convergenceType = ConvergenceError;
}

// Parameters specifically used by 

namespace eprm {

  double pressureMmHg = 100.0;

  bool isMeasureBeta = false;  // Set to true to measure order param

  bool isWatchEmbolus = false; // Set to true to watch the embolus
  
  bool isExternalFile = true; // Set to true if intending to read from external file
  
  bool isGasBubble = true;     // Set to true to use the new gas absorption formula
  bool isEmbolusDeformable = true;
  bool isGasBubbleEckmann = true;

  // Set to true to write an output file with the history of blockages
  
  bool isMeasureHistory = false;

  // set to true if measuring the percentage over a finite number
  // of time steps

  bool isMeasurePerc = true;

  // Constants to handle automated embolus introduction

  double embRate;              // Embolus introduction rate
  double embSizeMax;           // Maximum embolus size if stochastic
  double embSizeMin;           // Minimum embolus size if stochastic
  double embDissolveRate;      // Embolus dissovle rate if stochastic

  int totalLevels;             // Total number of levels in the tree

  // If reading from an external file, this indexes the next embolus to be released

  int embNumber;               
  
  std::vector<int> embTime;
  std::vector<double> embSize;
  std::vector<double> embSizeDelta;

  int embolisationStartTime;
  int embolisationStopTime;
  
}

enum ModelType {
   LinearModel,
   NonlinearModel,
   ExtremeModel
};
   
ModelType modelType = LinearModel;

double computeResistance(int index, std::vector<Node> &gNodeVector);
void computeLength(int index, std::vector<Node> &gNodeVector);
void computeFlow(int index, std::vector<Node> &gNodeVector);
void computeFlowsAndResistances(std::vector<Node> &gNodeVector);
void writeTreeFlowsResistances(std::vector<Node> &nodeVector);

// Routine for initializing the tree

void initializeTree(std::vector<Node> &nodeVector, 
		    double bifurcationExponentMin, double bifurcationExponentMax,
		    double lastElement, int *startLastRow, int *finishLastRow,
		    int *totalLastRow,std::vector<double> &nodeSizes);

// Non linear weighting for emboli at bifurcations

double nonlinear(double ratio);

// Routine that attempts embolus insersion

void attemptEmbolusInsertion(std::vector<Embolus>&,int time_index);

// Routine that determines the proportion of blockages downstream

double flowDirectionAB(std::vector<Embolus> &embolusVector,
				 std::vector<Node> &nodeVector,
				 std::vector<double> &lastRowBlockage,
				 int emb_i, int startLastRow, bool directionA);

// Load a file with data from operations

void loadFile(std::string fileName,double cutOffSize);

// Write out specific embolus info if debugging

void outputEmbolusInfo(int emb_i,std::vector<Embolus> &embolusVector,
		       std::vector<Node> &nodeVector);

static double pi = 3.14159265359;

namespace gprm {
  double viscosity = 1.0;
  double pi = 3.14159265359;
  double gravityParam = 0.0;  // This must be between 0 and 1
  bool isFlowInformation = true;
  int permanentTime = 5*60;  // 1 hour for permanent damage?
}
			 		    
RandomGenerator randomNumber(-1247);

int main (int argc,char* argv[]) {

  int embolusVectorLastSize = 0;

  bool isStateChange, isStateChangeTimeStep;
  bool isStateChangeCompromised;

  std::string fileName;

  double result;
  
  int blockLength = 1000;

  prm::nmeas = blockLength;

  int nTimeCorrelatorConverged;
  
  int warmup = 0;
  
  Accumulator blockageTime;
  
  Accumulator blockagePercentage;

  int nComp = 20;

  std::vector<Accumulator> compromisedPercentage(nComp);
  
  Accumulator averageEmbolusNumber;
  
  Accumulator beta;

  Accumulator timeAverage;
  
  bool isMeasureTimeCorrelator = false;
  bool isConverged;
  bool isTimeCorrelatorConverged = true;
  
  // Set to true to empty all blockages before new blockage measurement
  
  bool isExtraSafe = false;    
  
  // int j;
  
  // Number of coarse grained points in history file (must be factor of 2)
  
  int nmeasblockage = 0;
  
  int coarseGrain = 128; 
  
  int daughterlocation;
  int emboluslocation;
  
  bool embolusRemoved = false;
  
  int intDummy;
  
  int branchlevel;
  
  int startLastRow = 0;
  int finishLastRow = 0;
  int totalLastRow = 0;
  
  int location;
  
  double betaMeasure;
  
  double proportionA,proportionB;
  
  double gravWeightA,gravWeightB;

  int startDaughter, finishDaughter;
  
  double dummy, dummy2;
  
  // (1) Add measurement for mutually exclusive probability average
  // (2) Add measurement for independent probability average
  // (3) Add measurement for inter-layer correlator
  // (N.B. 3: all inter-layer correlators need measureing to be certain that
  // they are all identical)
  
  std::cout << "Check embolus weighting" << std::endl;
  std::cout << "-----------------------" << std::endl;
  std::cout << "0.05 " << nonlinear(0.05) << std::endl;
  std::cout << "0.1 " << nonlinear(0.1) << std::endl;
  std::cout << "0.2 " << nonlinear(0.2) << std::endl;
  std::cout << "0.3 " << nonlinear(0.3) << std::endl;
  std::cout << "0.4 " << nonlinear(0.4) << std::endl;
  std::cout << "0.5 " << nonlinear(0.5) << std::endl;
  std::cout << "0.6 " << nonlinear(0.6) << std::endl;
  std::cout << "0.7 " << nonlinear(0.7) << std::endl;
  std::cout << "0.8 " << nonlinear(0.8) << std::endl;
  std::cout << "0.9 " << nonlinear(0.9) << std::endl;
  std::cout << "-----------------------" << std::endl;

  std::cout << "(1) Embolisation rate (1/sec)" << std::endl;
  std::cout << "(2) Dissolve rate (mm/sec) " << std::endl;
  std::cout << "(3) Maximum embolus size (mm)" << std::endl;		//same as min
  std::cout << "(4) Minimum embolus size (mm)" << std::endl;
  std::cout << "(5) Total time steps (secs)" << std::endl;			//150
  std::cout << "(6) Ensemble size" << std::endl;				//1
  std::cout << "(7) Percentage blocked time" << std::endl;			//150
  std::cout << "(8) Correlation time (Secs)" << std::endl;			//1
  std::cout << "(9) No. nodes used in time correlator measurement" << std::endl;	//1
  std::cout << "(10) Output file: percentage blocked" << std::endl;		//perc.txt etc.
  std::cout << "(11) Output file: time correlator" << std::endl;
  std::cout << "(12) Output file: space correlator" << std::endl;
  std::cout << "(13) Output file: equilibrium properties" << std::endl;	//new name for each set of parameters
  std::cout << "(14) Output file: node blockage history" << std::endl;
  std::cout << "(15) Total tree levels" << std::endl;			//18
  std::cout << "(16) Gamma" << std::endl;					//bifurcation exponent (2)
  std::cout << "(17) Root node size" << std::endl;				//1mm
  std::cout << "(18) Required accuracy" << std::endl;			// 0.03 (percentage)
  std::cout << "(19) Embolization start time" << std::endl;
  std::cout << "(20) Embolization stop time" << std::endl;
  if (eprm::isExternalFile)
    std::cout << "(21) Input file (turn off by isExternalFile=false in code)" << std::endl;
  std::cout << "----- Your parameters -----" << std::endl;

  for (int i=1; i<argc; i++)
    std::cout << "(" << i << ") " << argv[i] << std::endl;

  std::cout << "-----------------------------" << std::endl;

  if ((eprm::isExternalFile&&argc!=22)||(!eprm::isExternalFile&&argc!=21)) {
    std::cout << "Incorrect number of arguments \n";
    std::cout << argc-1 << std::endl;
    std::exit(1);
  }

  eprm::embRate = std::atof(argv[0+1]);
  
  if (eprm::embRate > 1.0) {
    std::cout << "Currently maximum one embolus per time step. Please reduce embolisation rate \n";
    std::exit(1);
  }
    
  // Read the parameters from the command line
  
  eprm::embDissolveRate = std::atof(argv[1+1]);
  eprm::embSizeMin = std::atof(argv[2+1]);
  eprm::embSizeMax = std::atof(argv[3+1]);

  if (eprm::isEmbolusDeformable)
    std::cout << "**** Embolus is deformable ****" << std::endl;
  else
    std::cout << "**** Embolus is not deformable ****" << std::endl;

  if (eprm::isGasBubble)
    std::cout << "**** Gas bubble ****" << std::endl;
  else
    std::cout << "**** Solid embolus ****" << std::endl;

  if (eprm::isGasBubbleEckmann)
    std::cout << "**** Using Eckmann parameterisation ****" << std::endl;
  else
    std::cout << "**** Not using Eckmann parameterisation ****" << std::endl;

  std::cout << "--------------------" << std::endl;

  std::cout << "Input pressure (mm Hg) = " << eprm::pressureMmHg << std::endl;
  
  std::cout << "--------------------" << std::endl;

  std::cout << "Check embolus max timescale:" << std::endl;

  Embolus testEmbolus;

  if (eprm::isEmbolusDeformable) {
    testEmbolus.setDeformable(true);
  } else {
    testEmbolus.setDeformable(false);
  }
  
  if (eprm::isGasBubble) {
    testEmbolus.setLinearArea();
    if (eprm::isGasBubbleEckmann) {
      testEmbolus.setEckmannParameterisation();
    }
  } else {
    testEmbolus.setLinearRadius();
    testEmbolus.setDeformable(false);
  }
  
  
  testEmbolus.setInitialRadius(eprm::embSizeMax);
  
  std::cout << "Embolus volume = " << testEmbolus.readVolume() << std::endl;
  std::cout << "Embolus lifetime = " << testEmbolus.readEmbolusLifetime() << std::endl;
  
  int totalTimeSteps = (int)std::atof(argv[4+1]);
  int ensembleSize = (int)std::atof(argv[5+1]);
  int percTimeSteps = (int)std::atof(argv[6+1]);
  int correlationTime = (int)std::atof(argv[7+1]);
  int noNodes = (int)std::atof(argv[8+1]);
  
  // This is total number of layers in tree    

  dummy = atof(argv[14+1]);
  std::cout << "dummy " << dummy << std::endl;

  eprm::totalLevels = (int)dummy;
  
  double bifurcationExponentMax = std::atof(argv[15+1]); 
  double bifurcationExponentMin = std::atof(argv[15+1]);
  
  // Initialise the node sizes:
  
  double lastElementDouble = std::atof(argv[16+1]); // Top node in mm
  
  prm::tolerance = std::atof(argv[17+1]);
  
  eprm::embolisationStartTime = (int)std::atof(argv[18+1]);
  eprm::embolisationStopTime = (int)std::atof(argv[19+1]);

  std::cout << "Parameters ..." << std::endl;
  std::cout << "--------------" << std::endl;
  std::cout << "Minimum embolus size (mm): " << eprm::embSizeMin << std::endl;
  std::cout << "Maximum embolus size (mm): " << eprm::embSizeMax << std::endl;
  std::cout << "Embolisation rate (Emboli / sec) : " << eprm::embRate << std::endl;
  std::cout << "Dissolve rate (mm / sec) : " << eprm::embDissolveRate << std::endl;
  if (totalTimeSteps > 0) {
    std::cout << "Total time steps (secs) : " << totalTimeSteps << std::endl;
  } else {
    std::cout << "Total time steps (secs) : Unlimited" << std::endl;
  }
  std::cout << "Measure percentage for " << percTimeSteps << " time steps (secs)" << std::endl;
  std::cout << "Ensemble size : " << ensembleSize << std::endl;
  std::cout << "Correlation time (secs) : " << correlationTime << std::endl;
  
  std::cout << "Total tree levels " << eprm::totalLevels << std::endl;
  
  if (correlationTime > 2)
    isMeasureTimeCorrelator = true;
  
  std::cout << "Output file: percentage blocked, " << argv[9+1] << std::endl;
  std::cout << "Output file: time correlator, " << argv[10+1] << std::endl;
  std::cout << "Output file: space correlator, " << argv[11+1] << std::endl;
  std::cout << "Output file: equilibrium properties, " << argv[12+1] << std::endl;
  std::cout << "Output file: node blockage history, " << argv[13+1] << std::endl;
  
  std::cout << "---------------------\n";
  std::cout << "Derived parameters...\n";
  std::cout << "---------------------\n";
  std::cout << "Maximum embolus lifetime (sec): " << eprm::embSizeMax/eprm::embDissolveRate << std::endl;
  std::cout << "Minimum embolus lifetime (sec): " << eprm::embSizeMin/eprm::embDissolveRate << std::endl;
  std::cout << "Maximum embolus lifetime (hours): " << eprm::embSizeMax/eprm::embDissolveRate/3600.0 << std::endl;
  std::cout << "Minimum embolus lifetime (hours): " << eprm::embSizeMin/eprm::embDissolveRate/3600.0 << std::endl;

  if (eprm::totalLevels == 0) {
    std::cout <<  "ERROR: Can't have tree with 0 levels - exiting" << std::endl;
    exit(1);
  }
  
  blockageTime.setSeriesLength(blockLength);
  blockageTime.setDataToZero();
  
  blockagePercentage.setSeriesLength(blockLength);
  blockagePercentage.setDataToZero();

  for (int time_comp = 0; time_comp < nComp; time_comp++) {
    compromisedPercentage[time_comp].setSeriesLength(blockLength);
    compromisedPercentage[time_comp].setDataToZero();
  }

  averageEmbolusNumber.setSeriesLength(blockLength);
  averageEmbolusNumber.setDataToZero();
  
  beta.setSeriesLength(blockLength);
  beta.setDataToZero();
  
  timeAverage.setSeriesLength(blockLength);
  timeAverage.setDataToZero();
  
  Embolus theEmbolus;
  
  // Make an array of nodes here
  
  // std::vector<Embolus> embolusVector(20);
  
  std::vector<Embolus> embolusVector(0);

  std::vector<Accumulator> timeCorrelator(correlationTime);
  std::vector<Accumulator> timeCorrelatorYY(correlationTime);
  std::vector<Accumulator> timeCorrelatorXY(correlationTime);
  
  for (int i = 0; i < correlationTime; i++) {
    // timeCorrelator.add(new Accumulator());
    // timeCorrelatorYY.add(new Accumulator());
    // timeCorrelatorXY.add(new Accumulator());
  }

  std::vector<double> percentageBlockedT(percTimeSteps);
  std::vector<double> percentageBlockedSqT(percTimeSteps);

  std::vector<std::vector<double> > percentageCompromisedT;
  std::vector<std::vector<double> > percentageCompromisedSqT;

  percentageCompromisedT.resize(nComp);
  percentageCompromisedSqT.resize(nComp);

  for (int time_comp = 0; time_comp < nComp; time_comp++) {
    percentageCompromisedT[time_comp].resize(percTimeSteps);
    percentageCompromisedSqT[time_comp].resize(percTimeSteps);    
  }

  double timeCorrelatorBlock;
  
  int nBlocks = 0;
  
  double timeAverageBlock = 0.0;

  double percentage;
  std::vector<double> percentageCompromised(nComp);
  std::vector<double> percentageCompromisedLast(nComp);
  
  std::vector<int> timeStepBlockage(correlationTime * noNodes);
  
  for (int time_comp=0; time_comp<nComp; time_comp++) {
    for (int i=0;i<percTimeSteps;i++) {
      percentageBlockedT[i]=0.0;
      percentageBlockedSqT[i]=0.0;
      percentageCompromisedT[time_comp][i]=0.0;
      percentageCompromisedSqT[time_comp][i]=0.0;
    }
  }
  
  // Reset the time correlator
  for (int i = 0; i < correlationTime; i++) {
    
    timeCorrelator[i].setSeriesLength(blockLength);
    // timeCorrelator[i].setTolerance(tolerance);
    timeCorrelator[i].setDataToZero();
	
    timeCorrelatorYY[i].setSeriesLength(blockLength);
    // timeCorrelatorYY[i].setTolerance(tolerance);
    timeCorrelatorYY[i].setDataToZero();
    
    timeCorrelatorXY[i].setSeriesLength(blockLength);
    // timeCorrelatorXY[i].setTolerance(tolerance);
    timeCorrelatorXY[i].setDataToZero();
    
    for (int inode = 0; inode < noNodes; inode++)
      timeStepBlockage[inode*correlationTime + i] = 0;
  }
  
  // Main initialisation is the node tree;
  
  // Initialise arrays with size eprm::totalLevels
  
  std::vector<double> percentageI(eprm::totalLevels);
  std::vector<Accumulator> blockagePercentageI(eprm::totalLevels);
  
  for (int i = 0; i < eprm::totalLevels; i++) {
    // blockagePercentageI.add(new Accumulator());
    blockagePercentageI[i].setSeriesLength(blockLength);
    // blockagePercentageI[i].setTolerance(tolerance);
    blockagePercentageI[i].setDataToZero();
  }
  
  // Initialise the node vectors - it is important to choose the size
  // of the tree in advance - resizing is *very* time consuming!
  
  // std::vector<Node> nodeVector((int)std::pow(2.0,eprm::totalLevels+1));
  
  std::vector<Node> nodeVector(0);  // Vector containing the tree

  //******************************
  // Initialize the tree structure
  //******************************

  std::vector<double> nodeSizes(0);  // Vector containing node sizes

  initializeTree(nodeVector, bifurcationExponentMin, 
		 bifurcationExponentMax, lastElementDouble,
		 &startLastRow, &finishLastRow, &totalLastRow,nodeSizes);
  
  if (eprm::isExternalFile)
    fileName = argv[20+1];  // Set the file name if reading from an external file

  // Load the file with the cutoff of last tree element

  loadFile(fileName,nodeSizes[nodeSizes.size()-1]);   // and load that file


  //******************************************************
  // Reset arrays involved in determining the blockage and 
  // correlation in the last row.
  //******************************************************
  
  std::vector<double> lastRowCorrelator(totalLastRow);
  std::vector<double> lastRowBlockage(totalLastRow);
  
  std::vector<int> lastRowBlockageI(eprm::totalLevels * totalLastRow);
  
  std::vector<int> lastRowBlockageTime(totalLastRow);
    
  // At this stage, the embolus vector is empty
  
  //*************************************************
  // Loop over the ensemble - total number of samples 
  // N.B. This should be something like 1000 if interested 
  // in the time evolution.
  // N.N.B. This should be 1 if only interested in the
  // equilibrium blockage, although need to make a modification 
  // to make the averages
  // May need an accumulator class
  //*************************************************
  
  for (int ens_i=0;ens_i<ensembleSize;ens_i++) {

    // Reset the compromised nodes and reorientate all nodes
    
    for (int i=0;i<nodeVector.size();i++) {
      for (int time_comp=0; time_comp<nComp; time_comp++) {
	nodeVector[i].setCompromised(time_comp,false);
      }
      nodeVector[i].orientation = 2.0 * pi * randomNumber.nextDouble();
    }
   
    for (int time_comp = 0; time_comp < nComp; time_comp++) {
      percentageCompromised[time_comp] = 0.0;
    }

    // Reset the embolus number if an external file is to be used
    // (reset it anyway even if not using an external file - it doesn't
    // cost anything!).

    eprm::embNumber = 0;

    //***************************************************
    // Clear out the blockages in the tree by brute force
    // and empty the embolus vector.
    //***************************************************
    
    isStateChange = false;
    for (int node_i=0; node_i<nodeVector.size(); node_i++)
      isStateChange |= nodeVector[node_i].setBlocked(false);
    embolusVector.clear();

    // Clear last row blockage information
    
    for (int i=0; i<totalLastRow; i++) {
      lastRowCorrelator[i] = 0.0;
      lastRowBlockage[i] = 0;
      lastRowBlockageTime[i] = 0;
      
      for (int k=0; k<eprm::totalLevels; k++) {
	lastRowBlockageI[k*totalLastRow + i] = 0;
      }
    }

    // After first initialisation of tree, likely that the 
    // flows and resistances need recomputing.

    if (isStateChange)
      computeFlowsAndResistances(nodeVector);

    // Start the run for the ith realization of the ensemble
    
    std::cout << "Beginning ensemble run " << ens_i << std::endl;
    
    // Loop over the total number of time steps in the simulation
    
    int time_i = 0;   // time_i is the time index of the whole simulation
    
    if (eprm::isMeasureHistory) {
      
      std::cout << "opening history file" << std::endl;
      
      std::ofstream outHist(argv[13+1]);
      outHist << "# Course grained history of blockage vs timestep,  \n";
      
      outHist.close();
		
      std::cout << "opened history file" << std::endl;
      
    }
    
    
    do { // Loop over time_i until converged
      
      for (int time_j = 0; time_j < blockLength; time_j++) {

	// Last size of the embolus vector

	embolusVectorLastSize = embolusVector.size();
	
	// Reset blockages on the node vector
	
	// REMOVE?

	// isStateChange = false;
	// for (int i=0;i<nodeVector.size();i++) {
	//   isStateChange |= nodeVector[i].setBlocked(false);
	// }
	// if (isStateChange)
	//   computeFlowsAndResistances(nodeVector);
	
	//*************************************************
	// Insert an embolus into the system with a certain 
	// probability per time step, or from file
	//*************************************************
	
	attemptEmbolusInsertion(embolusVector, time_i);
	
	// Check that there are currently emboli to operate on

	isStateChange = false;
	isStateChangeTimeStep = false;
	
	if (!embolusVector.empty()) {
	  
	  //******************************
	  // Destroy / remove spent emboli
	  //******************************
	  
	  // Loop over the possible emboli from top to bottom (so that elements can be removed without problems)

	  for (int emb_i=embolusVector.size()-1;emb_i>=0;emb_i--) {
	    
	    //********************************************************
	    // We may be watching the emboli as they traverse the tree
	    // To double check that the code is working
	    //********************************************************
	    
	    if (eprm::isWatchEmbolus)
	      outputEmbolusInfo(emb_i,embolusVector,nodeVector);
	      
	    //********************************************************
	    // Decrease the size of the embolus
	    // Note that the new radius of the embolus is computed at
	    // this point
	    //******************************************************** 
	    
	    embolusVector[emb_i].decrement();
	    
	    //***********************************
	    // Two possibilities for destruction:
	    //***********************************
	    
	    if ((embolusVector[emb_i].isAdvance(nodeVector[embolusVector[emb_i].location].size,nodeVector[embolusVector[emb_i].location].pressure))&&
		(nodeVector[embolusVector[emb_i].location].isLastNode)) {

	      // (I) The embolus is on the final layer of the tree, and is small enough to move
	      
	      embolusRemoved = true;
	      
	      if (eprm::isWatchEmbolus) {
		std::cout << "Embolus reached end of tree: removed" << std::endl;
	      }

	      isStateChange |= nodeVector[embolusVector[emb_i].location].setBlocked(false);
	      // embolusVector.remove(emb_i);
	      
	      embolusVector.erase(embolusVector.begin()+emb_i);

	    } else if (embolusVector[emb_i].isDissolved()) {

	      // (II) The embolus disolved before reaching the end of the tree
	      
	      embolusRemoved = true;
	      
	      if (eprm::isWatchEmbolus) {
		std::cout << "Embolus disolved: removed" << std::endl;
	      }

	      isStateChange |= nodeVector[embolusVector[emb_i].location].setBlocked(false);

	      // embolusVector.remove(emb_i);

	      embolusVector.erase(embolusVector.begin()+emb_i);
	    }
	  }
	}

	// Recompute state change after removal of embolus if necessary

	isStateChangeTimeStep |= isStateChange;

	if (isStateChange)
	  computeFlowsAndResistances(nodeVector);
	
	//************************************************************
	// Some emboli have now been destroyed, so
	// check again that there is actually an embolus to operate on
	//************************************************************
	
	if (!embolusVector.empty()) {
	  
	  //***************************************************
	  // Clear out the blockages in the tree by brute force
	  // Thought it might be slow, but turns out to 
	  // be sufficiently fast, and much safer!
	  //***************************************************

	  isStateChange = false;
	  
	  if (isExtraSafe) {
	    for (int node_i=0; node_i<nodeVector.size(); node_i++)
	      isStateChange |= nodeVector[node_i].setBlocked(false);
	    
	    if (isStateChange)
	      computeFlowsAndResistances(nodeVector);
	  }

	  isStateChangeTimeStep |= isStateChange;
	  
	  //**********************************************
	  // Determine which nodes are blocked before move
	  //**********************************************
	  
	  isStateChange = false;

	  for (int emb_i=0;emb_i<embolusVector.size();emb_i++) {
	    if (embolusVector[emb_i].isStuck(nodeVector[embolusVector[emb_i].location].size,nodeVector[embolusVector[emb_i].location].pressure)) {
	      isStateChange |= nodeVector[embolusVector[emb_i].location].setBlocked(true);
	      if (eprm::isWatchEmbolus) {
		std::cout << "Actual blockage in node " << embolusVector[emb_i].location << std::endl;
	      }
	    } else {
	      isStateChange |= nodeVector[embolusVector[emb_i].location].setBlocked(false);
	    }
	  }

	  isStateChangeTimeStep |= isStateChange;

	  if (isStateChange)
	    computeFlowsAndResistances(nodeVector);
	  
	  //****************************
	  // Determine which emboli move
	  //****************************

	  isStateChange = false;

	  // Loop over all the emboli

	  for (int emb_i=0;emb_i<embolusVector.size();emb_i++) {
	    
	    //***************************************************************
	    // If the embolus is less than the size of the node,
	    // or doesn't have stiction the embolus can move
	    //***************************************************************
	    if (embolusVector[emb_i].isAdvance(nodeVector[embolusVector[emb_i].location].size,nodeVector[embolusVector[emb_i].location].pressure)) {
		
	      //*************************************************************
	      // Determine the extent of nodes in the direction of daughter A
	      //*************************************************************

	      proportionA = flowDirectionAB(embolusVector, nodeVector, 
					    lastRowBlockage, emb_i,
					    startLastRow, true);

	      //*************************************************************
	      // Determing the extent of nodes in the direction of daughter B
	      //*************************************************************

	      proportionB = flowDirectionAB(embolusVector, nodeVector, 
					    lastRowBlockage, emb_i,
					    startLastRow, false);
	      
	      gravWeightA = 0.5*(gprm::gravityParam*std::cos(nodeVector[embolusVector[emb_i].location].orientation)+1.0);
	      gravWeightB = 1.0-gravWeightA;

	      //***************************************
	      // Now determine the motion of the emboli
	      //***************************************
	      
	      // std::cout << proportionA + " " + proportionB);
	      
	      if ((gravWeightB*proportionB>0.0)
		  ||(gravWeightA*proportionA>0.0)) {
		
		// Embolus moves if don't have all arteries blocked
		// Normally some arteries will be free
		
		double ratio = (gravWeightA*proportionA /(gravWeightA*proportionA + gravWeightB*proportionB));
		
		result=nonlinear(ratio);
		
		if (randomNumber.nextDouble() < result) {
		  // Embolus is about to move, so this node no longer blocked

		  isStateChange |= nodeVector[embolusVector[emb_i].location].setBlocked(false);
		  int daughter=nodeVector[embolusVector[emb_i].location].daughterA;
		  embolusVector[emb_i].location=daughter;
		} else {
		  // Embolus is about to move, so this node no longer blocked

		  isStateChange |= nodeVector[embolusVector[emb_i].location].setBlocked(false);
		  int daughter=nodeVector[embolusVector[emb_i].location].daughterB;
		  embolusVector[emb_i].location=daughter;
		}
		
		// }
	      } // The specific embolus has now been moved
	    } // End motion phase
	  } // End loop over embolus
	  
	  //*********************************************
	  // Check if the state of blockages in the tree
	  // changed. If so, then boolean variable 
	  // isStateChange will be true, so recompute the
	  // tree flows and pressures
	  //*********************************************

	  isStateChangeTimeStep |= isStateChange;

	  if (isStateChange)
	    computeFlowsAndResistances(nodeVector);

	  //***************************************************
	  // Clear out the blockages in the tree by brute force
	  //***************************************************

	  if (isExtraSafe) {

	    isStateChange = false;
	  
	    for (int node_i=0; node_i<nodeVector.size(); node_i++)
	      isStateChange |= nodeVector[node_i].setBlocked(false);
	    
	    if (isStateChange)
	      computeFlowsAndResistances(nodeVector);

	    isStateChangeTimeStep |= isStateChange;
	    
	  }

	  isStateChange = false;

	  //************************************************
	  // Determine which nodes are blocked after move.
	  // Pick up any change of state in the blockage
	  // config here.
	  // N.B. This looks very inefficient - should be
	  // able to check for new blockage immediately
	  // after moving an embolus. This should save
	  // making 2 sets of flow / resistance computations
	  // before and after the moves. However, may miss
	  // some pressure changes.
	  //************************************************
	  
	  for (int emb_i=0;emb_i<embolusVector.size();emb_i++) {
	    if (embolusVector[emb_i].isStuck(nodeVector[embolusVector[emb_i].location].size,nodeVector[embolusVector[emb_i].location].pressure)) {
	      isStateChange |= 
		nodeVector[embolusVector[emb_i].location].setBlocked(true);
	      if (eprm::isWatchEmbolus) {
		std::cout << "Actual blockage in node " 
			  << embolusVector[emb_i].location << std::endl;
	      }
	    } else {
	      isStateChange |= 
		nodeVector[embolusVector[emb_i].location].setBlocked(false);
	    }
	  }
	  
	} // end if (!embolusVector.empty())
	
	isStateChangeTimeStep |= isStateChange;

	if (isStateChange)
	  computeFlowsAndResistances(nodeVector);
	
	//*****************************************************************
	// Advance the queue of blockages used to determine time correlator
	//*****************************************************************
	
	for (int inode=0; inode<noNodes; inode++) {
	  for (int i = correlationTime-2; i>=0; i--) {
	    timeStepBlockage[inode * correlationTime + i + 1] 
	      = timeStepBlockage[inode * correlationTime + i];
	  }
	}
	
	
	//*********************************************************
	// Determine the locations of the blockages in the last row
	// N.B. This method is VERY inefficient. Since the tree level
	// and node number in that level were tracked, then the
	// start and end nodes of the blockage could be determined
	// immediately
	//*********************************************************
	
	// ** Reset array containing information on last row blockages ** 

	// EXPENSIVE. THE INFO RELATING TO lastRowBlockageI IS
	// ONLY USEFUL IN VERY SPECIFIC CIRCUMSTANCES, AND
	// SHOULD PROBABLY BE TURNED OFF NORMALLY. CERTAINLY, THERE IS
	// NO POINT IN DOING THIS IF THERE ARE NO EMBOLI! (BUT ENSURE
	// CORRECT SETTING OF MEASUREMENTS IF THERE ARE NO EMBOLI)

	if (!embolusVector.empty()) {	
	  
	  for (int node_i = 0; node_i < totalLastRow; node_i++) {
	    lastRowBlockage[node_i] = 0;
	    for (int level_j = 0; level_j < eprm::totalLevels; level_j++)
	      lastRowBlockageI[totalLastRow * level_j + node_i] = 0;
	  }
	}
	
	
	// ** Start loop over emboli **
	
	for (int emb_i=0;emb_i<embolusVector.size();emb_i++) {
	  
	  if (nodeVector[embolusVector[emb_i].location].isBlocked) {
	    
	    // ** Follow tree to left **
	    
	    daughterlocation = embolusVector[emb_i].location;
	    branchlevel = nodeVector[daughterlocation].treeLevel;
	    
	    for (int i=branchlevel;i<eprm::totalLevels-1;i++) {
	      daughterlocation=nodeVector[daughterlocation].daughterA;
	    }
	    
	    startDaughter=daughterlocation;
	    
	    // ** Follow tree to right **
	    
	    daughterlocation = embolusVector[emb_i].location;
	    
	    for (int i=branchlevel;i<eprm::totalLevels-1;i++) {
	      daughterlocation=nodeVector[daughterlocation].daughterB;
	    }
	    
	    finishDaughter=daughterlocation;
	    
	    // ** All nodes downstream from blockage are blocked **
	    
	    // std::cout << branchlevel);
	    
	    for (int node_i=startDaughter; node_i<=finishDaughter; node_i++) {
	      lastRowBlockage[node_i - startLastRow] = 1;
	      
	      lastRowBlockageI[node_i - startLastRow + totalLastRow * branchlevel ] = 1;
	    }
	  }
	  
	} // ** end loop over emboli **
	
	//********************************************************************
	// Determine the proportion of emboli on the last row that are blocked
	// and also update the timeStepBlockage array from which the time 
	// correlator is computed. Note that the lastRowBlockageI 
	//********************************************************************
	
	percentage = 0.0;
	
	for (int level_j = 0; level_j < eprm::totalLevels; level_j++) {
	  percentageI[level_j] = 0;
	}

	//********************************************************************
	// Note that there is no point in making percentage measurements
	// if there are no emboli in the system - with no emboli, no blockage!
	//********************************************************************

	isStateChangeCompromised = false;

	// N.B. If there were emboli at the beginning of the
	// iteration, or at the end, need to make measurements
	// (or at least zero the embolus blockage time vector)

	if (!embolusVector.empty() || (embolusVectorLastSize > 0)) {
	  
	  for (int node_i=0; node_i<totalLastRow; node_i++) {
	    
	    for (int level_j = 0; level_j < eprm::totalLevels; level_j++) {
	      if (lastRowBlockageI[node_i + level_j * totalLastRow] == 1) {
		percentageI[level_j] += 1.0/(double)totalLastRow;
	      }
	    }

	    if (lastRowBlockage[node_i] == 1) {
	      
	      percentage += 1.0/(double)totalLastRow;
	      lastRowBlockageTime[node_i] += 1;

	      // Convert this to make appropriate measure
	      // of the number of end nodes where cell death
	      // has occured.
	      
	      for (int time_comp=0; time_comp<nComp; time_comp++) {
		if (lastRowBlockageTime[node_i]>gprm::permanentTime*(time_comp+1)) {
		  if (!nodeVector[startLastRow+node_i].isCompromised[time_comp])
		    isStateChangeCompromised = true;
		  nodeVector[startLastRow+node_i].setCompromised(time_comp,true);
		}
	      }
	      
	    } else {
	      
	      // Make the average blockage time measurement
	      
	      if (time_i >= warmup) {
		
		if (lastRowBlockageTime[node_i] > 0) {
		  blockageTime.addNewReading(lastRowBlockageTime[node_i]);
		}
		
	      }

	      // Need to take care when doing this - may
	      // wind up with residual blockage time as this will
	      // not reset if the blockage stops because embolus vector
	      // is empty.
	      
	      lastRowBlockageTime[node_i] = 0;

	    }
	    
	  } // end Loop over last row nodes

	} // End conditional on filled embolus vector

	// Establish percentage of compromised end nodes
	// N.B. This measurement is slowing down the code a lot. 
	// Should only compute when there
	// has been a state change in the tree.
	
	if (isStateChangeCompromised) {

	  for (int time_comp = 0; time_comp < nComp; time_comp++) {
	    percentageCompromisedLast[time_comp] =
	      percentageCompromised[time_comp];
	    percentageCompromised[time_comp] = 0.0;
	  }
	  
	  for (int node_i=startLastRow; node_i<finishLastRow; node_i++) {
	    for (int time_comp=0; time_comp<nComp; time_comp++) {
	      if (nodeVector[node_i].isCompromised[time_comp]) {
		percentageCompromised[time_comp] += 1.0/(double)totalLastRow;
	      }
	    }
	  }

	  for (int time_comp = 0; time_comp < nComp; time_comp++) {
	    if (percentageCompromised[time_comp]<
		percentageCompromisedLast[time_comp]) {
	      std::cout << "Error, percentageCompromisedLast can't be greater than percentageCompromised" << std::endl;
	      exit(1);
	    }
	  }

	}
	
	//******************************************************
	// Fill up timeStepBlockage. Must be done at least
	// correlationTime steps before making a measurement.
	// For simplicity, measure on every time step.
	//******************************************************
	
	for (int node_i=0; node_i<noNodes; node_i++) {
	  if (!embolusVector.empty()) {
	    timeStepBlockage[node_i*correlationTime] 
	      = (int)lastRowBlockage[node_i * totalLastRow / noNodes];
	  } else {
	    timeStepBlockage[node_i*correlationTime] = 0;
	  }
	}
	
	
	//**************************************
	// Make measurements if warmup completed
	//**************************************
	
	if (time_i >= warmup) {
	  
	  //******************************
	  // Determine percentage averages
	  //******************************
	  
	  blockagePercentage.addNewReading(percentage);

	  for (int time_comp=0; time_comp<nComp; time_comp++) {
	    compromisedPercentage[time_comp].addNewReading(percentageCompromised[time_comp]);
	  }

	  double embolusNumber = (double)embolusVector.size();
	  
	  averageEmbolusNumber.addNewReading(embolusNumber);
	  
	  for (int level_j = 0; level_j < eprm::totalLevels; level_j++) {
	    blockagePercentageI[level_j].addNewReading(percentageI[level_j]);
	  }
	
	  //******************************************
	  // Determine inter level correlators (betas)
	  //******************************************
	  
	  // std::cout << "Start beta measurement");
	  
	  betaMeasure = 0.0;
	  
	  // The measurements over node_i should be spread out over the end nodes like the measurements for the
	  // time correlator. This should be done because this measurement takes eprm::totalLevels * eprm::totalLevels longer
	  // than measuring the percentage blockages on the end nodes, and the measurement can become a bottleneck
	  
	  int nBetaMeasure = 0;
	  
	  if (!embolusVector.empty() && eprm::isMeasureBeta) {

	    for (int node_i=0; node_i< totalLastRow; node_i = node_i + eprm::totalLevels * eprm::totalLevels) {
	      
	      nBetaMeasure++;
	      
	      for (int level_j = 0; level_j < eprm::totalLevels; level_j++) {
		for (int level_k = 0; level_k < eprm::totalLevels; level_k++) {
		  
		  if (level_j != level_k) {
		    betaMeasure += lastRowBlockageI[node_i + level_j * totalLastRow]
		      * lastRowBlockageI[node_i + level_k * totalLastRow]; 
		  }
		  
		  // Might want to look at the betas separately
		  //
		  //betaIJ[level_j + eprm::totalLevels * level_k] += lastRowBlockageI[node_i + level_j * totalLastRow]
		  //* lastRowBlockageI[node_i + level_k * totalLastRow];
		}
	      }
	      
	    }
	  
	    betaMeasure /= (double)(nBetaMeasure);
	  
	  }

	  beta.addNewReading(betaMeasure);
	  
	  // std::cout << "... finish beta measurement: " + betaMeasure);
	  
	  //********************************************
	  // Measurements related to the time correlator
	  //********************************************
	  
	  // Since timeStepBlockage only has values 0 and 1, timeCorrelator[0] has the same value as timeAverageBlock
	  
	  timeAverageBlock = 0.0;
	  for (int inode=0; inode<noNodes; inode++) {
	    timeAverageBlock += (double)timeStepBlockage[correlationTime * inode];
	  }
	  
	  dummy = (double)(timeAverageBlock / noNodes);
	  timeAverage.addNewReading( dummy );
	  
	  for (int i = 0; i < correlationTime; i++) {
	    
	    timeCorrelatorBlock = 0.0;
	    
	    for (int inode = 0; inode < noNodes; inode++) {
	      timeCorrelatorBlock += (double)(timeStepBlockage[inode * correlationTime]
					      * timeStepBlockage[inode * correlationTime + i]);
	    }
	    
	    dummy = (double)(timeCorrelatorBlock / noNodes);

	    timeCorrelator[i].addNewReading( dummy );

	    dummy = std::pow((double)(timeCorrelatorBlock / noNodes),2);

	    timeCorrelatorYY[i].addNewReading( dummy );

	    dummy = (double)(timeCorrelatorBlock / noNodes);

	    dummy2 = (double)(timeAverageBlock / noNodes);

	    timeCorrelatorXY[i].addNewCovarienceReading( dummy, dummy2 );
	    
	  }
	  
	} // End measurements at time > warmpup
	
	//******************************************
	// Measure the spacial correlator. 
	// This needs updating to use all the
	// nodes in the tree, and also to have 
	// a mapping from fractal space to real
	// space to make a realistic measurement of 
	// the extent of brain damage.
	//******************************************
	
	for (int i=0; i<totalLastRow; i++) {
	  if (!embolusVector.empty()) {
	    lastRowCorrelator[i] += (double)lastRowBlockage[0] * (double)lastRowBlockage[i];
	  } else {
	    lastRowCorrelator[i] = 0;
	  }
	}
	
	time_i++;
	
	//***********************************************
	// N.B. NO WARMUP FOR THE AVERAGES OVER ENSEMBLES
	//***********************************************
	
	//*********************************************************
	// Make the sums necessary to determine the
	// average blockage over ensembles, and its error.
	// Note that this must take place within the
	// loop over block as it should be computed every timestep
	//********************************************************
	
	if ((percTimeSteps) > 0 && (time_i < percTimeSteps)) {

	  // std::cout << "Instance no. " << ens_i << " Time step " << time_i << " Total emboli " << embolusVector.size() << std::endl;
	  
	  // std::cout << "Measuring percentage dead cells " << time_i 
	  //		    << " of " << percTimeSteps << std::endl;
	  
	  // if (time_i % 10 == 0)
	  // std::cout << "Percentage blocked last timestep " 
	  //	    << percentage*100.0 << " " 
	  //	    << percentageCompromised[0]*100.0 << std::endl;
	  
	  percentageBlockedT[time_i] += percentage / double(ensembleSize);
	  percentageBlockedSqT[time_i] += percentage * percentage / double(ensembleSize);

	  for (int time_comp=0; time_comp < nComp; time_comp++) {
	    percentageCompromisedT[time_comp][time_i] += percentageCompromised[time_comp] / double(ensembleSize);
	    percentageCompromisedSqT[time_comp][time_i] += percentageCompromised[time_comp] * percentageCompromised[time_comp] / double(ensembleSize);
	  }

	}
		
      } // End loop over block (time_j)
      
      //***********************************************************
      // This outputs data sufficient to construct a figure showing 
      // the time dependence of end node blockages. N.B. History 
      // should be written *EVERY* timestep.
      //***********************************************************
      
      if (eprm::isMeasureHistory) {	    
	
	std::ofstream outHist2(argv[13+1],std::ios::app);
	//std::cout << "Appending.." << std::endl;
	for (int node_i=0; node_i < coarseGrain; node_i++) {
	  double grain = 0.0;
	  //  outHist2 << time_i + " ";
	  
	  if (!embolusVector.empty()) {
	    for (int grain_i = 0; grain_i < totalLastRow / coarseGrain; grain_i++) {
	      if (lastRowBlockage[node_i * totalLastRow / coarseGrain + grain_i] == 1) {
		grain += 1.0;
	      }
	    }
	  }
	  
	  grain /= (double)(totalLastRow / coarseGrain);
	  outHist2 << grain << " ";
	  
	}
	
	outHist2 << "\n";
	outHist2.close();
	
	
      } // End history measurement
      
      if (time_i > warmup) {
	
	double timeAverageNormal = 0.0;
	double timeAverageNormalError = 0.0;
	
	double squaredTimeAverageNormal = 0.0;
	double squaredTimeAverageNormalError = 0.0;
	
	double timeCorrelator0Reading = 0.0;
	double timeCorrelator0Error = 0.0;
	
	double timeCorrelatorIReading = 0.0;
	double timeCorrelatorIError = 0.0;
	
	//**********************************************
	// Write out the time correlator every few steps
	//**********************************************
	
	//std::cout << "Writing time correlator" << std::endl;
	
	
	std::ofstream out(argv[10+1]);
	// This just causes problems when files are merged
	// out << "# time step, correlator, average,  \n";
	
	// Normalised "Time correlator" at time 0, with the standard error
	// The errors are complicated by significant correlations in the
	// time correlator.
	
	timeCorrelator0Reading
	  = timeCorrelator[0].getAverageOverSeries();
	timeCorrelator0Error
	  = timeCorrelator[0].getStandardDeviation();
	
	// Time average of blockage and standard error
	
	timeAverageNormal = timeAverage.getAverageOverSeries();
	timeAverageNormalError = timeAverage.getStandardDeviation();
	
	// Blockage average squared, with error from y = x*x -> Dy = 2x Dx
	
	squaredTimeAverageNormal = timeAverageNormal * timeAverageNormal;
	squaredTimeAverageNormalError = 2.0 * timeAverageNormal * timeAverageNormalError;
	
	// Denominator of the normalised time correlator, and error for z = x + y computed from
	// Dz <= Sqrt[ Dx^2 + Dy^2] (pythagoras on true error)
	
	double timeCorrelatorDenominator = timeCorrelator0Reading - squaredTimeAverageNormal;
	double timeCorrelatorDenominatorErrorSq = timeCorrelator0Error * timeCorrelator0Error + squaredTimeAverageNormalError * squaredTimeAverageNormalError;
	double timeCorrelatorDenominatorError = std::sqrt(timeCorrelator0Error * timeCorrelator0Error + squaredTimeAverageNormalError * squaredTimeAverageNormalError);
	
	// std::cout << timeCorrelator0Reading + " " + timeAverageNormal);
	
	isTimeCorrelatorConverged = true;
	nTimeCorrelatorConverged = 0;
	
	double lastTimeCorrelatorNumerator = 999999999.0;
	
	double integratedCorrelationTime = 0.0;
	double integratedCorrelationTimeError = 0.0;

	for (int i = 0; i < correlationTime; i++) {
	  
	  timeCorrelatorIReading
	    = timeCorrelator[i].getAverageOverSeries();
	  timeCorrelatorIError
	    = timeCorrelator[i].getStandardDeviation();
	  
	  
	  double timeCorrelatorNumerator = timeCorrelatorIReading - squaredTimeAverageNormal;
	  double timeCorrelatorNumeratorError = std::sqrt(timeCorrelatorIError * timeCorrelatorIError 
							  + squaredTimeAverageNormalError * squaredTimeAverageNormalError);
	  
	  // Normalised time correlator, and the error for z = x / y computed from 
	  // pythagoras on fractional error [Derived from pythag on true error]
	  
	  double timeCorrelatorValue = timeCorrelatorNumerator / timeCorrelatorDenominator;
	  
	  integratedCorrelationTime+=timeCorrelatorValue;

	  // Error estimation modified by significant correlation between variables
	  
	  double covarienceUv = timeCorrelatorXY[i].getStandardDeviationCovarientSq();
	  double dGbyDu = 1/timeCorrelatorDenominator;
	  double dGbyDv = -((timeCorrelatorIReading * (1 - 2.0 * timeCorrelator0Reading) + timeCorrelator0Reading * timeCorrelator0Reading)
			    / timeCorrelatorDenominator / timeCorrelatorDenominator);
	  double uError = timeCorrelatorIError;
	  double vError = timeCorrelator0Error;
	  
	  double timeCorrelatorError = std::pow(uError * dGbyDu , 2.0);
	  timeCorrelatorError += std::pow(vError * dGbyDv , 2.0);
	  timeCorrelatorError += 2.0 * covarienceUv * dGbyDu * dGbyDv;
	  
	  integratedCorrelationTimeError += timeCorrelatorError;

	  // std::cout << covarienceUv + " " + uError + " " + vError);
	  // std::cout << std::pow(uError * dGbyDu , 2.0) + " " + std::pow(vError * dGbyDv , 2.0) + " " + 2.0 * covarienceUv * dGbyDu * dGbyDv);
	  
	  // timeCorrelatorError = std::sqrt(timeCorrelatorError);
	  
	  out << i << " " << timeCorrelatorValue << " " << (timeCorrelatorError > 0.0 ? std::sqrt(timeCorrelatorError) : 0) << " ";
	  out << timeCorrelatorNumerator << " " << timeCorrelatorNumeratorError << " " << integratedCorrelationTime << " " << sqrt(integratedCorrelationTimeError) << std::endl;
	  
	  // The t0 time correlator is always 1. For plotting purposes, don't need overall
	  // accuracy > 1% of that value, so don't divide by timeCorrelatorValue
	  
	  // if ((std::sqrt(std::abs(timeCorrelatorError)) > prm::tolerance)
	  //     || (timeCorrelatorDenominator == 0.0)) 
	  //   isTimeCorrelatorConverged = false;
	  // else nTimeCorrelatorConverged++;
	  
	  if ((std::sqrt(std::abs(timeCorrelatorNumeratorError/timeCorrelatorNumerator)) > prm::tolerance)
	      || (timeCorrelatorDenominator == 0.0)) 
	    isTimeCorrelatorConverged = false;
	  else nTimeCorrelatorConverged++;
	  
	  lastTimeCorrelatorNumerator = timeCorrelatorNumerator;
	  
	}
	
	// std::cout << "Time correlator " << nTimeCorrelatorConverged << " of " << correlationTime << " converged" << std::endl;
	
	out.close();
	
	
	
	//****************************************
	// Write out the info file every few steps
	//****************************************

	// std::cout << "Writing information" << std::endl;
	
	
	std::ofstream outEquib(argv[12+1]);
	outEquib << "# Various dynamic equilibrium averages,  \n";
	
	// Normalised "Time correlator" at time 0, with the standard error
	
	outEquib << "Total time steps = " << time_i << "\n";
	
	outEquib << "Mean time node remains blocked = ";
	outEquib << blockageTime.getAverageOverSeries() << " +/- " << blockageTime.getStandardDeviation() << "\n\n";
	outEquib << "Inter-layer blockage correlation = ";
	outEquib << beta.getAverageOverSeries() << " +/- " << beta.getStandardDeviation() << "\n\n";
	outEquib << "Percentage blocked on average = ";
	outEquib << (100.0 * blockagePercentage.getAverageOverSeries()) << " +/- " << (100.0 * blockagePercentage.getStandardDeviation()) << "\n";
	for (int level_j = 0; level_j < eprm::totalLevels; level_j++) {
	  outEquib << "Percentage blocked in level " << level_j << " = " ;
	  outEquib << 100.0 * blockagePercentageI[level_j].getAverageOverSeries() << " +/- ";
	  outEquib << 100.0 * blockagePercentageI[level_j].getStandardDeviation() << "\n";
	}
	
	outEquib << "Average number of emboli = " 
		 << averageEmbolusNumber.getAverageOverSeries() << " +/- " 
		 << averageEmbolusNumber.getStandardDeviation() << "\n";
	outEquib << "Average squared blockage = ";
	outEquib << squaredTimeAverageNormal << " +/- " 
		 << squaredTimeAverageNormalError << "\n";
	outEquib << "Squared average blockage t0 = ";
	outEquib << timeCorrelator0Reading << " +/- " 
		 << timeCorrelator0Error << "\n";
	outEquib << "Squared average blockage tI = ";
	outEquib << timeCorrelatorIReading << " +/- " 
		 << timeCorrelatorIError << "\n";
	    
	outEquib.close();
	
	
      }
      
      if (time_i > warmup + 5 * blockLength) {
	isConverged = embolusRemoved; // Not sure what this does
	isConverged &= blockagePercentage.isConverged();
	isConverged &= averageEmbolusNumber.isConverged();
	// isConverged &= beta.isConverged();
	if (isMeasureTimeCorrelator)
	  isConverged = isTimeCorrelatorConverged;
      } else {
	isConverged = false;
      }

      // Fixed time measurements
      
      if ((eprm::isMeasureHistory || eprm::isMeasurePerc) && time_i > totalTimeSteps) {
	isConverged = true;
      } else {
	isConverged = false;
      }
      
    } while (!isConverged); // End loop over series
    
    //**********************************************
    // Write out the ensemble averages of the 
    // time dependence of the last row blockages
    // Note, this is a proportion, not a percentage
    //**********************************************
    
    
    std::ofstream out(argv[9+1]);
    out << "# Emb rate, dissolve rate, emb. size max, emb. size min\n";
    out << "# " << eprm::embRate << ", " << eprm::embDissolveRate << ", " 
	<< eprm::embSizeMax << ", " << eprm::embSizeMin << "\n";
    for (int i=0;i<percTimeSteps;i++) {
      out << i << " " << (double)ensembleSize * (100.0 * percentageBlockedT[i])/(double)(ens_i+1) << " " << 100.0 * std::sqrt((percentageBlockedSqT[i]-percentageBlockedT[i]*percentageBlockedT[i])/(double)(ens_i+1)) << " ";
      for (int time_comp = 0; time_comp<nComp; time_comp++) {
	out << (double)ensembleSize * (100.0 * percentageCompromisedT[time_comp][i])/(double)(ens_i+1) << " " << 100.0 * std::sqrt((percentageCompromisedSqT[time_comp][i]-percentageCompromisedT[time_comp][i]*percentageCompromisedT[time_comp][i])/(double)(ens_i+1)) << " ";
      }
      out << std::endl;
    }
    out.close();
    
    
  } // End loop over ensembles (ens_i)
  
  double spaceCorrelatorError, spaceCorrelatorValue;
  
  std::ofstream out(argv[11+1]);
  out << "# node diff, correlator, average,  \n";
  
  int k = 1;
  for (int node_i=0;node_i<eprm::totalLevels;node_i++) {
    int j = k - 1;
    
    // spaceCorrelatorValue = ((double)lastRowCorrelator[j]-(double)timeAverage*(double)timeAverage/(double)totalTimeAverage)
    //  /((double)lastRowCorrelator[0]-(double)timeAverage*(double)timeAverage/(double)totalTimeAverage);
    
    // out << j + " " + spaceCorrelatorValue + " " + (double)timeAverage/(double)totalTimeAverage + " " + lastRowCorrelator[j]/totalTimeAverage + "\n");
    // j++;
    k*=2;
  }
  out.close();
  
} // End function main


//********************************************
// Routine for initializing the tree structure
//********************************************
void initializeTree(std::vector<Node> &nodeVector, 
		    double bifurcationExponentMin, double bifurcationExponentMax,
		    double lastElement, int *startLastRow, int *finishLastRow,
		    int *totalLastRow,
		    std::vector<double> &nodeSizes)
{
  nodeSizes.resize(0);

  double lastElementDouble = lastElement;

  double bifurcationExponent;

  std::cout << "\n";
  std::cout << "Tree information\n";
  std::cout << "----------------\n";
  std::cout << "Tree has " << eprm::totalLevels << " levels" << std::endl;

  for (int i=0;i<eprm::totalLevels;i++) {	
    bifurcationExponent=bifurcationExponentMin+(double)i*(bifurcationExponentMax-bifurcationExponentMin)/(double)eprm::totalLevels;
    // nodeSizes.add(new Double(lastElementDouble));

    nodeSizes.push_back(lastElementDouble);

    std::cout << "Node " << i << " size : " << lastElementDouble << " mm, bifurcation exp " << bifurcationExponent << std::endl;
    
    // What we computed in our paper
    // lastElementDouble*=std::sqrt(bifurcationExponent)/2.0;
    // Get a bifurcation exponent from the layer ratio
    
    // layerRatio = std::sqrt(bifurcationExponent) / 2.0;
    // bifurcationExponent = -std::log(2.0)/std::log(layerRatio);
    
    std::cout << "True exponent " << bifurcationExponent << std::endl;
    
    // We ought to have used this exponent as defined in Burn's paper
    lastElementDouble /= std::pow(2.0,1.0/bifurcationExponent);
  }
  
  Node theNode;
  
  int levelBase;

  int lastLevelBase, nextLevelBase;

  // Initialise the tree structure
  // The tree structure has been tested and it initialises correctly
  
  std::cout << "Initialise the tree structure" << std::endl;
  
  // double[] betaIJ = new double[eprm::totalLevels * eprm::totalLevels];
  
  for (int i=0;i<eprm::totalLevels;i++) {
    levelBase=(int)std::pow(2.0,i)-1;
    lastLevelBase=(int)std::pow(2.0,i-1)-1;
    nextLevelBase=(int)std::pow(2.0,i+1)-1;
    *startLastRow=levelBase;
    *finishLastRow=nextLevelBase;
    for (int j=0;j<(int)std::pow(2.0,i);j++) {
      theNode.size=nodeSizes[i];
      theNode.daughterA=nextLevelBase+2*j;
      theNode.daughterB=nextLevelBase+2*j+1;
      theNode.parent=lastLevelBase+j/2;
      theNode.location=levelBase+j;
      theNode.isBlocked=false;
      theNode.treeLevel=i;
      theNode.resistance=std::pow(2.0,i);
      if (i==eprm::totalLevels-1) theNode.isLastNode=true;
      if (i==0) theNode.parent=0;
      // if (eprm::isWatchEmbolus) {
      //   std::cout << theNode.parent);
      //   std::cout << theNode.daughterA);
      //	  std::cout << theNode.daughterB);
      //  std::cout << theNode.isLastNode);
      //}
      nodeVector.push_back(theNode);
    }
  }

  // 1 mm Hg = 0.133e3 kPa

  nodeVector[0].pressure = eprm::pressureMmHg * 0.13 * 1e3;
  
  *totalLastRow = *finishLastRow - *startLastRow;
  
  std::cout << "... tree structure initialised with total elements: " << nodeVector.size() << std::endl;
  std::cout << "Capiliary nodes starting at " << *startLastRow << " and ending at " << *finishLastRow << std::endl;

  std::cout << "Computing initial flows and resistances" << std::endl;

  computeFlowsAndResistances(nodeVector);

  std::cout << "... flows and resistances initialised." << std::endl;
}

void attemptEmbolusInsertion(std::vector<Embolus> &embolusVector, int time_index) {
  
  Embolus theEmbolus;

  if (!eprm::isExternalFile) {

    // If there's no external file, emboli can be chosen at random to
    // appear at a characteristic rate, and with a characteristic size
    // distribution

    if (time_index>=eprm::embolisationStartTime && time_index<eprm::embolisationStopTime) {
      if (randomNumber.nextDouble() < eprm::embRate) {
	
	// note - it's very important that the embolus type is set
	// before the initial radius is set as setInitialRadius determines the
	// constants associated with the gas bubble
	
	if (eprm::isEmbolusDeformable) {
	  theEmbolus.setDeformable(true);
	} else {
	  theEmbolus.setDeformable(false);
	}

	if (eprm::isGasBubble) {
	  theEmbolus.setLinearArea();
	  if (eprm::isGasBubbleEckmann) {
	    theEmbolus.setEckmannParameterisation();
	  }
	} else {
	  theEmbolus.setLinearRadius();
	  theEmbolus.setDeformable(false);
	}
	
	theEmbolus.setInitialRadius( ( eprm::embSizeMax - eprm::embSizeMin )
				     * randomNumber.nextDouble() 
				     + eprm::embSizeMin );
	theEmbolus.location=0;

	theEmbolus.setDissolveRate(eprm::embDissolveRate);
	embolusVector.push_back(theEmbolus);
	
	// std::cout << "Embolus created");
      }
    }
  
  } else {

    // There is an external file, emboli appear at specific times
    // with a specified size and an error in size (related to the
    // EBR in decibels). Linke's system claims accuracy of +/- 7dB
    // and a few ms in the time (which will be negligible).

    // Using the error in the EBR, a spread of sizes should be computed
    // which follow a normal distribution (or a uniform one to avoid negative
    // numbers).

    // embNumber is the index of the embolus

    while (time_index > eprm::embTime[eprm::embNumber] && eprm::embNumber < eprm::embTime.size()) {

      // note - it's very important tha the embolus type is set
      // before the initial radius is set as setInitialRadius determines the
      // constants associated with the gas bubble

      if (eprm::isEmbolusDeformable) {
	theEmbolus.setDeformable(true);
      } else {
	theEmbolus.setDeformable(false);
      }
      
      if (eprm::isGasBubble) {
	theEmbolus.setLinearArea();
	if (eprm::isGasBubbleEckmann) {
	  theEmbolus.setEckmannParameterisation();
	}
      } else {
	// Solid embolus

	theEmbolus.setLinearRadius();
	theEmbolus.setDeformable(false);
      }

      double initialRadius = eprm::embSize[eprm::embNumber]
	+ 2.0 * (randomNumber.nextDouble() - 0.5) 
	* eprm::embSizeDelta[eprm::embNumber];

      if (initialRadius > 0) {

	theEmbolus.setInitialRadius(initialRadius);
	theEmbolus.location=0;

	// Dissolve rate is now fixed in case of external file
	// - set to 0.1mm/hr for solid emboli, and not relevant
	// for parameterised gaseous emboli.
	
	// theEmbolus.setDissolveRate(eprm::embDissolveRate);
 
	embolusVector.push_back(theEmbolus);

	// std::cout << "NEW EMBOLUS: " << eprm::embNumber << ", size " << theEmbolus.getRadius() << ", loc " << theEmbolus.location << std::endl;

      }

      eprm::embNumber++;

    }

    // Otherwise do nothing!

  }

}

double nonlinear(double ratio) {
  double a=5.0, b=0, w=5.89, x=ratio;
  
  //**************************************************************************
  // LINEAR. This is the equivalent of assuming that emboli 'go with the flow'
  //**************************************************************************
  
  if (modelType == LinearModel) {
	return ratio;
  }
  
  //*****************************
  // THE ORIGINAL PARAMETRISATION
  //*****************************
  
  // double embolusTrajectory = 0.5 * (Math.tanh((a*Math.tan(pi*(x-0.5))-b)/w)+1.0);
  // double embolusTrajectory = x; 			//linear flow function
  
  //************************
  // THE NEW PARAMETRISATION
  //************************
  
  // double y=2.0*x-1;
  // double atanh = Math.log((1.0+y)/(1.0-y))/2;
  // double embolusTrajectory = 0.5*(Math.tanh(a*atanh-b)+1.0);
  
  //*************************************************
  // AN EXTREME PARAMETRISATION
  // (EMBOLUS ALWAYS TRAVELS AWAY FROM MOST BLOCKAGE)
  // this is the most rapidly changing sigmoid
  //*************************************************
  
  if (modelType == ExtremeModel) {
  
    double embolusTrajectory = 1.0;
    if (ratio<0.5) embolusTrajectory = 0.0;
    if (ratio==0.5) embolusTrajectory = 0.5; // Important so there is no bias
    
    return embolusTrajectory;
    
  }
  
  std::cout << "Should not be here in nonlinear" << std::endl;
  exit(1);
  return 0.0;
}  

//****************************************************************************
// This is the most simplistic way of establishing flow in daughter
// vessels. It works by assuming that flow is proportional to the number
// of free nodes. This is equivalent to assuming that all pressure is dropped
// over end nodes
//****************************************************************************
double flowDirectionAB(std::vector<Embolus> &embolusVector,
		       std::vector<Node> &nodeVector,
		       std::vector<double> &lastRowBlockage,
		       int emb_i, int startLastRow, bool directionA)
{

  double proportion;
  
  int daughterlocation;
  
  int emboluslocation = embolusVector[emb_i].location;
  
  if (directionA) {
    daughterlocation = nodeVector[emboluslocation].daughterA;
  } else {
    daughterlocation = nodeVector[emboluslocation].daughterB;
  }

  if (gprm::isFlowInformation) {
    
    // In the presence of flow information, the relative flows
    // can be found easily

    proportion = nodeVector[daughterlocation].flow;

  } else {
    
    // Follow tree to left from daughter A
    
    int branchlevel = nodeVector[daughterlocation].treeLevel;
    
    for (int i=branchlevel;i<eprm::totalLevels-1;i++) {
      daughterlocation = nodeVector[daughterlocation].daughterA;
    }
    
    int startDaughter=daughterlocation;
    
    // Follow tree to right from daughter A
    
    if (directionA) {
      daughterlocation = nodeVector[emboluslocation].daughterA;
    } else {
      daughterlocation = nodeVector[emboluslocation].daughterB;
    }
    
    for (int i=branchlevel;i<eprm::totalLevels-1;i++) {
      daughterlocation = nodeVector[daughterlocation].daughterB;
    }
    
    int finishDaughter=daughterlocation;
    
    double percentage = 0.0;
    
    // Start with the last node line
    
    for (int node_i=startDaughter;node_i<=finishDaughter;node_i++) {
      if (lastRowBlockage[node_i-startLastRow]==1) 
	percentage+=1.0/(finishDaughter-startDaughter+1);
    }
    
    proportion = 1.0-percentage;

  }
    
  return proportion;
 
}

void loadFile(std::string fileName,double cutOffSize)
{
  int iOneEmbTime;
  double oneEmbTime;
  double oneEmbSize, oneEmbSizeDelta;
  double oneEmbMEBR;

  std::ifstream theFile(fileName.c_str());
  
  // Order as in Caroline's / Rizwan's file. Time is in seconds. Size is in mm

  std::cout << "Reading file" << std::endl;
  std::cout << "------------" << std::endl;

  while (theFile >> oneEmbTime >> oneEmbSize >> oneEmbSizeDelta >> oneEmbMEBR) {   
    std::cout << "t = " << oneEmbTime << ", size = " << oneEmbSize << ", d_size " << oneEmbSizeDelta;

    iOneEmbTime = (int)oneEmbTime;


    Embolus testEmbolus;
    
    if (eprm::isEmbolusDeformable) {
      testEmbolus.setDeformable(true);
    } else {
      testEmbolus.setDeformable(false);
    }
    
    if (eprm::isGasBubble) {
      testEmbolus.setLinearArea();
      if (eprm::isGasBubbleEckmann) {
	testEmbolus.setEckmannParameterisation();
      }
    } else {
      testEmbolus.setLinearRadius();
      testEmbolus.setDeformable(false);
    }
    
    
    testEmbolus.setInitialRadius(oneEmbSize);

    if (oneEmbSize>cutOffSize) {
      std::cout << " :yes: ";
    } else {
      std::cout << " :no : ";
    }
    
    std::cout << ", volume = " << testEmbolus.readVolume();
    std::cout << ", lifetime = " << testEmbolus.readEmbolusLifetime() << " min" << std::endl;
  
    if (oneEmbSize>cutOffSize) {
      eprm::embSize.push_back(oneEmbSize);
      eprm::embTime.push_back(iOneEmbTime);
      eprm::embSizeDelta.push_back(oneEmbSizeDelta);
    }
  }
  
  std::cout << "------------" << std::endl;

  theFile.close();
}

void outputEmbolusInfo(int emb_i,std::vector<Embolus> &embolusVector,
		       std::vector<Node> &nodeVector) {
  std::cout << "-------------------------------" << std::endl;
  std::cout << "Embolus location for embolus " << (emb_i+1) 
	    << " : " << embolusVector[emb_i].location << std::endl;
  std::cout << "Embolus size for embolus " << (emb_i+1) 
	    << " : " << embolusVector[emb_i].getRadius() << std::endl;
  std::cout << "Daughter A " 
	    << nodeVector[embolusVector[emb_i].location].daughterA 
	    << std::endl;
  std::cout << "Daughter B " 
	    << nodeVector[embolusVector[emb_i].location].daughterB 
	    << std::endl;
  std::cout << "Node location " 
	    << nodeVector[embolusVector[emb_i].location].location << std::endl;
  std::cout << "Node size " 
	    << nodeVector[embolusVector[emb_i].location].size << std::endl;
  std::cout << "Node blocked " 
	    << nodeVector[embolusVector[emb_i].location].isBlocked 
	    << std::endl;
}


//******************************************************
// Compute flows on the tree using a recursive algorithm
// N.B. This only works for Pouseille flow!
//******************************************************
void computeFlowsAndResistances(std::vector<Node> &gNodeVector)
{
  // Computes all resistances below the root node
  // using a recursive algorithm

  // std::cout << "Computing resistances" << std::endl;

  computeResistance(0,gNodeVector);

  // Each node has an element resistanceBelow containing
  // the resistance below the node. Another element 
  // resistance containing the resistance of the node
  // As with the resistance, a recursive algoritm is
  // appropriate.
  
  // With knowledge of the total input flow, the
  // relative flows in the two daughter branches
  // can be determined as Q_A / Q_B = R_B / R_A.
  // Also, the flow through the pipe above the node
  // can be determined (by flow conservation). It is
  // Q_0 = Q_A (1 + R_A / R_B) or 
  // Q_0 = Q_B (1 + R_B / R_A)
  // Although this only gives a ratio - the initial
  // flow must be known? Not is the pressure is known
  // it can be computed!
  // 
  // V = R_0 Q_A (1 + R_A / R_B) + Q_A R_A
  // 
  // or 
  //
  // V = Q_0 R_0 + Q_0 [1+R_A/R_B]^{-1}
  // 
  // leading to Q_0

  // Presumably it just requires the total resistance
  // of the tree below and including the top node!

  // std::cout << "Computing flows" << std::endl;

  computeFlow(0, gNodeVector);

  // std::cout << "write flows resistances" << std::endl;

  // writeTreeFlowsResistances(gNodeVector);

  // std::cout << "... done" << std::endl;
}

// Routine to write values of flow and resistance etc from
// the tree to file.
void writeTreeFlowsResistances(std::vector<Node> &nodeVector)
{
  std::ofstream outputFile("flowrestest.d");

  for (int i=0; i<nodeVector.size(); i++) {
    outputFile << i << " " 
	       << nodeVector[i].size << " "
	       << nodeVector[i].length << " "
	       << nodeVector[i].flow << " "
	       << nodeVector[i].pressure << " "
	       << nodeVector[i].resistance << " "
	       << nodeVector[i].resistanceBelow << " "
	       << nodeVector[i].xLoc << " " 
	       << nodeVector[i].yLoc << " "
	       << nodeVector[i].zLoc << " ";
    outputFile << nodeVector[nodeVector[i].parent].xLoc << " " 
	       << nodeVector[nodeVector[i].parent].yLoc << " "
	       << nodeVector[nodeVector[i].parent].zLoc << std::endl;

    outputFile << std::endl;
  }
  
  outputFile.close();
}


void computeFlow(int index, std::vector<Node> &gNodeVector)
{
  double flow;
  double top_pressure = gNodeVector[index].pressure; // This needs to be the pressure
  // at the top of the inlet pipe

  double node_pressure;

  flow = top_pressure / gNodeVector[index].resistanceBelow;

  // Pressure at the bifurcation

  double top_pressure_node = top_pressure - flow * gNodeVector[index].resistance;

  gNodeVector[index].flow = flow;

  if (!gNodeVector[index].isLastNode) {
    int daughterA = gNodeVector[index].daughterA;
    int daughterB = gNodeVector[index].daughterB;

    gNodeVector[daughterA].pressure = top_pressure_node;
    gNodeVector[daughterB].pressure = top_pressure_node;

    computeFlow(daughterA, gNodeVector);
    computeFlow(daughterB, gNodeVector);
  }
  
}

void computeLength(int index, std::vector<Node> &gNodeVector)
{
  double length = 0.0;
  length += std::pow((gNodeVector[index].xLoc -
		      gNodeVector[gNodeVector[index].parent].xLoc),2);
  length += std::pow((gNodeVector[index].yLoc -
		      gNodeVector[gNodeVector[index].parent].yLoc),2);
  length += std::pow((gNodeVector[index].zLoc -
		      gNodeVector[gNodeVector[index].parent].zLoc),2);
  gNodeVector[index].length = std::sqrt(length);
  if (!gNodeVector[index].isLastNode) {
    computeLength(gNodeVector[index].daughterA,gNodeVector);
    computeLength(gNodeVector[index].daughterB,gNodeVector);
  }
}

//******************************************************
// Compute resistances on the tree using a 
// recursive algorithm
//-------------------------
// N.B. The following could be improved by setting the
// resistance at a node to an infinite value. This
// could be achieved by having an 'isInfiniteResistance'
// flag on the nodes.
//
// As the code currently works, emboli located below
// a blockage can still move into their blocked position
// even though there is essentially no flow. This doesn't
// matter, because in any case, the downstream emboli 
// would rearrange themselves into new positions dictated
// by full flow in that part of the tree as soon as flow 
// restarted. The modification above might be nice
// for completion. 
//******************************************************
double computeResistance(int index, std::vector<Node> &gNodeVector)
{
  double resistance;
  
  if (gNodeVector[index].isLastNode) {

    // resistance = 8.0 * gprm::viscosity * gNodeVector[index].length / gprm::pi / std::pow(gNodeVector[index].size/2.0,4.0);  // Compute the resistance using the Pouiselle expression
    
    if (gNodeVector[index].isBlocked) {
      resistance = gNodeVector[index].resistance * 100000.0;
    } else {
      resistance = gNodeVector[index].resistance;
    }

  } else {

    // Not needed if resistances set at beginning and no change
    // to tree. N.B. Will need resistance changes when blockage occurs.

    // gNodeVector[index].resistance = 8.0 * gprm::viscosity * gNodeVector[index].length / gprm::pi / std::pow(gNodeVector[index].size/2.0,4.0);  // Compute the resistance using the Pouiselle expression
    
    resistance = 1.0/computeResistance(gNodeVector[index].daughterA,gNodeVector)
      + 1.0/computeResistance(gNodeVector[index].daughterB,gNodeVector);
    if (gNodeVector[index].isBlocked) {
      resistance = 1.0/resistance + 100000.0 * gNodeVector[index].resistance;
    } else {
      resistance = 1.0/resistance + gNodeVector[index].resistance;
    }
  }

  gNodeVector[index].resistanceBelow = resistance;

  return resistance;
}
