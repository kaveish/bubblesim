#include <cmath>
#include <iostream>
#include <fstream>
#include <ctime>

#include "embolus.h"
#include "node.h"
#include "Accumulator.h"
#include "RandomGenerator.h"
#include "ConvergenceType.h"

namespace prm {
  double tolerance;
  double beta;
  int wallTimeMinutes;
  int nmeas;
  int nseries;
  long*    	  idum;
  clock_t                   parameterSetStartTime;
  clock_t                   currentTime;
  ConvergenceType           convergenceType = ConvergenceError;
}

enum ModelType {
   LinearModel,
   NonlinearModel,
   ExtremeModel
   };
   
ModelType modelType = LinearModel;

static double pi = 3.14159265359;
			 		    
double nonlinear(double ratio) {
  double a=5.0, b=0, w=5.89, x=ratio;
  
  //*************
  // LINEAR
  //*************
  
  if (modelType == LinearModel) {
	return ratio;
  }
  
  //*****************************
  // THE ORIGINAL PARAMETRISATION
  //*****************************
  
  // double embolusTrajectory = 0.5 * (Math.tanh((a*Math.tan(pi*(x-0.5))-b)/w)+1.0);
  // double embolusTrajectory = x; 			//linear flow function
  
  //************************
  // THE NEW PARAMETRISATION
  //************************
  
  // double y=2.0*x-1;
  // double atanh = Math.log((1.0+y)/(1.0-y))/2;
  // double embolusTrajectory = 0.5*(Math.tanh(a*atanh-b)+1.0);
  
  //***************************
  // AN EXTREME PARAMETRISATION
  // (EMBOLUS ALWAYS TRAVELS AWAY FROM MOST BLOCKAGE)
  //***************************
  
  if (modelType == ExtremeModel) {
  
  double embolusTrajectory = 1.0;
  if (ratio<0.5) embolusTrajectory = 0.0;
  if (ratio==0.5) embolusTrajectory = 0.5; // Important so there is no bias
  
  return embolusTrajectory;
  
  }
  
  std::cout << "Should not be here in nonlinear" << std::endl;
  exit(1);
  return 0.0;
}  

int main (int argc,char* argv[]) {

  double result;
  
  int blockLength = 1000;

  prm::nmeas = blockLength;

  int nTimeCorrelatorConverged;
  
  int warmup = 5 * blockLength;
  
  Accumulator blockageTime;
  
  Accumulator blockagePercentage;
  
  Accumulator averageEmbolusNumber;
  
  Accumulator beta;

  Accumulator timeAverage;
  
  bool isMeasureTimeCorrelator = false;
  bool isConverged;
  bool isTimeCorrelatorConverged = true;
  
  bool isWatchEmbolus = false; // Set to true to watch the embolus
  
  // Set to true to empty all blockages before new blockage measurement
  
  bool isExtraSafe = false;    
  
  // Set to true to write an output file with the history of blockages
  
  bool isMeasureHistory = false;
  
  RandomGenerator randomNumber(-1247);
  
  // int j;
  
  // Number of coarse grained points in history file (must be factor of 2)
  
  int nmeasblockage = 0;
  
  int coarseGrain = 128; 
  
  int daughterlocation;
  int emboluslocation;
  
  bool embolusRemoved = false;
  
  int intDummy;
  
  int branchlevel;
  
  int startLastRow = 0;
  int finishLastRow = 0;
  int totalLastRow = 0;
  
  int location;
  
  int nextLevelBase;
  int lastLevelBase;
  int levelBase;
  
  double betaMeasure;
  
  double percentage = 0.0;
  
  double proportionA,proportionB;
  
  int startDaughter, finishDaughter;
  
  double dummy, dummy2;
  
  // (1) Add measurement for mutually exclusive probability average
  // (2) Add measurement for independent probability average
  // (3) Add measurement for inter-layer correlator
  // (N.B. 3: all inter-layer correlators need measureing to be certain that
  // they are all identical)
  
  std::cout << "Check embolus weighting" << std::endl;
  std::cout << "0.05 " << nonlinear(0.05) << std::endl;
  std::cout << "0.1 " << nonlinear(0.1) << std::endl;
  std::cout << "0.2 " << nonlinear(0.2) << std::endl;
  std::cout << "0.3 " << nonlinear(0.3) << std::endl;
  std::cout << "0.4 " << nonlinear(0.4) << std::endl;
  std::cout << "0.5 " << nonlinear(0.5) << std::endl;
  std::cout << "0.6 " << nonlinear(0.6) << std::endl;
  std::cout << "0.7 " << nonlinear(0.7) << std::endl;
  std::cout << "0.8 " << nonlinear(0.8) << std::endl;
  std::cout << "0.9 " << nonlinear(0.9) << std::endl;
  
  std::cout << "(1) Embolisation rate (1/sec)" << std::endl;
  std::cout << "(2) Dissolve rate (mm/sec) " << std::endl;
  std::cout << "(3) Maximum embolus size (mm)" << std::endl;		//same as min
  std::cout << "(4) Minimum embolus size (mm)" << std::endl;
  std::cout << "(5) Total time steps (secs)" << std::endl;			//150
  std::cout << "(6) Ensemble size" << std::endl;				//1
  std::cout << "(7) Percentage blocked time" << std::endl;			//150
  std::cout << "(8) Correlation time (Secs)" << std::endl;			//1
  std::cout << "(9) No. nodes used in time correlator measurement" << std::endl;	//1
  std::cout << "(10) Output file: percentage blocked" << std::endl;		//perc.txt etc.
  std::cout << "(11) Output file: time correlator" << std::endl;
  std::cout << "(12) Output file: space correlator" << std::endl;
  std::cout << "(13) Output file: equilibrium properties" << std::endl;	//new name for each set of parameters
  std::cout << "(14) Output file: node blockage history" << std::endl;
  std::cout << "(15) Total tree levels" << std::endl;			//18
  std::cout << "(16) Gamma" << std::endl;					//bifurcation exponent (2)
  std::cout << "(17) Root node size" << std::endl;				//1mm
  std::cout << "(18) Required accuracy" << std::endl;			// 0.03 (percentage)
  std::cout << "(19) Input file" << std::endl;
  std::cout << "----- Your parameters -----" << std::endl;

  if (argc!=19) {
    std::cout << "Incorrect number of arguments \n";
    std::cout << argc << std::endl;
    std::exit(1);
  }

  for (int i=1; i<argc; i++)
    std::cout << "(" << i << ") " << argv[i] << std::endl;
  
  dummy = std::atof(argv[0+1]);
  double embRate= dummy;
  
  if (embRate > 1.0) {
    std::cout << "Currently maximum one embolus per time step. Please reduce embolisation rate \n";
    std::exit(1);
  }
    
  // Read the parameters from the command line
  
  double dissolveRate = std::atof(argv[1+1]);
  double embSizeMax = std::atof(argv[2+1]);
  double embSizeMin = std::atof(argv[3+1]);
  
  intDummy = (int)std::atof(argv[4+1]);
  int totalTimeSteps = intDummy;
  
  intDummy = (int)std::atof(argv[5+1]);
  int ensembleSize = intDummy;
  
  intDummy = (int)std::atof(argv[6+1]);
  int percTimeSteps = intDummy;
  
  intDummy = (int)std::atof(argv[7+1]);
  int correlationTime = intDummy;
  
  intDummy = (int)std::atof(argv[8+1]);
  int noNodes = intDummy;
  
  // This is total number of layers in tree    
  
  intDummy = (int)atof(argv[14+1]);
  int totalLevels = intDummy;
  
  double bifurcationExponentMax = std::atof(argv[15+1]); 
  double bifurcationExponentMin = std::atof(argv[15+1]);
  
  // Initialise the node sizes:
  
  double lastElementDouble = std::atof(argv[16+1]); // Top node in mm
  
  prm::tolerance = std::atof(argv[17+1]);
  
  std::cout << "Parameters ..." << std::endl;
  std::cout << "--------------" << std::endl;
  std::cout << "Minimum embolus size (mm): " << embSizeMin << std::endl;
  std::cout << "Maximum embolus size (mm): " << embSizeMax << std::endl;
  std::cout << "Embolisation rate (Emboli / sec) : " << embRate << std::endl;
  std::cout << "Dissolve rate (mm / sec) : " << dissolveRate << std::endl;
  if (totalTimeSteps > 0) {
    std::cout << "Total time steps (secs) : " << totalTimeSteps << std::endl;
  } else {
    std::cout << "Total time steps (secs) : Unlimited" << std::endl;
  }
  std::cout << "Measure percentage for " << percTimeSteps << " time steps (secs)" << std::endl;
  std::cout << "Ensemble size : " << ensembleSize << std::endl;
  std::cout << "Correlation time (secs) : " << correlationTime << std::endl;
  
  if (correlationTime > 2)
    isMeasureTimeCorrelator = true;
  
  std::cout << "Output file: percentage blocked, " << argv[9+1] << std::endl;
  std::cout << "Output file: time correlator, " << argv[10+1] << std::endl;
  std::cout << "Output file: space correlator, " << argv[11+1] << std::endl;
  std::cout << "Output file: equilibrium properties, " << argv[12+1] << std::endl;
  std::cout << "Output file: node blockage history, " << argv[13+1] << std::endl;
  
  std::cout << "---------------------\n";
  std::cout << "Derived parameters...\n";
  std::cout << "---------------------\n";
  std::cout << "Maximum embolus lifetime (sec): " << embSizeMax/dissolveRate << std::endl;
  std::cout << "Minimum embolus lifetime (sec): " << embSizeMin/dissolveRate << std::endl;
  std::cout << "Maximum embolus lifetime (hours): " << embSizeMax/dissolveRate/3600.0 << std::endl;
  std::cout << "Minimum embolus lifetime (hours): " << embSizeMin/dissolveRate/3600.0 << std::endl;
  
  blockageTime.setSeriesLength(blockLength);
  blockageTime.setDataToZero();
  
  blockagePercentage.setSeriesLength(blockLength);
  blockagePercentage.setDataToZero();
  
  averageEmbolusNumber.setSeriesLength(blockLength);
  averageEmbolusNumber.setDataToZero();
  
  beta.setSeriesLength(blockLength);
  beta.setDataToZero();
  
  timeAverage.setSeriesLength(blockLength);
  timeAverage.setDataToZero();
  
  // std::vector<double> nodeSizes(10);
  
  std::vector<double> nodeSizes(0);

  Embolus theEmbolus;
  
  // Make an array of nodes here
  
  // std::vector<Embolus> embolusVector(20);
  
  std::vector<Embolus> embolusVector(0);

  std::vector<Accumulator> timeCorrelator(correlationTime);
  std::vector<Accumulator> timeCorrelatorYY(correlationTime);
  std::vector<Accumulator> timeCorrelatorXY(correlationTime);
  
  for (int i = 0; i < correlationTime; i++) {
    // timeCorrelator.add(new Accumulator());
    // timeCorrelatorYY.add(new Accumulator());
    // timeCorrelatorXY.add(new Accumulator());
  }

  std::vector<double> percentageDead(percTimeSteps);
  std::vector<double> percentageDeadSq(percTimeSteps);
    
  double timeCorrelatorBlock;
  
  int nBlocks = 0;
  
  double timeAverageBlock = 0.0;
  
  std::vector<int> timeStepBlockage(correlationTime * noNodes);
  
  for (int i=0;i<percTimeSteps;i++) {
    percentageDead[i]=0.0;
    percentageDeadSq[i]=0.0;
  }
  
  // Reset the time correlator
  for (int i = 0; i < correlationTime; i++) {
    
    timeCorrelator[i].setSeriesLength(blockLength);
    // timeCorrelator[i].setTolerance(tolerance);
    timeCorrelator[i].setDataToZero();
	
    timeCorrelatorYY[i].setSeriesLength(blockLength);
    // timeCorrelatorYY[i].setTolerance(tolerance);
    timeCorrelatorYY[i].setDataToZero();
    
    timeCorrelatorXY[i].setSeriesLength(blockLength);
    // timeCorrelatorXY[i].setTolerance(tolerance);
    timeCorrelatorXY[i].setDataToZero();
    
    for (int inode = 0; inode < noNodes; inode++)
      timeStepBlockage[inode*correlationTime + i] = 0;
  }
  
  // Main initialisation is the node tree;
  
  // Initialise arrays with size totalLevels
  
  std::vector<double> percentageI(totalLevels);
  std::vector<Accumulator> blockagePercentageI(totalLevels);
  
  for (int i = 0; i < totalLevels; i++) {
    // blockagePercentageI.add(new Accumulator());
    blockagePercentageI[i].setSeriesLength(blockLength);
    // blockagePercentageI[i].setTolerance(tolerance);
    blockagePercentageI[i].setDataToZero();
  }
  
  // Initialise the node vectors - it is important to choose the size
  // of the tree in advance - resizing is *very* time consuming!
  
  // std::vector<Node> nodeVector((int)std::pow(2.0,totalLevels+1));
  
  std::vector<Node> nodeVector(0);

  double bifurcationExponent;
  
  // double layerRatio;
  
  std::cout << "\n";
  std::cout << "Tree information\n";
  std::cout << "----------------\n";
  for (int i=0;i<totalLevels;i++) {	
    bifurcationExponent=bifurcationExponentMin+(double)i*(bifurcationExponentMax-bifurcationExponentMin)/(double)totalLevels;
    // nodeSizes.add(new Double(lastElementDouble));

    nodeSizes.push_back(lastElementDouble);

    std::cout << "Node " << i << " size : " << lastElementDouble << " mm, bifurcation exp " << bifurcationExponent << std::endl;
    
    // What we computed in our paper
    // lastElementDouble*=std::sqrt(bifurcationExponent)/2.0;
    // Get a bifurcation exponent from the layer ratio
    
    // layerRatio = std::sqrt(bifurcationExponent) / 2.0;
    // bifurcationExponent = -std::log(2.0)/std::log(layerRatio);
    
    std::cout << "True exponent " << bifurcationExponent << std::endl;
    
    // We ought to have used this exponent as defined in Burn's paper
    lastElementDouble /= std::pow(2.0,1.0/bifurcationExponent);
  }
  
  Node theNode;
  
  // Initialise the tree structure
  // The tree structure has been tested and it initialises correctly
  
  std::cout << "Initialise the tree structure" << std::endl;
  
  // double[] betaIJ = new double[totalLevels * totalLevels];
  
  for (int i=0;i<totalLevels;i++) {
    levelBase=(int)std::pow(2.0,i)-1;
    lastLevelBase=(int)std::pow(2.0,i-1)-1;
    nextLevelBase=(int)std::pow(2.0,i+1)-1;
    startLastRow=levelBase;
    finishLastRow=nextLevelBase;
    for (int j=0;j<(int)std::pow(2.0,i);j++) {
      theNode.size=nodeSizes[i];
      theNode.daughterA=nextLevelBase+2*j;
      theNode.daughterB=nextLevelBase+2*j+1;
      theNode.parent=lastLevelBase+j/2;
      theNode.location=levelBase+j;
      theNode.isBlocked=false;
      theNode.treeLevel=i;
      if (i==totalLevels-1) theNode.isLastNode=true;
      if (i==0) theNode.parent=0;
      // if (isWatchEmbolus) {
      //   std::cout << theNode.parent);
      //   std::cout << theNode.daughterA);
      //	  std::cout << theNode.daughterB);
      //  std::cout << theNode.isLastNode);
      //}
      nodeVector.push_back(theNode);
    }
  }
  
  totalLastRow = finishLastRow - startLastRow;
  
  std::cout << "... tree structure initialised with total elements: " << nodeVector.size() << std::endl;
  std::cout << "Capiliary nodes starting at " << startLastRow << " and ending at " << finishLastRow << std::endl;
  
  // Reset arrays involved in determining the blockage and correlation in the last row.
  
  std::vector<double> lastRowCorrelator(totalLastRow);
  std::vector<double> lastRowBlockage(totalLastRow);
  
  std::vector<int> lastRowBlockageI(totalLevels * totalLastRow);
  
  std::vector<int> lastRowBlockageTime(totalLastRow);
  
  for (int i=0; i<totalLastRow; i++) {
    lastRowCorrelator[i] = 0.0;
    lastRowBlockage[i] = 0;
    lastRowBlockageTime[i] = 0;
    
    for (int k=0; k<totalLevels; k++) {
      lastRowBlockageI[k*totalLastRow + i] = 0;
    }
  }
  
  // At this stage, the embolus vector is empty
  
  //*************************************************
  // Loop over the ensemble - total number of samples 
  // N.B. This should be something like 1000 if interested in the time evolution.
  // N.N.B. This should be 1 if only interested in the
  // equilibrium blockage, although need to make a modification to make the averages
  // May need an accumulator class
  //*************************************************
  
  percentage = 0.0;
  
  for (int ens_i=0;ens_i<ensembleSize;ens_i++) {
    
    //***************************************************
    // Clear out the blockages in the tree by brute force
    // and empty the embolus vector.
    //***************************************************
    
    for (int node_i=0; node_i<nodeVector.size(); node_i++)
      nodeVector[node_i].isBlocked=false;
    embolusVector.clear();
    
    std::cout << "Beginning ensemble run " << ens_i << std::endl;
    
    // Loop over the total number of time steps in the simulation
    
    int time_i = 0;
    
    if (isMeasureHistory) {
      
      std::cout << "opening history file" << std::endl;
      
      std::ofstream outHist(argv[13+1]);
      outHist << "# Course grained history of blockage vs timestep,  \n";
      
      outHist.close();
		
      std::cout << "opened history file" << std::endl;
      
    }
    
    
    do {
      
      for (int time_j = 0; time_j < blockLength; time_j++) {
	
	// Reset blockages on the node vector
	
	for (int i=0;i<nodeVector.size();i++) {
	  nodeVector[i].isBlocked=false;
	}
	
	// Insert an embolus into the system with a certain 
	// probability per time step
	
	if (randomNumber.nextDouble()<embRate) {
	  
	  // theEmbolus = new Embolus();
	  
	  theEmbolus.size= ( embSizeMax - embSizeMin )
	    * randomNumber.nextDouble() + embSizeMin;
	  theEmbolus.location=0;
	  theEmbolus.dissolveRate=dissolveRate;
	  embolusVector.push_back(theEmbolus);
	  // std::cout << "Embolus created");
	}
	
	// Check that there are currently emboli to operate on
	
	if (!embolusVector.empty()) {
	  
	  //******************************
	  // Destroy / remove spent emboli
	  //******************************
	  
	  // Loop over the possible emboli from top to bottom (so that elements can be removed without problems)
	  
	  for (int emb_i=embolusVector.size()-1;emb_i>=0;emb_i--) {
	    
	    //********************************************************
	    // We may be watching the emboli as they traverse the tree
	    // To double check that the code is working
	    //********************************************************
	    
	    if (isWatchEmbolus) {
	      std::cout << "-------------------------------" << std::endl;
	      std::cout << "Embolus location for embolus " << (emb_i+1) << " : " << embolusVector[emb_i].location << std::endl;
	      std::cout << "Embolus size for embolus " << (emb_i+1) << " : " << embolusVector[emb_i].size << std::endl;
	      std::cout << "Daughter A " << nodeVector[embolusVector[emb_i].location].daughterA << std::endl;
	      std::cout << "Daughter B " << nodeVector[embolusVector[emb_i].location].daughterB << std::endl;
	      std::cout << "Node location " << nodeVector[embolusVector[emb_i].location].location << std::endl;
	      std::cout << "Node size " << nodeVector[embolusVector[emb_i].location].size << std::endl;
	      std::cout << "Node blocked " << nodeVector[embolusVector[emb_i].location].isBlocked << std::endl;
	      std::cout << "Time step " << time_i << std::endl;
	    }
	      
	    //*********************************
	    // Decrease the size of the embolus
	    //********************************* 
	    
	    embolusVector[emb_i].decrement();
	    
	    //***********************************
	    // Two possibilities for destruction:
	    //***********************************
	    
	    if ((embolusVector[emb_i].size<nodeVector[embolusVector[emb_i].location].size)&&
		(nodeVector[embolusVector[emb_i].location].isLastNode)) {

	      // (I) The embolus is on the final layer of the tree, and is small enough to move
	      
	      embolusRemoved = true;
	      
	      if (isWatchEmbolus) {
		std::cout << "Embolus reached end of tree: removed" << std::endl;
	      }

	      nodeVector[embolusVector[emb_i].location].isBlocked=false;
	      // embolusVector.remove(emb_i);
	      
	      embolusVector.erase(embolusVector.begin()+emb_i);

	    } else if (embolusVector[emb_i].size<0.0) {

	      // (II) The embolus disolved before reaching the end of the tree
	      
	      embolusRemoved = true;
	      
	      if (isWatchEmbolus) {
		std::cout << "Embolus disolved: removed" << std::endl;
	      }

	      nodeVector[embolusVector[emb_i].location].isBlocked=false;

	      // embolusVector.remove(emb_i);

	      embolusVector.erase(embolusVector.begin()+emb_i);
	    }
	  }
	}
	
	//************************************************************
	// Some emboli have now been destroyed, so
	// check again that there is actually an embolus to operate on
	//************************************************************
	
	if (!embolusVector.empty()) {
	  
	  //***************************************************
	  // Clear out the blockages in the tree by brute force
	  // Thought it might be slow, but turns out to 
	  // be sufficiently fast, and much safer!
	  //***************************************************
	  
	  if (isExtraSafe)
	    for (int node_i=0; node_i<nodeVector.size(); node_i++)
	      nodeVector[node_i].isBlocked=false;
	  
	  //**********************************************
	  // Determine which nodes are blocked before move
	  //**********************************************
	  
	  for (int emb_i=0;emb_i<embolusVector.size();emb_i++) {
	    if (embolusVector[emb_i].size > nodeVector[embolusVector[emb_i].location].size) {
	      nodeVector[embolusVector[emb_i].location].isBlocked=true;
	      if (isWatchEmbolus) {
		std::cout << "Actual blockage in node " + embolusVector[emb_i].location << std::endl;
	      }
	    } else {
	      nodeVector[embolusVector[emb_i].location].isBlocked=false;
	    }
	  }
	  
	  //*************************************
	  // Loop over all the emboli
	  //*************************************
	  for (int emb_i=0;emb_i<embolusVector.size();emb_i++) {
	    
	    //***********************************************************************
	    // If the embolus is less than the size of the node, the embolus can move
	    //***********************************************************************
	    if (embolusVector[emb_i].size<nodeVector[embolusVector[emb_i].location].size) {
		
	      //*********************************************
	      // Determine the extent of nodes
	      // in the direction of daughter A
	      //*********************************************
	      
	      // Follow tree to left from daughter A
	      
	      emboluslocation = embolusVector[emb_i].location;
	      daughterlocation = nodeVector[emboluslocation].daughterA;
	      branchlevel = nodeVector[daughterlocation].treeLevel;
	      
	      for (int i=branchlevel;i<totalLevels-1;i++) {
		daughterlocation = nodeVector[daughterlocation].daughterA;
	      }
	      startDaughter=daughterlocation;
	      
	      // Follow tree to right from daughter A
	      
	      emboluslocation = embolusVector[emb_i].location;
	      daughterlocation = nodeVector[emboluslocation].daughterA;
	      
	      for (int i=branchlevel;i<totalLevels-1;i++) {
		daughterlocation = nodeVector[daughterlocation].daughterB;
	      }
	      finishDaughter=daughterlocation;
	      
	      percentage = 0.0;
	      
	      // Start with the last node line
	      
	      for (int node_i=startDaughter;node_i<=finishDaughter;node_i++) {
		if (lastRowBlockage[node_i-startLastRow]==1) percentage+=1.0/(finishDaughter-startDaughter+1);
	      }
	      
	      proportionA=1.0-percentage;
	      
	      //****************************************************************
	      // Determing the extent of nodes in the direction of daughter B
	      //****************************************************************
	      
	      // Follow tree to left from daughter B
	      
	      daughterlocation=nodeVector[embolusVector[emb_i].location].daughterB;	      
	      for (int i=nodeVector[daughterlocation].treeLevel;i<totalLevels-1;i++) {
		daughterlocation=nodeVector[daughterlocation].daughterA;
	      }
	      startDaughter=daughterlocation;
	      
	      // Follow tree to right from daughter B
	      
	      daughterlocation=nodeVector[embolusVector[emb_i].location].daughterB;
	      for (int i=nodeVector[daughterlocation].treeLevel;i<totalLevels-1;i++) {
		daughterlocation=nodeVector[daughterlocation].daughterB;
	      }
	      finishDaughter=daughterlocation;
	      
	      // Determine the proportion blocked
	      
	      percentage=0; 
	      for (int node_i=startDaughter;node_i<=finishDaughter;node_i++) {
		if (lastRowBlockage[node_i-startLastRow]==1) {
		  percentage+=1.0/(finishDaughter-startDaughter+1);
		}
	      }
	      proportionB=1.0-percentage; // This is the unblocked proportion
	      
	      //***************************************
	      // Now determine the motion of the emboli
	      //***************************************
	      
	      // std::cout << proportionA + " " + proportionB);
	      
	      if ((proportionB>0.0)||(proportionA>0.0)) {
		
		// Embolus moves if don't have all arteries blocked
		// Normally some arteries will be blocked
		
		double ratio = (proportionA /(proportionA + proportionB));
		
		result=nonlinear(ratio);
		
		if (randomNumber.nextDouble() < result) {
		  nodeVector[embolusVector[emb_i].location].isBlocked=false;
		  int daughter=nodeVector[embolusVector[emb_i].location].daughterA;
		  embolusVector[emb_i].location=daughter;
		} else {
		  nodeVector[embolusVector[emb_i].location].isBlocked=false;
		  int daughter=nodeVector[embolusVector[emb_i].location].daughterB;
		  embolusVector[emb_i].location=daughter;
		}
		
		// }
	      } // The specific embolus has now been moved
	    } // End motion phase
	  } // End loop over embolus
	  
	  
	    //***************************************************
	    // Clear out the blockages in the tree by brute force
	    //***************************************************
	  
	  if (isExtraSafe)
	    for (int node_i=0; node_i<nodeVector.size(); node_i++)
	      nodeVector[node_i].isBlocked=false;
	  
	  //*********************************************
	  // Determine which nodes are blocked after move
	  //*********************************************
	  
	  for (int emb_i=0;emb_i<embolusVector.size();emb_i++) {
	    if (embolusVector[emb_i].size > 
		nodeVector[embolusVector[emb_i].location].size) {
	      nodeVector[embolusVector[emb_i].location].isBlocked=true;
	      if (isWatchEmbolus) {
		std::cout << "Actual blockage in node " << embolusVector[emb_i].location << std::endl;
	      }
	    } else {
	      nodeVector[embolusVector[emb_i].location].isBlocked=false;
	    }
	  }
	  
	} // end if (!embolusVector.empty())

	
	  //**********************************************************************
	  // Advance the queue of blockages used to determine the time correlation
	  //**********************************************************************
	
	for (int inode=0; inode<noNodes; inode++) {
	  for (int i = correlationTime-2; i>=0; i--) {
	    timeStepBlockage[inode * correlationTime + i + 1] 
	      = timeStepBlockage[inode * correlationTime + i];
	  }
	}
	
	
	//*********************************************************
	// Determine the locations of the blockages in the last row
	// N.B. This method is VERY inefficient. Since the tree level
	// and node number in that level were tracked, then the
	// start and end nodes of the blockage could be determined
	// immediately
	//*********************************************************
	
	// ** Reset array containing information on last row blockages ** 
	
	for (int node_i = 0; node_i < totalLastRow; node_i++) {
	  lastRowBlockage[node_i] = 0;
	  for (int level_j = 0; level_j < totalLevels; level_j++)
	    lastRowBlockageI[totalLastRow * level_j + node_i] = 0;
	}
	
	
	// ** Start loop over emboli **
	
	for (int emb_i=0;emb_i<embolusVector.size();emb_i++) {
	  
	  if (nodeVector[embolusVector[emb_i].location].isBlocked==true) {
	    
	    // ** Follow tree to left **
	    
	    daughterlocation = embolusVector[emb_i].location;
	    branchlevel = nodeVector[daughterlocation].treeLevel;
	    
	    for (int i=branchlevel;i<totalLevels-1;i++) {
	      daughterlocation=nodeVector[daughterlocation].daughterA;
	    }
	    
	    startDaughter=daughterlocation;
	    
	    // ** Follow tree to right **
	    
	    daughterlocation = embolusVector[emb_i].location;
	    
	    for (int i=branchlevel;i<totalLevels-1;i++) {
	      daughterlocation=nodeVector[daughterlocation].daughterB;
	    }
	    
	    finishDaughter=daughterlocation;
	    
	    // ** All nodes downstream from blockage are blocked **
	    
	    // std::cout << branchlevel);
	    
	    for (int node_i=startDaughter; node_i<=finishDaughter; node_i++) {
	      lastRowBlockage[node_i - startLastRow] = 1;
	      
	      lastRowBlockageI[node_i - startLastRow + totalLastRow * branchlevel ] = 1;
	    }
	  }
	  
	} // ** end loop over emboli **
	
	  //**************************************************************************
	  // Determine the proportion of emboli on the last row that are blocked
	  // and also update the timeStepBlockage array from which the time correlator
	  // is computed. Note that the lastRowBlockageI 
	  //**************************************************************************
	
	percentage = 0;
	
	for (int level_j = 0; level_j < totalLevels; level_j++) {
	  percentageI[level_j] = 0;
	}
	
	for (int node_i=0; node_i<totalLastRow; node_i++) {
	  
	  for (int level_j = 0; level_j < totalLevels; level_j++) {
	    if (lastRowBlockageI[node_i + level_j * totalLastRow] == 1) {
	      percentageI[level_j] += 1.0/(double)totalLastRow;
	    }
	  }
	  
	  if (lastRowBlockage[node_i] == 1) {
	    
	    percentage += 1.0/(double)totalLastRow;
	    lastRowBlockageTime[node_i] += 1;
	    
	  } else {
	    
	    // Make the average blockage time measurement
	    
	    if (time_i >= warmup) {
	      
	      if (lastRowBlockageTime[node_i] > 0) {
		blockageTime.addNewReading(lastRowBlockageTime[node_i]);
	      }
	      
	    }
	    
	    lastRowBlockageTime[node_i] = 0;
	  }
	  
	}	
	
	//******************************************************
	// Fill up timeStepBlockage. Must be done at least
	// correlationTime steps before making a measurement.
	// For simplicity, measure on every time step.
	//******************************************************
	
	for (int node_i=0; node_i<noNodes; node_i++) {
	  timeStepBlockage[node_i*correlationTime] 
	    = lastRowBlockage[node_i * totalLastRow / noNodes];
	}
	
	
	//**************************************
	// Make measurements if warmup completed
	//**************************************
	
	if (time_i >= warmup) {
	  
	  //******************************
	  // Determine percentage averages
	  //******************************
	  
	  blockagePercentage.addNewReading(percentage);
	  
	  double embolusNumber = (double)embolusVector.size();
	  
	  averageEmbolusNumber.addNewReading(embolusNumber);
	  
	  for (int level_j = 0; level_j < totalLevels; level_j++) {
	    blockagePercentageI[level_j].addNewReading(percentageI[level_j]);
	  }
	
	  //******************************************
	  // Determine inter level correlators (betas)
	  //******************************************
	  
	  // std::cout << "Start beta measurement");
	  
	  betaMeasure = 0.0;
	  
	  // The measurements over node_i should be spread out over the end nodes like the measurements for the
	  // time correlator. This should be done because this measurement takes totalLevels * totalLevels longer
	  // than measuring the percentage blockages on the end nodes, and the measurement can become a bottleneck
	  
	  int nBetaMeasure = 0;
	  
	  for (int node_i=0; node_i< totalLastRow; node_i = node_i + totalLevels * totalLevels) {
	    
	    nBetaMeasure++;
	    
	    for (int level_j = 0; level_j < totalLevels; level_j++) {
	      for (int level_k = 0; level_k < totalLevels; level_k++) {
		
		if (level_j != level_k) {
		  betaMeasure += lastRowBlockageI[node_i + level_j * totalLastRow]
		    * lastRowBlockageI[node_i + level_k * totalLastRow]; 
		}
		
		// Might want to look at the betas separately
		//
		//betaIJ[level_j + totalLevels * level_k] += lastRowBlockageI[node_i + level_j * totalLastRow]
		//* lastRowBlockageI[node_i + level_k * totalLastRow];
	      }
	    }
	    
	  }
	  
	  betaMeasure /= (double)(nBetaMeasure);
	  
	  beta.addNewReading(betaMeasure);
	  
	  // std::cout << "... finish beta measurement: " + betaMeasure);
	  
	  //********************************************
	  // Measurements related to the time correlator
	  //********************************************
	  
	  // Since timeStepBlockage only has values 0 and 1, timeCorrelator[0] has the same value as timeAverageBlock
	  
	  timeAverageBlock = 0.0;
	  for (int inode=0; inode<noNodes; inode++) {
	    timeAverageBlock += (double)timeStepBlockage[correlationTime * inode];
	  }
	  
	  dummy = (double)(timeAverageBlock / noNodes);
	  timeAverage.addNewReading( dummy );
	  
	  for (int i = 0; i < correlationTime; i++) {
	    
	    timeCorrelatorBlock = 0.0;
	    
	    for (int inode = 0; inode < noNodes; inode++) {
	      timeCorrelatorBlock += (double)(timeStepBlockage[inode * correlationTime]
					      * timeStepBlockage[inode * correlationTime + i]);
	    }
	    
	    dummy = (double)(timeCorrelatorBlock / noNodes);

	    timeCorrelator[i].addNewReading( dummy );

	    dummy = std::pow((double)(timeCorrelatorBlock / noNodes),2);

	    timeCorrelatorYY[i].addNewReading( dummy );

	    dummy = (double)(timeCorrelatorBlock / noNodes);

	    dummy2 = (double)(timeAverageBlock / noNodes);

	    timeCorrelatorXY[i].addNewCovarienceReading( dummy, dummy2 );
	    
	  }
	  
	} // End measurements at time > warmpup
	
	  //**************************************************************************
	  // Measure the spacial correlator. This needs updating to use all the
	  // nodes in the tree, and also to have a mapping from fractal space to real
	  // space to make a realistic measurement of the extent of brain damage.
	  //**************************************************************************
	
	for (int i=0; i<totalLastRow; i++) {
	  lastRowCorrelator[i] += (double)lastRowBlockage[0] * (double)lastRowBlockage[i];
	}
	
	time_i++;
	
      } // End loop over block (time_j)
      
      std::cout << "Time step " << time_i << " Total emboli " << embolusVector.size() << std::endl;
      
      //***********************************************
      // N.B. NO WARMUP FOR THE AVERAGES OVER ENSEMBLES
      //***********************************************
      
      //*****************************************
      // Make the sums necessary to determine the
      // average over ensembles, and its error
      //*****************************************	
      
      if ((percTimeSteps) > 0 && (time_i < percTimeSteps)) {
	percentageDead[time_i] += percentage / ensembleSize;
	percentageDeadSq[time_i] += percentage * percentage / ensembleSize;
      }
      
      // if (time_i % 10 == 0)
      std::cout << "Percentage blocked last timestep " << percentage*100.0 << std::endl;
      
      //**********************************************************************************
      // This outputs data sufficient to construct a figure showing the time dependence of
      // end node blockages. N.B. History should be written *EVERY* timestep.
      //**********************************************************************************
      
      if (isMeasureHistory) {	    
	
	std::ofstream outHist2(argv[13+1],std::ios::app);
	std::cout << "Appending.." << std::endl;
	for (int node_i=0; node_i < coarseGrain; node_i++) {
	  double grain = 0.0;
	  //  outHist2 << time_i + " ";
	  
	  for (int grain_i = 0; grain_i < totalLastRow / coarseGrain; grain_i++) {
	    if (lastRowBlockage[node_i * totalLastRow / coarseGrain + grain_i] == 1) {
	      grain += 1.0;
	    }
	  }
	  
	  grain /= (double)(totalLastRow / coarseGrain);
	  outHist2 << grain << " ";
	  
	}
	
	outHist2 << "\n";
	outHist2.close();
	
	
      } // End history measurement
      
      if (time_i > warmup) {
	
	double timeAverageNormal = 0.0;
	double timeAverageNormalError = 0.0;
	
	double squaredTimeAverageNormal = 0.0;
	double squaredTimeAverageNormalError = 0.0;
	
	double timeCorrelator0Reading = 0.0;
	double timeCorrelator0Error = 0.0;
	
	double timeCorrelatorIReading = 0.0;
	double timeCorrelatorIError = 0.0;
	
	//**********************************************
	// Write out the time correlator every few steps
	//**********************************************
	
	std::cout << "Writing time correlator" << std::endl;
	
	
	std::ofstream out(argv[10+1]);
	// This just causes problems when files are merged
	// out << "# time step, correlator, average,  \n";
	
	// Normalised "Time correlator" at time 0, with the standard error
	// The errors are complicated by significant correlations in the
	// time correlator.
	
	timeCorrelator0Reading
	  = timeCorrelator[0].getAverageOverSeries();
	timeCorrelator0Error
	  = timeCorrelator[0].getStandardDeviation();
	
	// Time average of blockage and standard error
	
	timeAverageNormal = timeAverage.getAverageOverSeries();
	timeAverageNormalError = timeAverage.getStandardDeviation();
	
	// Blockage average squared, with error from y = x*x -> Dy = 2x Dx
	
	squaredTimeAverageNormal = timeAverageNormal * timeAverageNormal;
	squaredTimeAverageNormalError = 2.0 * timeAverageNormal * timeAverageNormalError;
	
	// Denominator of the normalised time correlator, and error for z = x + y computed from
	// Dz <= Sqrt[ Dx^2 + Dy^2] (pythagoras on true error)
	
	double timeCorrelatorDenominator = timeCorrelator0Reading - squaredTimeAverageNormal;
	double timeCorrelatorDenominatorErrorSq = timeCorrelator0Error * timeCorrelator0Error + squaredTimeAverageNormalError * squaredTimeAverageNormalError;
	double timeCorrelatorDenominatorError = std::sqrt(timeCorrelator0Error * timeCorrelator0Error + squaredTimeAverageNormalError * squaredTimeAverageNormalError);
	
	// std::cout << timeCorrelator0Reading + " " + timeAverageNormal);
	
	isTimeCorrelatorConverged = true;
	nTimeCorrelatorConverged = 0;
	
	double lastTimeCorrelatorNumerator = 999999999.0;
	
	double integratedCorrelationTime = 0.0;
	double integratedCorrelationTimeError = 0.0;

	for (int i = 0; i < correlationTime; i++) {
	  
	  timeCorrelatorIReading
	    = timeCorrelator[i].getAverageOverSeries();
	  timeCorrelatorIError
	    = timeCorrelator[i].getStandardDeviation();
	  
	  
	  double timeCorrelatorNumerator = timeCorrelatorIReading - squaredTimeAverageNormal;
	  double timeCorrelatorNumeratorError = std::sqrt(timeCorrelatorIError * timeCorrelatorIError 
							  + squaredTimeAverageNormalError * squaredTimeAverageNormalError);
	  
	  // Normalised time correlator, and the error for z = x / y computed from 
	  // pythagoras on fractional error [Derived from pythag on true error]
	  
	  double timeCorrelatorValue = timeCorrelatorNumerator / timeCorrelatorDenominator;
	  
	  integratedCorrelationTime+=timeCorrelatorValue;

	  // Error estimation modified by significant correlation between variables
	  
	  double covarienceUv = timeCorrelatorXY[i].getStandardDeviationCovarientSq();
	  double dGbyDu = 1/timeCorrelatorDenominator;
	  double dGbyDv = -((timeCorrelatorIReading * (1 - 2.0 * timeCorrelator0Reading) + timeCorrelator0Reading * timeCorrelator0Reading)
			    / timeCorrelatorDenominator / timeCorrelatorDenominator);
	  double uError = timeCorrelatorIError;
	  double vError = timeCorrelator0Error;
	  
	  double timeCorrelatorError = std::pow(uError * dGbyDu , 2.0);
	  timeCorrelatorError += std::pow(vError * dGbyDv , 2.0);
	  timeCorrelatorError += 2.0 * covarienceUv * dGbyDu * dGbyDv;
	  
	  integratedCorrelationTimeError += timeCorrelatorError;

	  // std::cout << covarienceUv + " " + uError + " " + vError);
	  // std::cout << std::pow(uError * dGbyDu , 2.0) + " " + std::pow(vError * dGbyDv , 2.0) + " " + 2.0 * covarienceUv * dGbyDu * dGbyDv);
	  
	  // timeCorrelatorError = std::sqrt(timeCorrelatorError);
	  
	  out << i << " " << timeCorrelatorValue << " " << (timeCorrelatorError > 0.0 ? std::sqrt(timeCorrelatorError) : 0) << " ";
	  out << timeCorrelatorNumerator << " " << timeCorrelatorNumeratorError << " " << integratedCorrelationTime << " " << sqrt(integratedCorrelationTimeError) << std::endl;
	  
	  // The t0 time correlator is always 1. For plotting purposes, don't need overall
	  // accuracy > 1% of that value, so don't divide by timeCorrelatorValue
	  
	  // if ((std::sqrt(std::abs(timeCorrelatorError)) > prm::tolerance)
	  //     || (timeCorrelatorDenominator == 0.0)) 
	  //   isTimeCorrelatorConverged = false;
	  // else nTimeCorrelatorConverged++;
	  
	  if ((std::sqrt(std::abs(timeCorrelatorNumeratorError/timeCorrelatorNumerator)) > prm::tolerance)
	      || (timeCorrelatorDenominator == 0.0)) 
	    isTimeCorrelatorConverged = false;
	  else nTimeCorrelatorConverged++;
	  
	  lastTimeCorrelatorNumerator = timeCorrelatorNumerator;
	  
	}
	
	std::cout << "Time correlator " << nTimeCorrelatorConverged << " of " << correlationTime << " converged" << std::endl;
	
	out.close();
	
	
	
	//**********************************************
	// Write out the info file every few steps
	//**********************************************
	
	std::cout << "Writing information" << std::endl;
	
	
	std::ofstream outEquib(argv[12+1]);
	outEquib << "# Various dynamic equilibrium averages,  \n";
	
	// Normalised "Time correlator" at time 0, with the standard error
	
	outEquib << "Total time steps = " << time_i << "\n";
	
	outEquib << "Mean time node remains blocked = ";
	outEquib << blockageTime.getAverageOverSeries() << " +/- " << blockageTime.getStandardDeviation() << "\n\n";
	outEquib << "Inter-layer blockage correlation = ";
	outEquib << beta.getAverageOverSeries() << " +/- " << beta.getStandardDeviation() << "\n\n";
	outEquib << "Percentage blocked on average = ";
	outEquib << (100.0 * blockagePercentage.getAverageOverSeries()) << " +/- " << (100.0 * blockagePercentage.getStandardDeviation()) << "\n";
	for (int level_j = 0; level_j < totalLevels; level_j++) {
	  outEquib << "Percentage blocked in level " << level_j << " = " ;
	  outEquib << 100.0 * blockagePercentageI[level_j].getAverageOverSeries() << " +/- ";
	  outEquib << 100.0 * blockagePercentageI[level_j].getStandardDeviation() << "\n";
	}
	
	outEquib << "Average number of emboli = " << averageEmbolusNumber.getAverageOverSeries() << " +/- " << averageEmbolusNumber.getStandardDeviation() << "\n";
	outEquib << "Average squared blockage = ";
	outEquib << squaredTimeAverageNormal << " +/- " << squaredTimeAverageNormalError << "\n";
	outEquib << "Squared average blockage t0 = ";
	outEquib << timeCorrelator0Reading << " +/- " << timeCorrelator0Error << "\n";
	outEquib << "Squared average blockage tI = ";
	outEquib << timeCorrelatorIReading << " +/- " << timeCorrelatorIError << "\n";
	    
	outEquib.close();
	
	
      }
      
      if (time_i > warmup + 5 * blockLength) {
	isConverged = embolusRemoved;
	isConverged &= blockagePercentage.isConverged();
	isConverged &= averageEmbolusNumber.isConverged();
	// isConverged &= beta.isConverged();
	if (isMeasureTimeCorrelator)
	  isConverged = isTimeCorrelatorConverged;
      } else {
	isConverged = false;
      }
      
      if (isMeasureHistory && time_i > totalTimeSteps) {
	isConverged = true;
      }
      
    }  while (!isConverged); // End loop over series
    
    //*********************************************************************************
    // Write out the ensemble averages of the time dependence of the last row blockages
    //*********************************************************************************
    
    
    std::ofstream out(argv[9+1]);
    out << "# Emb rate, dissolve rate, emb. size max, emb. size min\n";
    out << "# " << embRate << ", " << dissolveRate << ", " << embSizeMax << ", " << embSizeMin << "\n";
    for (int i=0;i<percTimeSteps;i++) {
      out << i << " " << (double)ensembleSize * percentageDead[i]/(double)(ens_i+1) << " " << std::sqrt((percentageDeadSq[i]-percentageDead[i]*percentageDead[i])/(double)(ens_i+1)) << "\n";
    }
    out.close();
    
    
  } // End loop over ensembles (ens_i)
  
  double spaceCorrelatorError, spaceCorrelatorValue;
  
  std::ofstream out(argv[11+1]);
  out << "# node diff, correlator, average,  \n";
  
  int k = 1;
  for (int node_i=0;node_i<totalLevels;node_i++) {
    int j = k - 1;
    
    // spaceCorrelatorValue = ((double)lastRowCorrelator[j]-(double)timeAverage*(double)timeAverage/(double)totalTimeAverage)
    //  /((double)lastRowCorrelator[0]-(double)timeAverage*(double)timeAverage/(double)totalTimeAverage);
    
    // out << j + " " + spaceCorrelatorValue + " " + (double)timeAverage/(double)totalTimeAverage + " " + lastRowCorrelator[j]/totalTimeAverage + "\n");
    // j++;
    k*=2;
  }
  out.close();
  
} // End function main



