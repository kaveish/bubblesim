//****************************************************************************
//  Accumulator.cpp                                                            
//  'Accumulator' class                                                        
//  Copyright (C) 2002 Pavel Kornilovich.  All right reserved                  
//  Mountain View, CA                                                          
//****************************************************************************

// #define DEBUG

#include "Accumulator.h"
#include "nr.h"

//*****************************************************************************
// ..Constructors and destructors..                                           
//*****************************************************************************

void Accumulator::setHistogram(double one,double two,int three)
{
  histogramMin = one;
  histogramMax = two;
  histogramSize = three;
}

// Default constructor.  Assumes that "nseries" and "nmeas" are defined
// as external variables and takes their values to "numberOfSeries" and
// "seriesLength"

void Accumulator::setWeightingFactor(double externalWeighting)
{
  internalWeighting = externalWeighting;
  flagWeightingFactor = true;
}

void Accumulator::setSeriesLength(int one)
{ seriesLength = one; }

void Accumulator::setLocalTolerance(double one)
{ localTolerance = one/3.0; }

// const Accumulator& Accumulator::operator= (const Accumulator& right)
Accumulator& Accumulator::operator= (const Accumulator& right)
{
  numberOfSeries=right.numberOfSeries;
  nseriesCompleted=right.nseriesCompleted;
  ndoubleseriesCompleted=right.ndoubleseriesCompleted;

  // Data associated with the current series of measurements

  seriesLength=right.seriesLength;
  nreadings=right.nreadings;

  currentSeriesSum = right.currentSeriesSum;
  lastSeriesSum = right.lastSeriesSum;

  localSquaredSum=right.localSquaredSum;
  localSum=right.localSum;

  localDoubleSquaredSum=right.localDoubleSquaredSum;
  localDoubleSum=right.localDoubleSum;

  return *this;
}

Accumulator::Accumulator(const Accumulator& right)
{
  numberOfSeries = right.numberOfSeries;
  nseriesCompleted = right.nseriesCompleted;
  ndoubleseriesCompleted=right.ndoubleseriesCompleted;

  // Data associated with the current series of measurements

  seriesLength=right.seriesLength;
  nreadings=right.nreadings;

  currentSeriesSum=right.currentSeriesSum;
  lastSeriesSum = right.lastSeriesSum;

  localSquaredSum=right.localSquaredSum;
  localSum=right.localSum;

  localDoubleSquaredSum=right.localDoubleSquaredSum;
  localDoubleSum=right.localDoubleSum;
}

Accumulator::Accumulator() {

#ifdef DEBUG
  std::cout << "Initialising accumulator" << std::endl;
#endif

  numberOfSeries = prm::nseries;
  seriesLength = prm::nmeas;

  isDumpFlag=false;

  flagWeightingFactor = false;

  setDataToZero();

#ifdef DEBUG
  std::cout << "Initialised accumulator" << std::endl;
#endif

}


// Constructs an accumulator that holds only one value of measured quantity.
// Assumes "numberOfSeries" = 1.  The only series has length "slength"

Accumulator::Accumulator(int slength) {

  internalWeighting = 1.0;

  numberOfSeries=1;
  seriesLength=slength;

  isDumpFlag=false;

  setDataToZero();
  
}

// Constructs and accumulator that holds "nseries" values of the
// measured quantity.  Each series has length "slength"

Accumulator::Accumulator(int numbseries, int slength) {

  internalWeighting = 1.0;
  
  numberOfSeries=numbseries;
  seriesLength=slength;

  isDumpFlag=false;

  setDataToZero();
 
}


// Destructor

Accumulator::~Accumulator() {
  

}
//****************************************************************************
// ..END of constructors and destructors..                                    
//****************************************************************************


//****************************************************************************
//  ..Useful functions..                                                     
//****************************************************************************

//****************************************************************************
// ..FUNCTION that clears the data array, that is sets it to zero..            
//****************************************************************************

void Accumulator::setDataToZero() {
  
  int i;

#ifdef DEBUG
  std::cout  << "Zeroing accumulator" << std::endl;
#endif

  numberOfSeries = prm::nseries;
  seriesLength = prm::nmeas;

  nreadings 	   = 0;
  nseriesCompleted = 0;
  ndoubleseriesCompleted = 0;

  currentSeriesSum = 0.0;
  currentWeightingSum = 0.0;
  currentExternalSum = 0.0;

  lastSeriesSum  	   = 0.0;
  lastWeightingSum  	   = 0.0;

  localSquaredSum  = 0.0;
  localSum         = 0.0;
  localDoubleSquaredSum  = 0.0;
  localDoubleSum         = 0.0;

  weightingSquaredSum  = 0.0;
  weightingCovarienceSum  = 0.0;
  weightingSum         = 0.0;
  weightingDoubleSquaredSum  = 0.0;
  weightingDoubleSum         = 0.0;

  weightingCovarienceSum = 0.0;

  externalSum      = 0.0;
  externalSumSquared = 0.0;
  
  localTolerance = prm::tolerance/3.0; // Use the 3 sigma condition

  measurementRecord.resize(0);
  weightingRecord.resize(0);
  
#ifdef DEBUG
  std::cout << "Accumulator zeroed" << std::endl;
#endif
}


//*****************************************************************************
//  ..END of function that clears the data array..                             
//*****************************************************************************


//*****************************************************************************
// ..FUNCTION that adds a new reading to the accumulator..                     
//*****************************************************************************

void Accumulator::setDumpFlag()
{
  isDumpFlag=true;
}

void Accumulator::addNewCovarienceReading(double& u,double& v)
{
  
  currentSeriesSum += u;
  currentExternalSum += v;
  currentWeightingSum += internalWeighting;

  nreadings++;

  if ( nreadings == seriesLength ) {

    localSum += currentSeriesSum/(double)seriesLength;
    localSquaredSum += std::pow(currentSeriesSum/(double)seriesLength,2.0);

    externalSum+=currentExternalSum/(double)seriesLength;
    externalSumSquared+=currentExternalSum * currentSeriesSum 
      /(double)(seriesLength * seriesLength);
    
    weightingSum += currentWeightingSum/(double)seriesLength;
    weightingSquaredSum += 
      std::pow(currentWeightingSum/(double)seriesLength,2.0);
    weightingCovarienceSum += 0.0; // This is not complete!

    nseriesCompleted++;
    nreadings = 0;

    lastSeriesSum = 0.0;
    lastWeightingSum = 0.0;

    currentSeriesSum = 0.0;
    currentExternalSum = 0.0;
    currentWeightingSum = 0.0;
  }
}

void Accumulator::addNewReading(double& newReading) {
  
  //  First update the data

  if (flagWeightingFactor) {
    currentSeriesSum += newReading * internalWeighting;
    currentWeightingSum += internalWeighting;
  } else {
    currentSeriesSum += newReading;
    currentWeightingSum += 1.0;
  }

  nreadings++;

  // Check if the current series has ended.  If yes, compute and store
  // the series average, and reset the values of all members
  
  if ( nreadings == seriesLength ) {

    if (nseriesCompleted%2==1) {
      ndoubleseriesCompleted++;

      localDoubleSum
	+=0.5*(currentSeriesSum/(double)seriesLength+lastSeriesAverage);
      localDoubleSquaredSum
	+=std::pow(0.5*(lastSeriesAverage+currentSeriesSum
			/(double)seriesLength),2.0);

      weightingDoubleSum += 
	0.5*(currentWeightingSum/(double)seriesLength+lastWeightingAverage);
      weightingDoubleSquaredSum += 
	std::pow(0.5*(lastWeightingAverage+currentWeightingSum
		      /(double)seriesLength),2.0);
    }

    lastSeriesAverage = currentSeriesSum/(double)seriesLength;
    lastWeightingAverage = currentWeightingSum/(double)seriesLength;

    localSum += lastSeriesAverage;
    localSquaredSum += lastSeriesAverage * lastSeriesAverage;

    if (prm::convergenceType == ConvergenceBootstrap
	|| prm::convergenceType == ConvergenceBootstrapSeries)
      measurementRecord.push_back(lastSeriesAverage);
    
    weightingSum += lastWeightingAverage;
    weightingSquaredSum += lastWeightingAverage * lastWeightingAverage;

    if (prm::convergenceType == ConvergenceBootstrap
	|| prm::convergenceType == ConvergenceBootstrapSeries)
      weightingRecord.push_back(lastWeightingAverage);
    
    weightingCovarienceSum += lastWeightingAverage * lastSeriesAverage;
    
    nseriesCompleted++;
    nreadings = 0;
    currentSeriesSum = 0.0;
    lastSeriesSum = 0.0;

    currentWeightingSum = 0.0;
    lastWeightingSum = 0.0;

  }
}

void Accumulator::addNewReading(bool& bNewReading) {
  
  double newReading;

  // Convert boolean value to double

  if (bNewReading)
    newReading = 1.0;
  else 
    newReading = 0.0;

  addNewReading(newReading);
}


void Accumulator::addNewReading(int& iNewReading) {
  
  double newReading;

  newReading = (double)iNewReading;

  addNewReading(newReading);
}


//*****************************************************************************
//  ..END of function that adds a new reading to the accumulator..             
//*****************************************************************************

//****************************************************************************
// ..FUNCTION that returns average over all series of measurements..         
//****************************************************************************
double Accumulator::getAverageOverSeries() {

  if ( nseriesCompleted == 0 ) {
    std::cout 	<< "Accumulator::getAverageOverSeries() : "
		<< "Warning: no completed series" << std::endl;
    return 0.0;
  }
  else {
    return localSum/weightingSum;
  }
}

double Accumulator::getAverageOverSeriesNoWeighting() {

  if ( nseriesCompleted == 0 ) {
    std::cout 	<< "Accumulator::getAverageOverSeries() : "
		<< "Warning: no completed series" << std::endl;
    return 0.0;
  }
  else {
    return localSum / double(nseriesCompleted);
  }
}

double Accumulator::getAverageWeighting() {

  if ( nseriesCompleted == 0 ) {
    std::cout 	<< "Accumulator::getAverageOverSeries() : "
		<< "Warning: no completed series" << std::endl;
    return 0.0;
  }
  else {
    return weightingSum/double(nseriesCompleted);
  }
}

//****************************************************************************
// ..END of function that returns average over all series of measurements..   
//****************************************************************************

double Accumulator::getAverageOverSeriesWeighted(Accumulator &weight) {
  return getAverageOverSeries()/weight.getAverageOverSeries();
}

double Accumulator::getStandardDeviationWeighted(Accumulator &sign) {
  if (std::abs(getAverageOverSeriesWeighted(sign)) == 0.0) return 0.0;
  return std::abs(getAverageOverSeriesWeighted(sign)
		  *std::sqrt(std::pow(getStandardDeviation()
				      /getAverageOverSeries(),2.0)
			     +std::pow(sign.getStandardDeviation()
				       /sign.getAverageOverSeries(),2.0)));
}

//****************************************************************************
// ..FUNCTION that estimates the standard deviation of the mean..             
//****************************************************************************
double Accumulator::getStandardDeviationSeries() {
 
  if ( nseriesCompleted < 2 ) {
    // In the event of only one measurement, can't say anything about
    // the standard deviation, so error is 100% of value
    return localSum/((double) nseriesCompleted);
  }
  else {

    if ( prm::convergenceType == ConvergenceBootstrap
	 || prm::convergenceType == ConvergenceBootstrapSeries)
      return bootstrap(measurementRecord);

    double sum   = 0.0;
    double sumsq = 0.0;
 
    sum   = localSum / double(nseriesCompleted);
    sumsq = localSquaredSum / double(nseriesCompleted);
    double sigmasq = (sumsq - sum*sum) / double(nseriesCompleted - 1);
    return sqrt(sigmasq);
  }
}

double Accumulator::getDeviationWeighting() {
  
  if ( nseriesCompleted < 2 ) {
    // In the event of only one measurement, can't say anything about
    // the standard deviation, so error is 100% of value
    return weightingSum/((double) nseriesCompleted);
  }
  else {

    if ( prm::convergenceType == ConvergenceBootstrap 
	 || prm::convergenceType == ConvergenceBootstrapSeries)
      return bootstrap(weightingRecord);

    double sum   = 0.0;
    double sumsq = 0.0;
 
    sum   = weightingSum/((double) nseriesCompleted);
    sumsq = weightingSquaredSum/((double) nseriesCompleted);
    double sigmasq = (sumsq - sum*sum) / double(nseriesCompleted - 1);
    return sqrt(sigmasq);
  }
}

double Accumulator::getCovarienceWeighting() {
  
  if ( nseriesCompleted < 2 ) {
    // In the event of only one measurement, can't say anything about
    // the standard deviation, so error is 100% of value
    return weightingSum/((double) nseriesCompleted);
  }
  else {

    if ( prm::convergenceType == ConvergenceBootstrap 
	 || prm::convergenceType == ConvergenceBootstrapSeries)
      return bootstrapCovariance(measurementRecord,weightingRecord);

    double sum   = 0.0;
    double sumsq = 0.0;
    double sumLocal = 0.0;
 
    sum   = weightingSum/((double) nseriesCompleted);
    sumLocal   = localSum/((double) nseriesCompleted);
    sumsq = weightingCovarienceSum/((double) nseriesCompleted);
    double sigmasq = (sumsq - sum*sumLocal) / double(nseriesCompleted - 1);
    return sigmasq;
  }
}



double Accumulator::getStandardDeviation() {
  if (std::abs(getAverageOverSeries()) == 0.0) return 0.0;

  double result;

  if ( prm::convergenceType == ConvergenceBootstrap 
       || prm::convergenceType == ConvergenceBootstrapSeries)
    return std::abs(getAverageOverSeries()) * bootstrapCovarianceErrorEst(measurementRecord,weightingRecord);  

  result = std::pow(getStandardDeviationSeries() / getAverageOverSeriesNoWeighting(),2.0) + std::pow(getDeviationWeighting() / getAverageWeighting(),2.0) - 2.0 * getCovarienceWeighting() / getAverageOverSeriesNoWeighting() / getAverageWeighting();

  result = std::abs(result);
  result = std::sqrt(result);
  result *= getAverageOverSeries();
  result = std::abs(result);

  return result;


  // return std::abs(getAverageOverSeries()
  //		  * std::sqrt(std::abs(std::pow(getStandardDeviationSeries()
  //						/ getAverageOverSeriesNoWeighting(),2.0)
  //				       + std::pow(getDeviationWeighting()
  //						  / getAverageWeighting(),2.0)
  //				       - 2.0 * (getCovarienceWeighting()
  //						/ getAverageOverSeriesNoWeighting() 
  //						/ getAverageWeighting()))));
}

double Accumulator::getStandardDeviationDouble() {
  
  if ( ndoubleseriesCompleted < 2 ) {
    // In the event of only one measurement, can't say anything about
    // the standard deviation, so error is 100% of value
    return localSum/((double) nseriesCompleted);
  }
  else {
    double sum   = 0.0;
    double sumsq = 0.0;
 
    sum   = localDoubleSum/((double) ndoubleseriesCompleted);
    sumsq = localDoubleSquaredSum/((double) ndoubleseriesCompleted);
    double sigmasq = (sumsq - sum*sum) /
      ( (double)(ndoubleseriesCompleted - 1) );
    return sqrt(sigmasq);
  }
}

// N.B. Should be deprecated since there is no sqrt at the end
double Accumulator::getStandardDeviationCovarient() {

  if ( nseriesCompleted < 2 ) {
    // In the event of only one measurement, can't say anything about
    // the standard deviation, so error is 100% of value
    return localSum/((double) nseriesCompleted);
  }
  else {
    double sum   = 0.0;
    double sumsq = 0.0;
    double sumext = 0.0;
 
    sum   = localSum/((double) nseriesCompleted);
    sumsq = externalSumSquared/((double) nseriesCompleted);
    sumext = externalSum/((double) nseriesCompleted);
    double sigmasq = (sumsq - sumext*sum) /
      ( (double)(nseriesCompleted - 1) );
    // std::cout << "sigma sq " << sigmasq << std::endl;
    return sigmasq;
  }
}

// N.B. This is more appropriate I think, since there is no sqrt at the end
double Accumulator::getStandardDeviationCovarientSq() {
    
  if ( nseriesCompleted < 2 ) {
    // In the event of only one measurement, can't say anything about
    // the standard deviation, so error is 100% of value
    return localSum/((double) nseriesCompleted);
  }
  else {
    double sum   = 0.0;
    double sumsq = 0.0;
    double sumext = 0.0;
    
    sum   = localSum/((double) nseriesCompleted);
    sumsq = externalSumSquared/((double) nseriesCompleted);
    sumext = externalSum/((double) nseriesCompleted);
    double sigmasq = (sumsq - sumext*sum) /
      ( (double)(nseriesCompleted - 1) );
    return sigmasq;
  }
}


double Accumulator::getLastSeriesAverage() {
  return lastSeriesAverage;
}

bool Accumulator::isConverged()
{
  if (prm::convergenceType == ConvergenceError
      || prm::convergenceType == ConvergenceBootstrap) {
    if (std::abs(getStandardDeviation())<=
	localTolerance * std::abs(getAverageOverSeries())) return true;
    return false;
  } else if (prm::convergenceType == ConvergenceSeries ||
	     prm::convergenceType == ConvergenceBootstrapSeries) {
    if (nseriesCompleted >= prm::nseries)
      return true;
    return false;
  } else { // Convergence by time
    if ((prm::currentTime - prm::parameterSetStartTime)/CLOCKS_PER_SEC/60 
	>= prm::wallTimeMinutes)
      return true;
    return false;
  }
}

bool Accumulator::isConvergedWeighted(Accumulator &sign) {
  if (prm::convergenceType == ConvergenceError ||
      prm::convergenceType == ConvergenceBootstrap) {
    if (std::abs(getAverageOverSeriesWeighted(sign)) == 0) return true;
    if (std::abs(getStandardDeviationWeighted(sign))<=
	localTolerance * std::abs(getAverageOverSeriesWeighted(sign))) return true;
    return false;
  } else if (prm::convergenceType == ConvergenceSeries
	     || prm::convergenceType == ConvergenceBootstrapSeries) {
    if (nseriesCompleted >= prm::nseries)
      return true;
    return false;
  } else { // Convergence by time
    if ((prm::currentTime - prm::parameterSetStartTime)/CLOCKS_PER_SEC/60 
	>= prm::wallTimeMinutes)
      return true;
    return false;
  }
}

double Accumulator::getAverageOverSeriesLogBeta()
{
  if (getAverageOverSeries() <= 0.0) return -9999.0;
  return -log(getAverageOverSeries())/prm::beta;
}

double Accumulator::getStandardDeviationLogBeta()
{
  return std::abs(getStandardDeviation()
		  /prm::beta/getAverageOverSeries());
}

bool Accumulator::isConvergedLogBeta() {
  if (prm::convergenceType == ConvergenceError
      || prm::convergenceType == ConvergenceBootstrap) {
    double error = getStandardDeviationLogBeta();
    if (getAverageOverSeries() <= 0.0) return false;
    double value = getAverageOverSeriesLogBeta();
    if (error / value < localTolerance) return true;
    return false;
  } else if (prm::convergenceType == ConvergenceSeries
	     || prm::convergenceType == ConvergenceBootstrapSeries) {
    if (nseriesCompleted >= prm::nseries)
      return true;
    return false;
  } else { // Convergence by time
    if ((prm::currentTime - prm::parameterSetStartTime)/CLOCKS_PER_SEC/60 >= prm::wallTimeMinutes)
      return true;
    return false;
  }
}

int Accumulator::estimatedIterations()
{
  if ( nseriesCompleted < 2 ) {
    // In the event of only one measurement, can't say anything about
    // the standard deviation, so return anomalous number

    return -999;

  }
  else {

    return  nseriesCompleted * ( (int)( std::pow(getStandardDeviation()
						 /getAverageOverSeries()
						 /localTolerance,2.0) ) - 1  );
    
  }
}

//****************************************************************************
// ..END of function that estimates the standard deviation of the mean..      
//****************************************************************************

std::string Accumulator::printScr()
{
  std::stringstream allOutput;
  allOutput << this->getAverageOverSeries() << " +/- "
	    << 3.0*this->getStandardDeviation() 
    // << " (+/- "
    //     << 3.0*this->getStandardDeviationDouble() << " )" 
	    << std::ends;
  return allOutput.str();
}

void Accumulator::printFile(std::ofstream &dataStream)
{
  dataStream << this->getAverageOverSeries() << " "
	     << this->getStandardDeviation();
}

std::string Accumulator::printScrLogBeta()
{
  std::stringstream allOutput;
  allOutput << this->getAverageOverSeriesLogBeta() << " +/- "
	    << 3.0*this->getStandardDeviationLogBeta() << std::ends;
  return allOutput.str();
}

void Accumulator::printFileLogBeta(std::ofstream &dataStream)
{
  dataStream << this->getAverageOverSeriesLogBeta() << " "
	     << this->getStandardDeviationLogBeta();
}

std::string Accumulator::printScrWeighted(Accumulator &sign)
{
  std::stringstream allOutput;
  allOutput << this->getAverageOverSeriesWeighted(sign) << " +/- "
	    << 3.0*this->getStandardDeviationWeighted(sign) << std::ends;
  return allOutput.str();
}

void Accumulator::printFileWeighted(Accumulator &sign, 
					   std::ofstream &dataStream)
{
  dataStream << this->getAverageOverSeriesWeighted(sign) << " "
	     << this->getStandardDeviationWeighted(sign);
}

double Accumulator::bootstrap(std::vector<double> &dataValues)
{
  int randomIndex;
  int dataSize = dataValues.size();
  double result;
  double localResult, localResultSq;

  double randomMean;

  // int nBootstrap = dataSize * 10;

  // I read somewhere that 1000 bootstrap samples is
  // what is needed for confidence intervals.
  // This sounds plausible, since one could get a gaussian 
  // histogram easily in this way (bootstrap is essentially
  // an advanced Gausianizing technique)
  // I really need a proper book though.

  int nBootstrap = 1000;

  // std::cout << "bootstrapping" << std::endl;

  localResult = 0.0;
  localResultSq = 0.0;

  for (int i=0; i<nBootstrap; i++) {

    randomMean = 0.0;

    for (int j=0; j<dataSize; j++) {
      randomIndex = selectRandomValue(dataSize);
      randomMean += dataValues[randomIndex];
    }

    randomMean /= (double)dataSize; // randomMean is now the bootstrap value
    localResult += randomMean;
    localResultSq += randomMean * randomMean;
  }

  localResult /= (double)nBootstrap;
  localResultSq /= (double)nBootstrap;

  // Now the standard error is simply the standard deviation of the
  // bootstrap.

  return sqrt(localResultSq - localResult * localResult);

}


double Accumulator::bootstrapCovariance(std::vector<double> &dataValuesOne,
					std::vector<double> &dataValuesTwo)
{
  int randomIndex;
  int dataSize = dataValuesOne.size();
  double result;
  double localResultOne, localResultTwo, localResultOneTwo;

  double bootstrapValueOne, bootstrapValueTwo;

  // int nBootstrap = dataSize * 10;

  // I read somewhere that 1000 bootstrap samples is
  // what is needed for confidence intervals.
  // This sounds plausible, since one could get a gaussian 
  // histogram easily in this way (bootstrap is essentially
  // an advanced Gausianizing technique)
  // I really need a proper book though.

  int nBootstrap = 1000;

  // std::cout << "bootstrapping" << std::endl;

  localResultOne = 0.0;
  localResultTwo = 0.0;
  localResultOneTwo = 0.0;

  for (int i=0; i<nBootstrap; i++) {

    bootstrapValueOne = 0.0;
    bootstrapValueTwo = 0.0;

    for (int j=0; j<dataSize; j++) {
      randomIndex = selectRandomValue(dataSize);

      // I think these have to be from the same data set

      bootstrapValueOne += dataValuesOne[randomIndex];
      bootstrapValueTwo += dataValuesTwo[randomIndex];
    }

    bootstrapValueOne /= (double)dataSize;
    bootstrapValueTwo /= (double)dataSize;
    localResultOne += bootstrapValueOne;
    localResultTwo += bootstrapValueTwo;
    localResultOneTwo += bootstrapValueOne * bootstrapValueTwo;
  }

  localResultOne /= (double)nBootstrap;
  localResultTwo /= (double)nBootstrap;
  localResultOneTwo /= (double)nBootstrap;

  // Now the covarience can be computed.

  return localResultOneTwo - localResultOne * localResultTwo;

}

// Error estimation for quotient
double Accumulator::bootstrapCovarianceErrorEst(std::vector<double> &dataValuesOne,
						std::vector<double> &dataValuesTwo)
{
  int randomIndex;
  int dataSize = dataValuesOne.size();

  double localResultOne, localResultTwo, localResultOneTwo;
  double localResultOneSq, localResultTwoSq;
  
  double bootstrapValueOne, bootstrapValueTwo;

  // int nBootstrap = dataSize * 10;

  // I read somewhere that 1000 bootstrap samples is
  // what is needed for confidence intervals.
  // This sounds plausible, since one could get a gaussian 
  // histogram easily in this way (bootstrap is essentially
  // an advanced Gausianizing technique)
  // I really need a proper book though.

  int nBootstrap = 1000;

  // std::cout << "bootstrapping" << std::endl;
 
  localResultOne = 0.0;
  localResultTwo = 0.0;
  localResultOneSq = 0.0;
  localResultTwoSq = 0.0;
  localResultOneTwo = 0.0;
  
  for (int i=0; i<nBootstrap; i++) {
    
    bootstrapValueOne = 0.0;
    bootstrapValueTwo = 0.0;

    for (int j=0; j<dataSize; j++) {
      randomIndex = selectRandomValue(dataSize);

      // I think these have to be from the same data set

      bootstrapValueOne += dataValuesOne[randomIndex];
      bootstrapValueTwo += dataValuesTwo[randomIndex];
    }

    bootstrapValueOne /= (double)dataSize;
    bootstrapValueTwo /= (double)dataSize;
    localResultOne += bootstrapValueOne;
    localResultOneSq += bootstrapValueOne * bootstrapValueOne;
    localResultTwo += bootstrapValueTwo;
    localResultTwoSq += bootstrapValueTwo * bootstrapValueTwo;
    localResultOneTwo += bootstrapValueOne * bootstrapValueTwo;
  }

  localResultOne /= (double)nBootstrap;
  localResultTwo /= (double)nBootstrap;
  localResultOneSq /= (double)nBootstrap;
  localResultTwoSq /= (double)nBootstrap;
  localResultOneTwo /= (double)nBootstrap;

  // Now the error can be computed.

  double result;

  result = localResultOneSq / localResultOne / localResultOne - 1.0;

  result += localResultTwoSq / localResultTwo / localResultTwo - 1.0;

  result -= 2.0 * (localResultOneTwo / localResultOne / localResultTwo - 1.0);

  return std::sqrt(std::abs(result));
}


int Accumulator::selectRandomValue(int nValues)
{
  return (int)std::floor(ran2(prm::idum) 
			 * (double)nValues);
}
